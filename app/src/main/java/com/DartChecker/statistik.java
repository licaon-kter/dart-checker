package com.DartChecker;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import org.jsefa.Serializer;
import org.jsefa.csv.CsvIOFactory;
import org.jsefa.csv.config.CsvConfiguration;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import android.app.NotificationChannel;
import android.app.NotificationManager;



public class statistik extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    private final DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    private final DecimalFormat formater = new DecimalFormat("###.##", symbols);

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "NAmedeschannels";
            String description = "blabbla";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1212", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_statistik);
        ArrayAdapter<MainActivity.spieler> arrayAdapter = new ArrayAdapter<>(
                statistik.this, R.layout.spinner_item_head, MainActivity.allespieler);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        SharedPreferences settings = getSharedPreferences("Einstellungen",0);
        ConstraintLayout main = findViewById(R.id.mainlayout);
        if (settings.contains("keepscreenonmenu")) main.setKeepScreenOn(settings.getBoolean("keepscreenonmenu",false));

        final Spinner spielera = findViewById(R.id.spielera);
        final Spinner spielerb = findViewById(R.id.spielerb);
        final TextView pf1 = findViewById(R.id.pfeile1);
        final TextView pf2 = findViewById(R.id.pfeile2);
        final TextView ds1 = findViewById(R.id.durchschnitt1);
        final TextView ds2 = findViewById(R.id.durchschnitt2);
        final TextView spg1 = findViewById(R.id.spielegesamt1);
        final TextView spg2 = findViewById(R.id.spielegesamt2);
        final TextView spe1 = findViewById(R.id.einzel1);
        final TextView spe2 = findViewById(R.id.einzel2);
        final TextView siege1 = findViewById(R.id.siege1);
        final TextView siege2 = findViewById(R.id.siege2);
        final TextView u1801 = findViewById(R.id.u1801);
        final TextView u1802 = findViewById(R.id.u1802);
        final TextView u1401 = findViewById(R.id.u1401);
        final TextView u1402 = findViewById(R.id.u1402);
        final TextView u1001 = findViewById(R.id.u1001);
        final TextView u1002 = findViewById(R.id.u1002);
        final TextView u601 = findViewById(R.id.u601);
        final TextView u602 = findViewById(R.id.u602);
        final TextView besthit1 = findViewById(R.id.besterwurf1);
        final TextView besthit2 = findViewById(R.id.besterwurf2);
        final Button playera = findViewById(R.id.buttonspielera);
        final Button playerb = findViewById(R.id.buttonspielerb);
        final Button alle = findViewById(R.id.alle);
        final Button exportcsv = findViewById(R.id.exportcsv);
        final Button exportdat = findViewById(R.id.exportdat);
        final Button exporttxt = findViewById(R.id.exporttxt);
        final Button importdat = findViewById(R.id.importdat);
        final TextView setlegs1 = findViewById(R.id.setlegs1);
        final TextView setlegs2 = findViewById(R.id.setlegs2);
        final TextView check1 = findViewById(R.id.checkout1);
        final TextView check2 = findViewById(R.id.checkout2);
        final TextView setlegs1lost = findViewById(R.id.setlegs1lost);
        final TextView setlegs2lost = findViewById(R.id.setlegs2lost);
        final TextView quote1 = findViewById(R.id.quote1);
        final TextView quote2 = findViewById(R.id.quote2);

        spielera.setAdapter(arrayAdapter);
        spielerb.setAdapter(arrayAdapter);
        if (arrayAdapter.getCount() > 0) spielera.setSelection(0);
        if (arrayAdapter.getCount() > 1) spielerb.setSelection(1);

        playera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(String.format(getResources().getString(R.string.willstStatistik), spielera.getSelectedItem().toString()));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jal),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spielerdaten komplett löschen
                                int index = spielera.getSelectedItemPosition();
                                MainActivity.spieler dummyspieler = MainActivity.allespieler.get(index);
                                dummyspieler.durchschnitt = 0;
                                dummyspieler.geworfenePfeile = 0;
                                dummyspieler.AnzahlSpiele = 0;
                                dummyspieler.anzahlSiege = 0;
                                dummyspieler.AnzahlEinzelspiele = 0;
                                dummyspieler.anzahl180 = 0;
                                dummyspieler.anzahluber140 = 0;
                                dummyspieler.anzahluber100 = 0;
                                dummyspieler.anzahluber60 = 0;
                                dummyspieler.besterWurf = 0;
                                dummyspieler.checkoutmax = 0;
                                dummyspieler.matcheswon = 0;
                                dummyspieler.matcheslost = 0;
                                MainActivity.allespieler.set(index, dummyspieler);
                                MainActivity.speichern(statistik.this, false);
                                //anzeige aktualisieren
                                ds1.setText(formater.format(dummyspieler.durchschnitt));
                                pf1.setText(Integer.toString(dummyspieler.geworfenePfeile));
                                spg1.setText(Integer.toString(dummyspieler.AnzahlSpiele));
                                siege1.setText(Integer.toString(dummyspieler.anzahlSiege));
                                spe1.setText(Integer.toString(dummyspieler.AnzahlEinzelspiele));
                                u1801.setText(Integer.toString(dummyspieler.anzahl180));
                                u1401.setText(Integer.toString(dummyspieler.anzahluber140));
                                u1001.setText(Integer.toString(dummyspieler.anzahluber100));
                                u601.setText(Integer.toString(dummyspieler.anzahluber60));
                                besthit1.setText(Integer.toString(dummyspieler.besterWurf));
                                check1.setText(Integer.toString(dummyspieler.checkoutmax));
                                setlegs1.setText(Integer.toString(dummyspieler.matcheswon));
                                setlegs1lost.setText(Integer.toString(dummyspieler.matcheslost));
                                quote1.setText("0 %");

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                alertDialog.show();


            }
        });


        playerb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(String.format(getResources().getString(R.string.willstStatistik), spielerb.getSelectedItem().toString()));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jal),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spielerdaten komplett löschen
                                int index = spielerb.getSelectedItemPosition();
                                MainActivity.spieler dummyspieler = MainActivity.allespieler.get(index);
                                dummyspieler.durchschnitt = 0;
                                dummyspieler.geworfenePfeile = 0;
                                dummyspieler.AnzahlSpiele = 0;
                                dummyspieler.anzahlSiege = 0;
                                dummyspieler.AnzahlEinzelspiele = 0;
                                dummyspieler.anzahl180 = 0;
                                dummyspieler.anzahluber140 = 0;
                                dummyspieler.anzahluber100 = 0;
                                dummyspieler.anzahluber60 = 0;
                                dummyspieler.besterWurf = 0;
                                dummyspieler.checkoutmax = 0;
                                dummyspieler.matcheswon = 0;
                                dummyspieler.matcheslost = 0;
                                MainActivity.allespieler.set(index, dummyspieler);
                                MainActivity.speichern(statistik.this, false);
                                //anzeige aktualisieren
                                ds2.setText(formater.format(dummyspieler.durchschnitt));
                                pf2.setText(Integer.toString(dummyspieler.geworfenePfeile));
                                spg2.setText(Integer.toString(dummyspieler.AnzahlSpiele));
                                siege2.setText(Integer.toString(dummyspieler.anzahlSiege));
                                spe2.setText(Integer.toString(dummyspieler.AnzahlEinzelspiele));
                                u1802.setText(Integer.toString(dummyspieler.anzahl180));
                                u1402.setText(Integer.toString(dummyspieler.anzahluber140));
                                u1002.setText(Integer.toString(dummyspieler.anzahluber100));
                                u602.setText(Integer.toString(dummyspieler.anzahluber60));
                                besthit2.setText(Integer.toString(dummyspieler.besterWurf));
                                check2.setText(Integer.toString(dummyspieler.checkoutmax));
                                setlegs2.setText(Integer.toString(dummyspieler.matcheswon));
                                setlegs2lost.setText(Integer.toString(dummyspieler.matcheslost));
                                quote2.setText("0 %");

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                alertDialog.show();
            }
        });

        alle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.allel));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jal),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spielerdaten komplett löschen
                                MainActivity.spieler dummyspieler;
                                for (int index = 0; index < MainActivity.allespieler.size(); index++) {
                                    dummyspieler = MainActivity.allespieler.get(index);
                                    dummyspieler.durchschnitt = 0;
                                    dummyspieler.geworfenePfeile = 0;
                                    dummyspieler.AnzahlSpiele = 0;
                                    dummyspieler.anzahlSiege = 0;
                                    dummyspieler.AnzahlEinzelspiele = 0;
                                    dummyspieler.anzahl180 = 0;
                                    dummyspieler.anzahluber140 = 0;
                                    dummyspieler.anzahluber100 = 0;
                                    dummyspieler.anzahluber60 = 0;
                                    dummyspieler.besterWurf = 0;
                                    dummyspieler.checkoutmax = 0;
                                    dummyspieler.matcheswon = 0;
                                    dummyspieler.matcheslost = 0;
                                    MainActivity.allespieler.set(index, dummyspieler);
                                }

                                MainActivity.speichern(statistik.this, false);
                                //anzeige aktualisieren
                                ds1.setText("0");
                                pf1.setText("0");
                                spg1.setText("0");
                                siege1.setText("0");
                                spe1.setText("0");
                                u1801.setText("0");
                                u1401.setText("0");
                                u1001.setText("0");
                                u601.setText("0");
                                besthit1.setText("0");
                                check1.setText("0");
                                setlegs1.setText("0");
                                setlegs1lost.setText("0");
                                quote1.setText("0 %");

                                ds2.setText("0");
                                pf2.setText("0");
                                spg2.setText("0");
                                siege2.setText("0");
                                spe2.setText("0");
                                u1802.setText("0");
                                u1402.setText("0");
                                u1002.setText("0");
                                u602.setText("0");
                                besthit2.setText("0");
                                check2.setText("0");
                                setlegs2.setText("0");
                                setlegs2lost.setText("0");
                                quote2.setText("0 %");
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();


            }
        });


        spielera.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                MainActivity.spieler dummyspieler = MainActivity.allespieler.get(position);
                ds1.setText(formater.format(dummyspieler.durchschnitt));
                pf1.setText(Integer.toString(dummyspieler.geworfenePfeile));
                spg1.setText(Integer.toString(dummyspieler.AnzahlSpiele));
                siege1.setText(Integer.toString(dummyspieler.anzahlSiege));
                spe1.setText(Integer.toString(dummyspieler.AnzahlEinzelspiele));
                u1801.setText(Integer.toString(dummyspieler.anzahl180));
                u1401.setText(Integer.toString(dummyspieler.anzahluber140));
                u1001.setText(Integer.toString(dummyspieler.anzahluber100));
                u601.setText(Integer.toString(dummyspieler.anzahluber60));
                besthit1.setText(Integer.toString(dummyspieler.besterWurf));
                check1.setText(Integer.toString(dummyspieler.checkoutmax));
                setlegs1.setText(Integer.toString(dummyspieler.matcheswon));
                setlegs1lost.setText(Integer.toString(dummyspieler.matcheslost));
                if (!(dummyspieler.AnzahlSpiele == 0) && !(dummyspieler.AnzahlSpiele <= dummyspieler.AnzahlEinzelspiele)) {
                    float q11 = dummyspieler.AnzahlSpiele - dummyspieler.AnzahlEinzelspiele;
                    q11 = q11 / 100;
                    q11 = dummyspieler.anzahlSiege / q11;
                    quote1.setText(formater.format(q11) + " %");
                } else quote1.setText("0 %");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        spielerb.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                MainActivity.spieler dummyspieler2 = MainActivity.allespieler.get(position);
                ds2.setText(formater.format(dummyspieler2.durchschnitt));
                pf2.setText(Integer.toString(dummyspieler2.geworfenePfeile));
                spg2.setText(Integer.toString(dummyspieler2.AnzahlSpiele));
                siege2.setText(Integer.toString(dummyspieler2.anzahlSiege));
                spe2.setText(Integer.toString(dummyspieler2.AnzahlEinzelspiele));
                u1802.setText(Integer.toString(dummyspieler2.anzahl180));
                u1402.setText(Integer.toString(dummyspieler2.anzahluber140));
                u1002.setText(Integer.toString(dummyspieler2.anzahluber100));
                u602.setText(Integer.toString(dummyspieler2.anzahluber60));
                besthit2.setText(Integer.toString(dummyspieler2.besterWurf));
                check2.setText(Integer.toString(dummyspieler2.checkoutmax));
                setlegs2.setText(Integer.toString(dummyspieler2.matcheswon));
                setlegs2lost.setText(Integer.toString(dummyspieler2.matcheslost));
                if (!(dummyspieler2.AnzahlSpiele == 0) && !(dummyspieler2.AnzahlSpiele <= dummyspieler2.AnzahlEinzelspiele)) {
                    float q2 = dummyspieler2.AnzahlSpiele - dummyspieler2.AnzahlEinzelspiele;
                    q2 = q2 / 100;
                    q2 = dummyspieler2.anzahlSiege / q2;
                    quote2.setText(formater.format(q2) + " %");
                } else quote2.setText("0 %");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });





        exportcsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context ctx = getApplicationContext();
                String filepath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
                String fname = getFreeFileName(filepath, "dartchecker_statistics.csv");
                File fileName = new File(filepath, fname);
                try {
                    FileOutputStream fout = new FileOutputStream(fileName);
                    OutputStreamWriter csvout = new OutputStreamWriter(fout);
                    CsvConfiguration conf = new CsvConfiguration();
                    String delim = ";";
                    conf.setFieldDelimiter(delim.charAt(0));
                    Serializer csvheadserial = CsvIOFactory.createFactory(conf, MainActivity.heading.class).createSerializer();
                    csvheadserial.open(csvout);
                    MainActivity.heading heading = new MainActivity.heading();
                    heading.AnzahlEinzelspiele = getResources().getString(R.string.einzelspiele);
                    heading.anzahlSiege = getResources().getString(R.string.siege);
                    heading.AnzahlSpiele = getResources().getString(R.string.spiele_insgesamt);
                    heading.spielerName = getResources().getString(R.string.spieler);
                    heading.matcheswon = getResources().getString(R.string.matches_won);
                    heading.matcheslost = getResources().getString(R.string.matches_lost);
                    heading.geworfenePfeile = getResources().getString(R.string.geworfene_darts);
                    heading.anzahluber60 = getResources().getString(R.string._60);
                    heading.anzahluber100 = getResources().getString(R.string._100);
                    heading.anzahluber140 = getResources().getString(R.string._140);
                    heading.anzahl180 = getResources().getString(R.string._180);
                    heading.besterWurf = getResources().getString(R.string.beste_aufnahme);
                    heading.checkoutmax = getResources().getString(R.string.checkout);
                    heading.durchschnitt = getResources().getString(R.string.durchschnitt_3_dart);
                    csvheadserial.write(heading);
                    csvheadserial.close(false);
                    Serializer csvdataserial = CsvIOFactory.createFactory(conf, MainActivity.spieler.class).createSerializer();
                    csvdataserial.open(csvout);

                    for (MainActivity.spieler spieler : MainActivity.allespieler) {
                        csvdataserial.write(spieler);
                    }
                    csvdataserial.close(true);
                    Toast.makeText(ctx, "Exported to " + Environment.DIRECTORY_DOWNLOADS + "/" + fname, Toast.LENGTH_LONG).show();
                } catch (FileNotFoundException e) {
                    Toast.makeText(ctx, e + "Error saving: file not found", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (Exception ee) {
                    Toast.makeText(ctx, "Error: " + ee, Toast.LENGTH_LONG).show();
                }
            }
        });

        exportdat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context ctx = getApplicationContext();
                String filepath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
                String fname = getFreeFileName(filepath,"dartchecker_statistics_export.dat");
                File fileName = new File(filepath, fname);
                try {
                    FileOutputStream fout = new FileOutputStream(fileName);
                    ObjectOutputStream oos = new ObjectOutputStream(fout);
                    oos.writeObject(MainActivity.allespieler);
                    oos.flush();
                    oos.close();  //fileoutputstream wird automatisch mitgeschlossen
                    Toast.makeText(ctx, "Exported to " + Environment.DIRECTORY_DOWNLOADS + "/" + fname, Toast.LENGTH_LONG).show();
                } catch (FileNotFoundException e) {
                    Toast.makeText(ctx, e + "Error saving: file not found", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IOException i) {
                    Toast.makeText(ctx, i + "Error saving: Input/Output error", Toast.LENGTH_LONG).show();
                    i.printStackTrace();
                } catch (Exception ee) {
                    Toast.makeText(ctx, "Error: " + ee, Toast.LENGTH_LONG).show();
                }
            }
        });

        exporttxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context ctx = getApplicationContext();
                String filepath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
                String fname = getFreeFileName(filepath, "dartchecker_statistics.txt");
                File fileName = new File(filepath, fname);
                try {
                    FileOutputStream fout = new FileOutputStream(fileName);
                    OutputStreamWriter osw = new OutputStreamWriter(fout);
                    Date now = new Date();
                    osw.write(getResources().getString(R.string.textfileheading) + " " + now.toLocaleString() + "\n");

                    for (MainActivity.spieler spieler : MainActivity.allespieler) {
                        String wurfquote = "0 %";
                        if (!(spieler.AnzahlSpiele == 0) && !(spieler.AnzahlSpiele <= spieler.AnzahlEinzelspiele)) {
                            float quotetemp = spieler.AnzahlSpiele - spieler.AnzahlEinzelspiele;
                            quotetemp = quotetemp / 100;
                            quotetemp = spieler.anzahlSiege / quotetemp;
                            wurfquote = formater.format(quotetemp) + " %";
                        }
                        osw.write("\n" + getResources().getString(R.string.spieler) + ": " + spieler.spielerName + "\n");
                        osw.write(getResources().getString(R.string.spiele_insgesamt) + ": " + spieler.AnzahlSpiele + "\n");
                        osw.write(getResources().getString(R.string.einzelspiele) + ": " + spieler.AnzahlEinzelspiele + "\n");
                        osw.write(getResources().getString(R.string.siege) + ": " + spieler.anzahlSiege + "\n");
                        osw.write(getResources().getString(R.string.quote) + ": " + wurfquote + "\n");
                        osw.write(getResources().getString(R.string.matches_won) + ": " + spieler.matcheswon + "\n");
                        osw.write(getResources().getString(R.string.matches_lost) + ": " + spieler.matcheslost + "\n");
                        osw.write(getResources().getString(R.string.geworfene_darts) + ": " + spieler.geworfenePfeile + "\n");
                        osw.write(getResources().getString(R.string.durchschnitt_3_dart) + ": " + spieler.durchschnitt + "\n");
                        osw.write(getResources().getString(R.string._60) + ": " + spieler.anzahluber60 + "\n");
                        osw.write(getResources().getString(R.string._100) + ": " + spieler.anzahluber100 + "\n");
                        osw.write(getResources().getString(R.string._140) + ": " + spieler.anzahluber140 + "\n");
                        osw.write(getResources().getString(R.string._180) + ": " + spieler.anzahl180 + "\n");
                        osw.write(getResources().getString(R.string.beste_aufnahme) + ": " + spieler.besterWurf + "\n");
                        osw.write(getResources().getString(R.string.checkout) + ": " + spieler.checkoutmax + "\n");
                    }
                    osw.close();  //fileoutputstream wird automatisch mitgeschlossen
                    Toast.makeText(ctx, "Exported to " + Environment.DIRECTORY_DOWNLOADS + "/" + fname, Toast.LENGTH_LONG).show();
                } catch (FileNotFoundException e) {
                    Toast.makeText(ctx, e + "Error saving: file not found", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IOException i) {
                    Toast.makeText(ctx, i + "Error saving: Input/Output error", Toast.LENGTH_LONG).show();
                    i.printStackTrace();
                } catch (Exception ee) {
                    Toast.makeText(ctx, "Error: " + ee, Toast.LENGTH_LONG).show();
                }
            }
        });

        importdat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.selectfile)), 111);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.selectfile), Toast.LENGTH_LONG).show();
            }
        });
    }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if(requestCode == 111 && resultCode == RESULT_OK) {
                Uri selectedfile = data.getData();
                String fileName = selectedfile.getPath();
                String fileextension = fileName.substring(fileName.length()-4, fileName.length()).toLowerCase();
                if (!fileextension.equals(".dat")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.nodatfile),Toast.LENGTH_LONG).show();
                    return;
                }
                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.willstduimport));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ja),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // import player data
                                try {
                                        ContentResolver cr = getContentResolver();
                                        InputStream in = cr.openInputStream(selectedfile);
                                        ObjectInputStream is = new ObjectInputStream(in);
                                        @SuppressWarnings("unchecked") ArrayList<MainActivity.spieler> returnlist = (ArrayList<MainActivity.spieler>) is.readObject();
                                        is.close();
                                        in.close();
                                        MainActivity.allespieler = returnlist;
                                        MainActivity.speichern(statistik.this, false);
                                        finish();
                                        overridePendingTransition(0, 0);
                                        startActivity(getIntent());
                                        overridePendingTransition(0, 0);
                                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.importsuccessful),Toast.LENGTH_LONG).show();
                                    } catch (FileNotFoundException e) {
                                        Toast.makeText(getApplicationContext(), e + "Error loading: file not found", Toast.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    } catch (ClassNotFoundException b) {
                                        Toast.makeText(getApplicationContext(), b + "Error loading: class not found", Toast.LENGTH_LONG).show();
                                        b.printStackTrace();
                                    } catch (IOException i) {
                                        Toast.makeText(getApplicationContext(), i + "Error loading: Input/Output", Toast.LENGTH_LONG).show();
                                        i.printStackTrace();
                                    }
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }

    private String getFreeFileName(String filepath, String fname ){
        boolean exists = true;
        int count = 0;
        File fileName = new File(filepath,fname);
        String fileextension = fname.substring(fname.length()-4);
        String name = fname.substring(0, fname.length()-4);
        while (exists) {
            fileName = new File(filepath, fname);
            if (fileName.exists()) {
                count++;
                fname = name+count+fileextension;
            }
            else exists = false;
        }
        return fname;
    }
}
