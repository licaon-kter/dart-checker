package com.DartChecker;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import java.util.ArrayList;
import java.util.Locale;

public class roundtclock extends AppCompatActivity {
    private final player[] spieler = new player[9];
    private final ArrayList<pfeil> wuerfe = new ArrayList<pfeil>();
    private long startTime = 0;
    private int bcolor;
    private int bcolorn;
    private int textcoloraktiv,
            textcolorpassiv;
    private float textsizeaktiv,
            textsizepassiv;
    private int spieleranzahl,
            aktiverSpieler = 1,       //index für namensarray
            xdart = 0,                //zähler für geworfene darts (0, 1 oder 2) pro runde
            aufnahme,
            changetime = 1500,
            undoIndex = -1;                   //index für undo-speicher
    private String s = "";
    private CharSequence spielmodus;
    private boolean undoabfrage = true; //soll die undoabfrage beim undoclick erscheinen oder undo() ohne rückfrage durchgeführt werden - wichtig für frage beim letzten wurf

    // undo
    private final View.OnClickListener undoclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // sicherheitsabfrage
             if (undoabfrage) {
                AlertDialog alertDialog = new AlertDialog.Builder(roundtclock.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.willstduUndo));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                undo();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
                 final SharedPreferences settings = getApplicationContext().getSharedPreferences("Einstellungen", 0);
                 if (settings.contains("Undosicherheitsabfrage")) {
                     if (!settings.getBoolean("Undosicherheitsabfrage",false)) {
                         alertDialog.hide();
                         alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                     }
                 }
            } else {
                undoabfrage = true;
                undo();
            }
        }

        private void undo() {
            int dart;
            TextView darts = findViewById(R.id.darts);

            if (undoIndex >= 0) {
                if (xdart == 0) {
                    //spielerwechsel
                    xdart = 2;
                    s = "..";
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);

                    aktiverSpieler = wuerfe.get(undoIndex).spielerindex;   //der letzte Spieler der geworfen hat
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setPaintFlags(textfeld(1, aktiverSpieler, 3).getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    textfeld(1, aktiverSpieler, 2).setPaintFlags(textfeld(1, aktiverSpieler, 2).getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);

                    ConstraintLayout cl = findViewById(R.id.matchbereich);
                    ConstraintSet csdart = new ConstraintSet();
                    csdart.clone(cl);
                    csdart.connect(darts.getId(), ConstraintSet.END, textfeld(1,aktiverSpieler,3).getId(), ConstraintSet.END);
                    csdart.connect(darts.getId(), ConstraintSet.START, textfeld(1,aktiverSpieler,3).getId(), ConstraintSet.START);
                    csdart.applyTo(cl);


                    if (spieler[aktiverSpieler].gewinnerplatz != 0)
                        spieler[aktiverSpieler].gewinnerplatz = 0;
                } else xdart--;

                if (xdart == 1) s = ".";
                else if (xdart == 0) s = "";
                darts.setText(s);
                dart = wuerfe.get(undoIndex).zahl;

                String str;
                switch (dart) {
                    case 0:
                        str = "undo: " + getResources().getString(R.string.daneben);
                        break;
                    case 25:
                        str = "undo: Bull";
                        break;
                    default:
                        str = "undo: " + dart;
                        break;
                }
                str = str + " " + getResources().getString(R.string.von_spieler) + " " + spieler[wuerfe.get(undoIndex).spielerindex].spielerName;
                Toast.makeText(roundtclock.this, str, Toast.LENGTH_SHORT).show();

                // hide (all) buttons
                for (int b = 1; b <= 20; b++) {
                    buttonselect(b).setVisibility(View.GONE);
                }
                buttonselect(25).setVisibility(View.GONE);

                // undo() a missed throw: get score from actual player and get/show next segment to hit
                if (dart == 0) {
                        dart = spieler[aktiverSpieler].score;

                    if (spielmodus.equals("1-20") && dart < 20) {
                        buttonselect(dart + 1).setVisibility(View.VISIBLE);
                    }
                    if (spielmodus.equals("20-1")) {
                        if (dart > 0) {
                            buttonselect(dart - 1).setVisibility(View.VISIBLE);
                        } else {
                            buttonselect(20).setVisibility(View.VISIBLE);
                        }
                    }
                    if (spielmodus.equals("1-Bull") && dart < 25) {
                        if (dart == 20) {
                            buttonselect(25).setVisibility(View.VISIBLE);
                        } else {
                            buttonselect(dart + 1).setVisibility(View.VISIBLE);
                        }
                    }
                    if (spielmodus.equals("Bull-1")) {
                        switch (dart) {
                            case 0:
                                buttonselect(25).setVisibility(View.VISIBLE);
                                break;
                            case 25:
                                buttonselect(20).setVisibility(View.VISIBLE);
                                break;
                            default:
                                buttonselect(dart - 1).setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                }
                // regular undo(): get/show last segment to hit
                else {
                    spieler[aktiverSpieler].treffer[dart] = 0;
                    buttonselect(dart).setVisibility(View.VISIBLE);

                    if (spielmodus.equals("1-20")) {
                        if (dart > 0) {
                            dart--;
                        }
                    }
                    if (spielmodus.equals("20-1")) {
                        if (dart <= 20) {
                            dart++;
                        }
                    }
                    if (spielmodus.equals("1-Bull")) {
                        if (dart == 25) {
                            dart = 20;
                        } else if (dart > 0) {
                            dart--;
                        }
                    }
                    Boolean changed = false;
                    if (spielmodus.equals("Bull-1")) {
                        if (dart == 20) {
                            dart = 25;
                            changed = true;
                        } else if (dart < 25) {
                            dart++;
                        }

                    }
                    spieler[aktiverSpieler].score = dart;

                    //update score textviews
                    switch (dart) {
                        case 25:
                            if (!changed)
                                textfeld(1, aktiverSpieler, 2).setText("0");
                            else
                                textfeld(1, aktiverSpieler, 2).setText("Bull");
                            break;
                        case 21:
                            if (spielmodus.equals("20-1"))
                                textfeld(1, aktiverSpieler, 2).setText("0");
                            break;
                        default:
                            textfeld(1, aktiverSpieler, 2).setText(Integer.toString(dart));
                    }
                }

                spieler[aktiverSpieler].darts--;
                undoIndex--;
            }
            else Toast.makeText(roundtclock.this, R.string.keinundo, Toast.LENGTH_LONG).show();
        }
    };

    private final View.OnClickListener weiter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button daneben=findViewById(R.id.daneben);
            switch (xdart) {
                case 0:
                    daneben.performClick();
                case 1:
                    daneben.performClick();
                case 2:
                    daneben.performClick();
            }
        }
    };

    private final View.OnClickListener buttonclick = new View.OnClickListener() {
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {

            int sender = v.getId();
            Button eingabe = findViewById(sender);
            int dart,
                addpunkte = 0;

            // welche taste wurde gedrückt?
            switch (sender) {
                case R.id.bull:
                    dart = 25;
                    break;
                case R.id.daneben:
                    dart = 0;
                    break;
                default:
                    try {
                        dart = Integer.parseInt(eingabe.getText().toString());
                    } catch (Exception e) {
                        dart = 255;         //fehlerwert - sollte niemals eintreten
                    }
                    break;
            }

            pfeil aktuellerWurf;
            if (dart != 0) {
                int i;
                // save data for undo()
                aktuellerWurf = new pfeil();
                undoIndex++;
                aktuellerWurf.addpunkte = addpunkte;
                aktuellerWurf.spielerindex = aktiverSpieler;
                aktuellerWurf.zahl = dart;
                wuerfe.add(undoIndex, aktuellerWurf);
                // hide actual button, display next button
                buttonselect(dart).setVisibility(View.GONE);
                if (spielmodus.equals("1-20") && dart < 20) {
                    buttonselect(dart + 1).setVisibility(View.VISIBLE);
                }
                if (spielmodus.equals("20-1") && dart > 1) {
                    buttonselect(dart - 1).setVisibility(View.VISIBLE);
                }
                if (spielmodus.equals("1-Bull") && dart < 25) {
                    if (dart == 20) {
                        buttonselect(25).setVisibility(View.VISIBLE);
                    } else {
                        buttonselect(dart + 1).setVisibility(View.VISIBLE);
                    }
                }
                if (spielmodus.equals("Bull-1") && dart > 1) {
                    if (dart == 25) {
                        buttonselect(20).setVisibility(View.VISIBLE);
                    } else {
                        buttonselect(dart - 1).setVisibility(View.VISIBLE);
                    }
                }

                // update player sore and corresponding textview
                spieler[aktiverSpieler].darts++;
                spieler[aktiverSpieler].treffer[dart]++;
                spieler[aktiverSpieler].score=dart;
                if (dart == 25) {
                    textfeld(1, aktiverSpieler, 2).setText("Bull");
                }
                else {
                    textfeld(1, aktiverSpieler, 2).setText(Integer.toString(dart));
                }

                //spiel zuende?
                if (gewinner(aktiverSpieler)) {
                    AlertDialog alertDialog = new AlertDialog.Builder(roundtclock.this).create();
                    alertDialog.setTitle(getResources().getString(R.string.achtung));
                    alertDialog.setMessage(getResources().getString(R.string.letzteEingabeKorrekt));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ja),
                            (dialog, which) -> {
                                dialog.dismiss();
                                if (gewinner(aktiverSpieler)) {
                                    //spielende einleiten und aufrufen
                                    long spielzeit = (System.currentTimeMillis() - startTime) / 1000;
                                    Intent intent = new Intent(roundtclock.this, spielende.class);
                                    intent.putExtra("anzahl", spieleranzahl);
                                    intent.putExtra("roundtclock", true);
                                    intent.putExtra("spielzeit", spielzeit);

                                    intent.putExtra("erster", spieler[aktiverSpieler].spielerName);
                                    intent.putExtra("ersterscore", spieler[aktiverSpieler].score);

                                    if (spieleranzahl == 2) {
                                        switch (aktiverSpieler) {
                                            case 1:
                                                intent.putExtra("zweiter", spieler[2].spielerName);
                                                intent.putExtra("zweiterscore", spieler[2].score);
                                                break;
                                            case 2:
                                                intent.putExtra("zweiter", spieler[1].spielerName);
                                                intent.putExtra("zweiterscore", spieler[1].score);
                                                break;
                                        }

                                    } else if (spieleranzahl == 3) {
                                        player s2 = new player(), s3 = new player();
                                        switch (aktiverSpieler) {
                                            case 1:
                                                s2 = spieler[2];
                                                s3 = spieler[3];
                                                break;
                                            case 2:
                                                s2 = spieler[1];
                                                s3 = spieler[3];
                                                break;
                                            case 3:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                break;
                                        }
                                        intent.putExtra("zweiter", s2.spielerName);
                                        intent.putExtra("zweiterscore", s2.score);
                                        intent.putExtra("dritter", s3.spielerName);
                                        intent.putExtra("dritterscore", s3.score);

                                    } else if (spieleranzahl == 4) {

                                        player s2 = new player(), s3 = new player(), s4 = new player();

                                        switch (aktiverSpieler) {
                                            case 1:
                                                s2 = spieler[2];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                break;
                                            case 2:
                                                s2 = spieler[1];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                break;
                                            case 3:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[4];
                                                break;
                                            case 4:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                break;
                                        }
                                        intent.putExtra("zweiter", s2.spielerName);
                                        intent.putExtra("zweiterscore", s2.score);
                                        intent.putExtra("dritter", s3.spielerName);
                                        intent.putExtra("dritterscore", s3.score);
                                        intent.putExtra("vierter", s4.spielerName);
                                        intent.putExtra("vierterscore", s4.score);

                                    } else if (spieleranzahl == 5) {

                                        player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player();

                                        switch (aktiverSpieler) {
                                            case 1:
                                                s2 = spieler[2];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                break;
                                            case 2:
                                                s2 = spieler[1];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                break;
                                            case 3:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                break;
                                            case 4:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[5];
                                                break;
                                            case 5:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                break;
                                        }
                                        intent.putExtra("zweiter", s2.spielerName);
                                        intent.putExtra("zweiterscore", s2.score);
                                        intent.putExtra("dritter", s3.spielerName);
                                        intent.putExtra("dritterscore", s3.score);
                                        intent.putExtra("vierter", s4.spielerName);
                                        intent.putExtra("vierterscore", s4.score);
                                        intent.putExtra("fuenfter", s5.spielerName);
                                        intent.putExtra("fuenfterscore", s5.score);

                                    } else if (spieleranzahl == 6) {

                                        player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player(), s6 = new player();

                                        switch (aktiverSpieler) {
                                            case 1:
                                                s2 = spieler[2];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                break;
                                            case 2:
                                                s2 = spieler[1];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                break;
                                            case 3:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                break;
                                            case 4:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                break;
                                            case 5:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s6 = spieler[6];
                                                break;
                                            case 6:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s6 = spieler[5];
                                                break;
                                        }
                                        intent.putExtra("zweiter", s2.spielerName);
                                        intent.putExtra("zweiterscore", s2.score);
                                        intent.putExtra("dritter", s3.spielerName);
                                        intent.putExtra("dritterscore", s3.score);
                                        intent.putExtra("vierter", s4.spielerName);
                                        intent.putExtra("vierterscore", s4.score);
                                        intent.putExtra("fuenfter", s5.spielerName);
                                        intent.putExtra("fuenfterscore", s5.score);
                                        intent.putExtra("sechster", s6.spielerName);
                                        intent.putExtra("sechsterscore", s6.score);

                                    } else if (spieleranzahl == 7) {

                                        player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player(), s6 = new player(), s7 = new player();

                                        switch (aktiverSpieler) {
                                            case 1:
                                                s2 = spieler[2];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                break;
                                            case 2:
                                                s2 = spieler[1];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                break;
                                            case 3:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                break;
                                            case 4:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                break;
                                            case 5:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                break;
                                            case 6:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s7 = spieler[7];
                                                s6 = spieler[5];
                                                break;
                                            case 7:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s7 = spieler[6];
                                                s6 = spieler[5];
                                                break;
                                        }
                                        intent.putExtra("zweiter", s2.spielerName);
                                        intent.putExtra("zweiterscore", s2.score);
                                        intent.putExtra("dritter", s3.spielerName);
                                        intent.putExtra("dritterscore", s3.score);
                                        intent.putExtra("vierter", s4.spielerName);
                                        intent.putExtra("vierterscore", s4.score);
                                        intent.putExtra("fuenfter", s5.spielerName);
                                        intent.putExtra("fuenfterscore", s5.score);
                                        intent.putExtra("sechster", s6.spielerName);
                                        intent.putExtra("sechsterscore", s6.score);
                                        intent.putExtra("siebenter", s7.spielerName);
                                        intent.putExtra("siebenterscore", s7.score);

                                    } else if (spieleranzahl == 8) {

                                        player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player(), s6 = new player(), s7 = new player(), s8 = new player();

                                        switch (aktiverSpieler) {
                                            case 1:
                                                s2 = spieler[2];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                s8 = spieler[8];
                                                break;
                                            case 2:
                                                s2 = spieler[1];
                                                s3 = spieler[3];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                s8 = spieler[8];
                                                break;
                                            case 3:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[4];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                s8 = spieler[8];
                                                break;
                                            case 4:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[5];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                s8 = spieler[8];
                                                break;
                                            case 5:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s6 = spieler[6];
                                                s7 = spieler[7];
                                                s8 = spieler[8];
                                                break;
                                            case 6:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s7 = spieler[7];
                                                s6 = spieler[5];
                                                s8 = spieler[8];
                                                break;
                                            case 7:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s7 = spieler[6];
                                                s6 = spieler[5];
                                                s8 = spieler[8];
                                                break;
                                            case 8:
                                                s2 = spieler[1];
                                                s3 = spieler[2];
                                                s4 = spieler[3];
                                                s5 = spieler[4];
                                                s7 = spieler[6];
                                                s6 = spieler[5];
                                                s8 = spieler[7];
                                                break;
                                        }
                                        intent.putExtra("zweiter", s2.spielerName);
                                        intent.putExtra("zweiterscore", s2.score);
                                        intent.putExtra("dritter", s3.spielerName);
                                        intent.putExtra("dritterscore", s3.score);
                                        intent.putExtra("vierter", s4.spielerName);
                                        intent.putExtra("vierterscore", s4.score);
                                        intent.putExtra("fuenfter", s5.spielerName);
                                        intent.putExtra("fuenfterscore", s5.score);
                                        intent.putExtra("sechster", s6.spielerName);
                                        intent.putExtra("sechsterscore", s6.score);
                                        intent.putExtra("siebenter", s7.spielerName);
                                        intent.putExtra("siebenterscore", s7.score);
                                        intent.putExtra("achter", s8.spielerName);
                                        intent.putExtra("achterscore", s8.score);
                                    }

                                    startActivity(intent);
                                    finish();
                                    return;
                                }
                                TextView darts = findViewById(R.id.darts);
                                if (xdart < 2) {
                                    xdart++;
                                    s = s + (".");

                                } else {  //Spielerwechsel
                                    xdart = 0;
                                    s = "";

                                    if ((aufnahme > 0) || (spieler[aktiverSpieler].gewinnerplatz != 0)) {
                                        buttonfreeze(true);
                                        final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                                        if (spieler[aktiverSpieler].gewinnerplatz != 0)
                                            aufnahmetv.setText(spieler[aktiverSpieler].gewinnerplatz + ".");
                                        else aufnahmetv.setText(Integer.toString(aufnahme));
                                        aufnahmetv.setVisibility(View.VISIBLE);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                buttonfreeze(false);
                                                aufnahmetv.setVisibility(View.INVISIBLE);
                                            }
                                        }, changetime);
                                        aufnahme = 0;
                                    }
                                    if (spieler[aktiverSpieler].gewinnerplatz != 0) {
                                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                    } else {
                                        textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                                        textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                    }
                                    do {
                                        if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                                        else aktiverSpieler = 1;
                                    } while (spieler[aktiverSpieler].gewinnerplatz != 0);    // für cricket über die volle distanz
                                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                                }
                                darts.setText(s);
                                ConstraintLayout cl = findViewById(R.id.matchbereich);
                                ConstraintSet csdart = new ConstraintSet();
                                csdart.clone(cl);
                                csdart.connect(darts.getId(), ConstraintSet.END, textfeld(1,aktiverSpieler,3).getId(), ConstraintSet.END);
                                csdart.connect(darts.getId(), ConstraintSet.START, textfeld(1,aktiverSpieler,3).getId(), ConstraintSet.START);
                                csdart.applyTo(cl);
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            // letzter Wurf war eine Fehleingabe -> Spiel nicht beenden und letzte Eingabe zurücknehmen ohne nochmal nachzufragen
                            TextView darts = findViewById(R.id.darts);
                            if (xdart < 2) {
                                xdart++;
                                s = s + (".");

                            } else {  //Spielerwechsel
                                xdart = 0;
                                s = "";
                            }
                            darts.setText(s);

                            undoabfrage = false;
                            Button undo = findViewById(R.id.undo);
                            undo.performClick();
                        }
                    });

                    alertDialog.show();
                    return;
                }
            }
            // kein Treffer
            else if (!gewinner(aktiverSpieler)) {
                undoIndex++;
                aktuellerWurf = new pfeil();
                aktuellerWurf.addpunkte = 0;
                aktuellerWurf.zahl = dart;
                aktuellerWurf.spielerindex = aktiverSpieler;
                wuerfe.add(undoIndex, aktuellerWurf);
            }
                // Spiel geht weiter

                TextView darts = findViewById(R.id.darts);
                if (xdart < 2) {
                    xdart++;
                    s = s + (".");

                } else {
                    //Spielerwechsel
                    xdart = 0;
                    s = "";

                    if ((aufnahme > 0) || (spieler[aktiverSpieler].gewinnerplatz != 0)) {
                        buttonfreeze(true);
                        final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                        if (spieler[aktiverSpieler].gewinnerplatz != 0)
                            aufnahmetv.setText(spieler[aktiverSpieler].gewinnerplatz + ".");
                        else aufnahmetv.setText(Integer.toString(aufnahme));
                        aufnahmetv.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                buttonfreeze(false);
                                aufnahmetv.setVisibility(View.INVISIBLE);
                            }
                        }, changetime);
                        aufnahme = 0;
                    }
                    if (spieler[aktiverSpieler].gewinnerplatz != 0) {
                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    } else {
                        textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                        textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    }
                    // hide button from last/actual player
                    for (int b=1;b<=20;b++) {
                        buttonselect(b).setVisibility(View.GONE);
                    }
                    buttonselect(25).setVisibility(View.GONE);
                    if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                    else aktiverSpieler = 1;
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    dart = spieler[aktiverSpieler].score;
                    if (spielmodus.equals("1-20") && dart < 20) {
                        buttonselect(dart + 1).setVisibility(View.VISIBLE);
                    }
                    if (spielmodus.equals("20-1")) {
                        if (dart > 0) {
                            buttonselect(dart - 1).setVisibility(View.VISIBLE);
                        } else {
                            buttonselect(20).setVisibility(View.VISIBLE);
                        }
                    }
                    if (spielmodus.equals("1-Bull") && dart < 25) {
                        if (dart == 20) {
                            buttonselect(25).setVisibility(View.VISIBLE);
                        } else {
                            buttonselect(dart + 1).setVisibility(View.VISIBLE);
                        }
                    }
                    if (spielmodus.equals("Bull-1")) {
                        switch (dart) {
                            case 0:
                                buttonselect(25).setVisibility(View.VISIBLE);
                                break;
                            case 25:
                                buttonselect(20).setVisibility(View.VISIBLE);
                                break;
                            default:
                                buttonselect(dart - 1).setVisibility(View.VISIBLE);
                                break;
                        }
                    }

                }
                darts.setText(s);
                ConstraintLayout cl = findViewById(R.id.matchbereich);
                ConstraintSet csdart = new ConstraintSet();
                csdart.clone(cl);
                csdart.connect(darts.getId(), ConstraintSet.END, textfeld(1,aktiverSpieler,3).getId(), ConstraintSet.END);
                csdart.connect(darts.getId(), ConstraintSet.START, textfeld(1,aktiverSpieler,3).getId(), ConstraintSet.START);
                csdart.applyTo(cl);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_roundtclock);
        TypedValue outValue = new TypedValue();
        roundtclock.this.getTheme().resolveAttribute(R.attr.colorButtonNormal, outValue, true);
        bcolor = outValue.data;
        roundtclock.this.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
        bcolorn = outValue.data;
        SharedPreferences settings = getApplicationContext().getSharedPreferences("Einstellungen", 0);
        if (settings.contains("changetime")) changetime = settings.getInt("changetime", 1500);
        ConstraintLayout main = findViewById(R.id.matchbereich);
        if (settings.contains("keepscreenongame")) main.setKeepScreenOn(settings.getBoolean("keepscreenongame",true));

        TextView p1name = findViewById(R.id.p1name);
        TextView p2name = findViewById(R.id.p2name);
        textcoloraktiv = p1name.getCurrentTextColor();
        textcolorpassiv = p2name.getCurrentTextColor();
        textsizepassiv = p2name.getTextSize();
        textsizeaktiv = textsizepassiv + 4;
        textfeld(1, 1, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
        textfeld(1, 1, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);

        TextView darts = findViewById(R.id.darts);
        ConstraintSet csdart = new ConstraintSet();
        csdart.clone(main);
        csdart.connect(darts.getId(), ConstraintSet.END, R.id.p1name, ConstraintSet.END);
        csdart.connect(darts.getId(), ConstraintSet.START, R.id.p1name, ConstraintSet.START);
        csdart.applyTo(main);


        Intent intent = getIntent();
        spieleranzahl = Integer.parseInt(intent.getStringExtra("spieleranzahl"));
        spielmodus = intent.getCharSequenceExtra("spielmodus");
        CharSequence spieler1n = intent.getCharSequenceExtra("spieler1");
        CharSequence spieler2n = intent.getCharSequenceExtra("spieler2");
        CharSequence spieler3n = intent.getCharSequenceExtra("spieler3");
        CharSequence spieler4n = intent.getCharSequenceExtra("spieler4");
        CharSequence spieler5n = intent.getCharSequenceExtra("spieler5");
        CharSequence spieler6n = intent.getCharSequenceExtra("spieler6");
        CharSequence spieler7n = intent.getCharSequenceExtra("spieler7");
        CharSequence spieler8n = intent.getCharSequenceExtra("spieler8");

        player s1 = new player();
        player s2 = new player();
        player s3 = new player();
        player s4 = new player();
        player s5 = new player();
        player s6 = new player();
        player s7 = new player();
        player s8 = new player();

        spieler[1] = s1;
        spieler[2] = s2;
        spieler[3] = s3;
        spieler[4] = s4;
        spieler[5] = s5;
        spieler[6] = s6;
        spieler[7] = s7;
        spieler[8] = s8;
        for (int a = 1; a <= spieleranzahl; a++) spieler[a].score = 0;

        spieler[1].spielerName = spieler1n.toString();
        spieler[2].spielerName = spieler2n.toString();
        spieler[3].spielerName = spieler3n.toString();
        spieler[4].spielerName = spieler4n.toString();
        spieler[5].spielerName = spieler5n.toString();
        spieler[6].spielerName = spieler6n.toString();
        spieler[7].spielerName = spieler7n.toString();
        spieler[8].spielerName = spieler8n.toString();

        TextView p3name = findViewById(R.id.p3name);
        TextView p4name = findViewById(R.id.p4name);
        TextView p5name = findViewById(R.id.p5name);
        TextView p6name = findViewById(R.id.p6name);
        TextView p7name = findViewById(R.id.p7name);
        TextView p8name = findViewById(R.id.p8name);
        TextView p2score = findViewById(R.id.p2score);
        TextView p3score = findViewById(R.id.p3score);
        TextView p4score = findViewById(R.id.p4score);
        TextView p5score = findViewById(R.id.p5score);
        TextView p6score = findViewById(R.id.p6score);
        TextView p7score = findViewById(R.id.p7score);
        TextView p8score = findViewById(R.id.p8score);

        // Namenszuweisung der Namensanzeige in Abhängigkeit von der Anzahl der Spieler ändern
        p1name.setText(spieler[1].spielerName);
        p2name.setVisibility(View.GONE);
        p3name.setVisibility(View.GONE);
        p4name.setVisibility(View.GONE);
        p5name.setVisibility(View.GONE);
        p6name.setVisibility(View.GONE);
        p7name.setVisibility(View.GONE);
        p8name.setVisibility(View.GONE);
        p2score.setVisibility(View.GONE);
        p3score.setVisibility(View.GONE);
        p4score.setVisibility(View.GONE);
        p5score.setVisibility(View.GONE);
        p6score.setVisibility(View.GONE);
        p7score.setVisibility(View.GONE);
        p8score.setVisibility(View.GONE);
        switch (spieleranzahl) {
            case 8:
                p8score.setVisibility(View.VISIBLE);
                p8name.setVisibility(View.VISIBLE);
                p8name.setText(spieler[8].spielerName);
            case 7:
                p7score.setVisibility(View.VISIBLE);
                p7name.setVisibility(View.VISIBLE);
                p7name.setText(spieler[7].spielerName);
            case 6:
                p6score.setVisibility(View.VISIBLE);
                p6name.setVisibility(View.VISIBLE);
                p6name.setText(spieler[6].spielerName);
            case 5:
                p5score.setVisibility(View.VISIBLE);
                p5name.setVisibility(View.VISIBLE);
                p5name.setText(spieler[5].spielerName);
            case 4:
                p4score.setVisibility(View.VISIBLE);
                p4name.setVisibility(View.VISIBLE);
                p4name.setText(spieler[4].spielerName);
            case 3:
                p3name.setVisibility(View.VISIBLE);
                p3score.setVisibility(View.VISIBLE);
                p3name.setText(spieler[3].spielerName);
            case 2:
                p2name.setVisibility(View.VISIBLE);
                p2score.setVisibility(View.VISIBLE);
                p2name.setText(spieler[2].spielerName);
        }

        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button daneben = findViewById(R.id.daneben);
        Button restdaneben = findViewById(R.id.weiter);
        Button bundo = findViewById(R.id.undo);
        b1.setOnClickListener(buttonclick);
        b2.setOnClickListener(buttonclick);
        b3.setOnClickListener(buttonclick);
        b4.setOnClickListener(buttonclick);
        b5.setOnClickListener(buttonclick);
        b6.setOnClickListener(buttonclick);
        b7.setOnClickListener(buttonclick);
        b8.setOnClickListener(buttonclick);
        b9.setOnClickListener(buttonclick);
        b10.setOnClickListener(buttonclick);
        b11.setOnClickListener(buttonclick);
        b12.setOnClickListener(buttonclick);
        b13.setOnClickListener(buttonclick);
        b14.setOnClickListener(buttonclick);
        b15.setOnClickListener(buttonclick);
        b16.setOnClickListener(buttonclick);
        b17.setOnClickListener(buttonclick);
        b18.setOnClickListener(buttonclick);
        b19.setOnClickListener(buttonclick);
        b20.setOnClickListener(buttonclick);
        daneben.setOnClickListener(buttonclick);
        restdaneben.setOnClickListener(weiter);
        bull.setOnClickListener(buttonclick);
        bundo.setOnClickListener(undoclick);


        //Layout in Abhängikeit vom spielmodus ändern
        b1.setVisibility(View.GONE);
        b2.setVisibility(View.GONE);
        b3.setVisibility(View.GONE);
        b4.setVisibility(View.GONE);
        b5.setVisibility(View.GONE);
        b6.setVisibility(View.GONE);
        b7.setVisibility(View.GONE);
        b8.setVisibility(View.GONE);
        b9.setVisibility(View.GONE);
        b10.setVisibility(View.GONE);
        b11.setVisibility(View.GONE);
        b12.setVisibility(View.GONE);
        b13.setVisibility(View.GONE);
        b14.setVisibility(View.GONE);
        b15.setVisibility(View.GONE);
        b16.setVisibility(View.GONE);
        b17.setVisibility(View.GONE);
        b18.setVisibility(View.GONE);
        b19.setVisibility(View.GONE);
        b20.setVisibility(View.GONE);
        bull.setVisibility(View.GONE);

        if (spielmodus.equals("1-20") || spielmodus.equals("1-Bull")) {
            b1.setVisibility(View.VISIBLE);
        }
        if (spielmodus.equals("20-1")) {
            b20.setVisibility(View.VISIBLE);
        }
        if (spielmodus.equals("Bull-1")) {
            bull.setVisibility(View.VISIBLE);
        }
        findViewById(android.R.id.content).invalidate();
        startTime = System.currentTimeMillis();
    }

    private Button buttonselect(int buttonNumber) {
        String buttonID = "b" + buttonNumber;
        if (buttonNumber == 25)
            buttonID = "bull";
        int buttonObjectID = getResources().getIdentifier(buttonID, "id", getPackageName());
        return findViewById(buttonObjectID);
    }

    // liefert das gewünschte textview zurück
    private TextView textfeld(int dart, int aktiverSpieler, int typ)
    // typ=1 -> trefferfeld
    // typ=2 -> scorefeld
    // typ=3 -> namensfeld
    {
        String stt = "";
        switch (typ) {
            case 1:
                if (dart == 25) {
                    stt = "tbull" + "p" + aktiverSpieler;
                } else {
                    stt = "t" + dart + "p" + aktiverSpieler;
                }
                break;
            case 2:
                stt = "p" + aktiverSpieler + "score";
                break;
            case 3:
                stt = "p" + aktiverSpieler + "name";
                break;
        }
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }


    private boolean gewinner(int aktSpieler) {
        int endziel=0;
        if (spielmodus.equals("Bull-1") || spielmodus.equals("20-1")) {
            endziel = 1;
        }
        if  (spielmodus.equals("1-20")) {
            endziel = 20;
        }
        if (spielmodus.equals("1-Bull")) {
            endziel = 25;
        }
        return spieler[aktSpieler].treffer[endziel] > 0;
    }

    private void buttonfreeze(boolean freeze) {
        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button restdaneben = findViewById(R.id.weiter);
        Button bundo = findViewById(R.id.undo);

        freeze = !freeze;       //eine frage der logik ;-)

        bdouble.setEnabled(freeze);
        btripel.setEnabled(freeze);
        daneben.setEnabled(freeze);
        restdaneben.setEnabled(freeze);
        bundo.setEnabled(freeze);
        if (!freeze) {
            b1.setEnabled(false);
            b2.setEnabled(false);
            b3.setEnabled(false);
            b4.setEnabled(false);
            b5.setEnabled(false);
            b6.setEnabled(false);
            b7.setEnabled(false);
            b8.setEnabled(false);
            b9.setEnabled(false);
            b10.setEnabled(false);
            b11.setEnabled(false);
            b12.setEnabled(false);
            b13.setEnabled(false);
            b14.setEnabled(false);
            b15.setEnabled(false);
            b16.setEnabled(false);
            b17.setEnabled(false);
            b18.setEnabled(false);
            b19.setEnabled(false);
            b20.setEnabled(false);
            bull.setEnabled(false);

        } else {
            b1.setEnabled(true);
            b2.setEnabled(true);
            b3.setEnabled(true);
            b4.setEnabled(true);
            b5.setEnabled(true);
            b6.setEnabled(true);
            b7.setEnabled(true);
            b8.setEnabled(true);
            b9.setEnabled(true);
            b10.setEnabled(true);
            b11.setEnabled(true);
            b12.setEnabled(true);
            b13.setEnabled(true);
            b14.setEnabled(true);
            b15.setEnabled(true);
            b16.setEnabled(true);
            b17.setEnabled(true);
            b18.setEnabled(true);
            b19.setEnabled(true);
            b20.setEnabled(true);
            bull.setEnabled(true);
        }
    }

    public void onBackPressed() {
        AlertDialog alertDialog = new AlertDialog.Builder(roundtclock.this).create();
        alertDialog.setTitle(getResources().getString(R.string.achtung));
        alertDialog.setMessage(getResources().getString(R.string.willstduverlassen));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.zuruck), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private static class player {
        final int[] treffer = new int[26]; // feld 25 ist anzahl_bull_treffer, ansonsten 10-20, bei crazy cricket auch mehr;
        String spielerName;
        int score;
        int darts;
        int gewinnerplatz;
    }

    private static class pfeil {
        int zahl;
        int addpunkte;
        int spielerindex;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }
        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }
}

