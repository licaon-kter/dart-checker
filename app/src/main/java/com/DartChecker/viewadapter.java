package com.DartChecker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class viewadapter extends RecyclerView.Adapter<viewadapter.ViewHolder> {

    private final List<String> mData;
    private final LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private final Context con;

    // data is passed into the constructor
    viewadapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        con = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String animal = mData.get(position);
        holder.myTextView.setText(animal);

        holder.deletebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dummy2 = MainActivity.allespieler.get(position).spielerName;
                AlertDialog alertDialog = new AlertDialog.Builder(holder.deletebutton.getContext()).create();
                alertDialog.setTitle(con.getResources().getString(R.string.achtung));
                alertDialog.setMessage(String.format(con.getResources().getString(R.string.willstdu), dummy2));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, con.getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spieler komplett löschen
                                String itemLabel = mData.get(position);

                                mData.remove(position);

                                notifyItemRemoved(position);

                                notifyItemRangeChanged(position, mData.size());
                                MainActivity.allespieler.remove(position);

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, con.getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();


            }
        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView myTextView;
        final ImageButton deletebutton;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tvAnimalName);
            deletebutton = itemView.findViewById(R.id.deletebutton);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition());

        }


    }
}