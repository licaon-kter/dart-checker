package com.DartChecker;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;


public class MainActivity extends AppCompatActivity {

    private static final String fileName = "spieler.dat";
    public static ArrayList<spieler> allespieler;
    public String startsprache;
    public static boolean themeauswahl = true,
            setlegmodus = true;
    private ArrayAdapter<spieler> arrayAdapter;
    private static int maxspieleranzahl = 8;




    public static void speichern(Context ctx, Boolean speichern_aussetzen) {
        if (speichern_aussetzen) return;
        try {
            FileOutputStream fout = ctx.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(allespieler);
            oos.flush();
            oos.close();  //fileoutputstream wird automatisch mitgeschlossen
        } catch (FileNotFoundException e) {
            Toast.makeText(ctx, e + "Error saving: file not found", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (IOException i) {
            Toast.makeText(ctx, i + "Error saving: Input/Output", Toast.LENGTH_LONG).show();
            i.printStackTrace();
        }
    }

    private void laden() {
        try {
            FileInputStream fis = openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            @SuppressWarnings("unchecked") ArrayList<spieler> returnlist = (ArrayList<spieler>) is.readObject();
            is.close();
            fis.close();
            allespieler = returnlist;
        } catch (FileNotFoundException e) {
            Toast.makeText(this, e + "Error loading: file not found", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (ClassNotFoundException b) {
            Toast.makeText(this, b + "Error loading: class not found", Toast.LENGTH_LONG).show();
            b.printStackTrace();
        } catch (IOException i) {
            Toast.makeText(this, i + "Error loading: Input/Output", Toast.LENGTH_LONG).show();
            i.printStackTrace();
        }
    }

    private int maxlegs = 5;
    private int maxsets = 13;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        if (settings.contains("Sprache")) {
            startsprache = settings.getString("Sprache", "en");
        } else {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("SystemSprache", Locale.getDefault().getLanguage());
            editor.apply();
        }
        if (settings.contains("Theme")) themeauswahl = settings.getBoolean("Theme", true);
        if (themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_main);
        ConstraintLayout main = findViewById(R.id.ebene1);
        if (settings.contains("keepscreenonmenu")) main.setKeepScreenOn(settings.getBoolean("keepscreenonmenu",true));


        final CharSequence[] x01spielmode = {"301", "501"};
        List<String> spieltyptext = Arrays.asList("X01", "FREE", "SET/LEG", "CRICKET","ELIMINATION", "HALVE IT", "ROUNDtCLOCK");
        List<String> spielmodecricket = Arrays.asList("15-Bull", "14-Bull", "13-Bull", "12-Bull", "11-Bull", "10-Bull", "Crazy");
        List<String> spielmoderoundtclock = Arrays.asList("1-Bull", "1-20", "Bull-1", "20-1");
        final CharSequence[] cricketmodes = {"Classic", "Cut Throat"};

        final CharSequence[] cricketspielmode = spielmodecricket.toArray(new CharSequence[spielmodecricket.size()]);
        final CharSequence[] roundtclockmode = spielmoderoundtclock.toArray(new CharSequence[spielmoderoundtclock.size()]);

        if (settings.contains("setlegmodus"))
            setlegmodus = settings.getBoolean("setlegmodus", true);
        if (setlegmodus) {
            maxlegs = 3;
            maxsets = 7;
        }

        allespieler = new ArrayList<>();
        // wenn Speicherdatei vorhanden, laden! ansonsten 8 Spieler erstellen und speichern
        File file = getBaseContext().getFileStreamPath(fileName);
        if (file.exists()) {
            laden();
        } else {
            spieler gast1 = new spieler();
            spieler gast2 = new spieler();
            spieler gast3 = new spieler();
            spieler gast4 = new spieler();
            spieler gast5 = new spieler();
            spieler gast6 = new spieler();
            spieler gast7 = new spieler();
            spieler gast8 = new spieler();
            gast1.spielerName = getResources().getString(R.string.gast1);
            gast2.spielerName = getResources().getString(R.string.gast2);
            gast3.spielerName = getResources().getString(R.string.gast3);
            gast4.spielerName = getResources().getString(R.string.gast4);
            gast5.spielerName = getResources().getString(R.string.gast5);
            gast6.spielerName = getResources().getString(R.string.gast6);
            gast7.spielerName = getResources().getString(R.string.gast7);
            gast8.spielerName = getResources().getString(R.string.gast8);
            allespieler.add(gast1);
            allespieler.add(gast2);
            allespieler.add(gast3);
            allespieler.add(gast4);
            allespieler.add(gast5);
            allespieler.add(gast6);
            allespieler.add(gast7);
            allespieler.add(gast8);
            speichern(this,true);
        }
        // spiellistenAuswahllisten "bestücken"
        arrayAdapter = new ArrayAdapter<>(
                MainActivity.this,
                R.layout.spinner_item_head, allespieler);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        final Spinner spin1 = findViewById(R.id.spinner);
        final Spinner spin2 = findViewById(R.id.spinner2);
        final Spinner spin3 = findViewById(R.id.spinner3);
        final Spinner spin4 = findViewById(R.id.spinner4);
        final Spinner spin5 = findViewById(R.id.spinner5);
        final Spinner spin6 = findViewById(R.id.spinner6);
        final Spinner spin7 = findViewById(R.id.spinner7);
        final Spinner spin8 = findViewById(R.id.spinner8);
        spin1.setAdapter(arrayAdapter);
        spin2.setAdapter(arrayAdapter);
        spin3.setAdapter(arrayAdapter);
        spin4.setAdapter(arrayAdapter);
        spin5.setAdapter(arrayAdapter);
        spin6.setAdapter(arrayAdapter);
        spin7.setAdapter(arrayAdapter);
        spin8.setAdapter(arrayAdapter);

        ArrayList<Integer> spieleranzahlarraylist = new ArrayList<Integer>();
        for (int icount = 1 ; icount <= maxspieleranzahl ; icount++) {
            spieleranzahlarraylist.add(icount);
        }
        ArrayAdapter<Integer> arraySpieleranzahl = new ArrayAdapter<>(MainActivity.this,R.layout.spinner_item_head_spieleranzahl,spieleranzahlarraylist);
        final Spinner spieleranzahlspinner = findViewById(R.id.spieleranzahlspinner);
        spieleranzahlspinner.setAdapter(arraySpieleranzahl);

        ArrayList<String> spieltyparraylist = new ArrayList<>(spieltyptext);
        ArrayAdapter<String> arraySpieltyp = new ArrayAdapter(MainActivity.this, R.layout.spinner_item_head_spieleranzahl,spieltyparraylist);
        final Spinner spieltypspinner = findViewById(R.id.spieltypspinner);
        spieltypspinner.setAdapter(arraySpieltyp);

        ArrayList<String> spielmodearraylist = new ArrayList<>(spielmodecricket);
        ArrayAdapter<String> arraySpielmode = new ArrayAdapter(MainActivity.this, R.layout.spinner_item_head_spieleranzahl,spielmodearraylist);
        final Spinner spielmodusspinner = findViewById(R.id.spielmodusspinner);
        spielmodusspinner.setAdapter(arraySpielmode);




        final Button sliste = findViewById(R.id.spielerlisteb);
        final Button spielmodeknopf = findViewById(R.id.spielmodus);
        final int x01variants = x01spielmode.length;
        final Button spieltypeknopf = findViewById(R.id.spieltyp);
        final SwitchCompat doubleout = findViewById(R.id.doubleout);
        final SwitchCompat masterout = findViewById(R.id.masterout);
        final SwitchCompat doublein = findViewById(R.id.doublein);
        final SwitchCompat masterin = findViewById(R.id.masterin);
        final EditText eingabe = findViewById(R.id.eingabe);
        final Button spieleranzahl = findViewById(R.id.spieleranzahl);
        final Button startbutton = findViewById(R.id.startbutton);
        final ImageButton statistikb = findViewById(R.id.statistikbutton);
        final ImageButton einstellungsbutton = findViewById(R.id.einstellungsbutton);
        final TextView settext = findViewById(R.id.settext);
        final TextView legtext = findViewById(R.id.legtext);
        final Button sets = findViewById(R.id.sets);
        final Button legs = findViewById(R.id.legs);
        final TextView spiele2text = findViewById(R.id.spieler2text);
        final TextView spiele3text = findViewById(R.id.spieler3text);
        final TextView spiele4text = findViewById(R.id.spieler4text);
        final TextView spiele5text = findViewById(R.id.spieler5text);
        final TextView spiele6text = findViewById(R.id.spieler6text);
        final TextView spiele7text = findViewById(R.id.spieler7text);
        final TextView spiele8text = findViewById(R.id.spieler8text);
        final Button cricketmodeknopf = findViewById(R.id.cricketmodeknopf);
        final TextView cricketmodetext = findViewById(R.id.cricketmodetext);
        final Button zufall = findViewById(R.id.zufall);
        final ConstraintLayout touchlayout = findViewById(R.id.touchlayer);


        //setanzahl wechseln bei touch
        sets.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int aktuell = Integer.parseInt(sets.getText().toString());
                if (aktuell == maxsets)
                    aktuell = 1;
                else {
                    aktuell++;
                    if (!setlegmodus && aktuell % 2 == 0) aktuell++;
                    if (aktuell > maxsets) aktuell = 1;
                }
                sets.setText(String.valueOf(aktuell));
            }
        });


        //leganzahl wechseln bei touch
        legs.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int aktuell = Integer.parseInt(legs.getText().toString());
                if (aktuell == maxlegs)
                    aktuell = 1;
                else {
                    aktuell++;
                    if (!setlegmodus && aktuell % 2 == 0) aktuell++;
                    if (aktuell > maxlegs) aktuell = 1;
                }
                legs.setText(String.valueOf(aktuell));
            }
        });


        // Spielmodus wechseln bei touch
        spielmodeknopf.setOnClickListener(new OnClickListener() {
                                              public void onClick(View v) {
                                                  CharSequence aktuell = spielmodeknopf.getText();
                                                  if (!(spieltypeknopf.getText().equals("FREE"))) {

                                                      if (spieltypeknopf.getText().equals("HALVE IT")) {
                                                          spielmodeknopf.setText(cricketspielmode[0]);
                                                          return;
                                                      }
                                                      if (aktuell.equals(x01spielmode[x01variants - 1]))
                                                          spielmodeknopf.setText(x01spielmode[0]);
                                                      else
                                                          for (int i = 0; i < x01variants - 1; i++) {
                                                              if (aktuell.equals(x01spielmode[i])) {
                                                                  spielmodeknopf.setText(x01spielmode[i + 1]);
                                                                  break;
                                                              }
                                                          }

                                                      // CRICKET selected
                                                      if (spieltypeknopf.getText().equals("CRICKET")) {
                                                          spielmodearraylist.clear();
                                                          spielmodearraylist.addAll(spielmodecricket);
                                                          spielmodusspinner.performClick();
                                                      }

                                                      // ROUNDtClock selected
                                                      if (spieltypeknopf.getText().equals("ROUNDtCLOCK")) {
                                                          spielmodearraylist.clear();
                                                          spielmodearraylist.addAll(spielmoderoundtclock);
                                                          spielmodusspinner.performClick();
                                                      }

                                                  } else if (spieltypeknopf.getText().equals("FREE")) {
                                                      if (eingabe.getVisibility() == View.INVISIBLE) {
                                                          eingabe.setText(spielmodeknopf.getText());
                                                          spieleranzahl.setEnabled(false);
                                                          spielmodeknopf.setEnabled(false);
                                                          spieltypeknopf.setEnabled(false);
                                                          spin1.setEnabled(false);
                                                          spin2.setEnabled(false);
                                                          spin3.setEnabled(false);
                                                          spin4.setEnabled(false);
                                                          spin5.setEnabled(false);
                                                          spin6.setEnabled(false);
                                                          spin7.setEnabled(false);
                                                          spin8.setEnabled(false);
                                                          startbutton.setEnabled(false);
                                                          statistikb.setEnabled(false);
                                                          doubleout.setVisibility(View.INVISIBLE);
                                                          masterout.setVisibility(View.INVISIBLE);
                                                          doublein.setVisibility(View.INVISIBLE);
                                                          masterin.setVisibility(View.INVISIBLE);
                                                          einstellungsbutton.setEnabled(false);
                                                          zufall.setVisibility(View.INVISIBLE);
                                                          touchlayout.setVisibility(View.VISIBLE);
                                                          eingabe.setVisibility(View.VISIBLE);
                                                          eingabe.selectAll();
                                                          // tastatur einblenden
                                                          if (eingabe.requestFocus()) {
                                                              InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                              assert imm != null;
                                                              imm.showSoftInput(eingabe, InputMethodManager.SHOW_IMPLICIT);

                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );

        // beware of wrong selection on create
        spielmodusspinner.setSelection(0,false);
        // Spielmodus wechseln bei touch
        spielmodusspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = spielmodusspinner.getSelectedItem().toString();
                spielmodeknopf.setText(selection);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spieltyp wechseln bei touch
        spieltypspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = spieltypspinner.getSelectedItem().toString();
                spieltypeknopf.setText(selection);

                if (selection.equals("FREE"))
                    spielmodeknopf.setText("170");
                else spielmodeknopf.setText("301");
                if (selection.equals("SET/LEG")) {
                    spieleranzahl.setText("2");
                    spieleranzahl.setEnabled(false);
                    settext.setVisibility(View.VISIBLE);
                    sets.setVisibility(View.VISIBLE);
                    legtext.setVisibility(View.VISIBLE);
                    legs.setVisibility(View.VISIBLE);
                    spin2.setVisibility(View.VISIBLE);
                    spiele2text.setVisibility(View.VISIBLE);
                    spiele3text.setVisibility(View.GONE);
                    spiele4text.setVisibility(View.GONE);
                    spiele5text.setVisibility(View.GONE);
                    spiele6text.setVisibility(View.GONE);
                    spiele7text.setVisibility(View.GONE);
                    spiele8text.setVisibility(View.GONE);
                    spin3.setVisibility(View.GONE);
                    spin4.setVisibility(View.GONE);
                    spin5.setVisibility(View.GONE);
                    spin6.setVisibility(View.GONE);
                    spin7.setVisibility(View.GONE);
                    spin8.setVisibility(View.GONE);
                } else {
                    spieleranzahl.setEnabled(true);
                    settext.setVisibility(View.GONE);
                    sets.setVisibility(View.GONE);
                    legtext.setVisibility(View.GONE);
                    legs.setVisibility(View.GONE);
                }
                if (selection.equals("CRICKET")) {
                    doubleout.setEnabled(false);
                    masterout.setEnabled(false);
                    doublein.setEnabled(false);
                    masterin.setEnabled(false);
                    spielmodeknopf.setText(cricketspielmode[0]);
                    cricketmodeknopf.setVisibility(View.VISIBLE);
                    cricketmodetext.setVisibility(View.VISIBLE);
                } else {
                    doubleout.setEnabled(true);
                    masterout.setEnabled(true);
                    doublein.setEnabled(true);
                    masterin.setEnabled(true);
                    cricketmodeknopf.setVisibility(View.GONE);
                    cricketmodetext.setVisibility(View.GONE);
                }
                if (selection.equals("HALVE IT")) {
                    doubleout.setEnabled(false);
                    masterout.setEnabled(false);
                    doublein.setEnabled(false);
                    masterin.setEnabled(false);
                    spielmodeknopf.setText(cricketspielmode[0]);
                }
                if (selection.equals("ROUNDtCLOCK")) {
                    doubleout.setEnabled(false);
                    masterout.setEnabled(false);
                    doublein.setEnabled(false);
                    masterin.setEnabled(false);
                    spielmodeknopf.setText(roundtclockmode[0]);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        spieltypeknopf.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                spieltypspinner.performClick();
            }
        });

        // cricketvarianten wechseln bei touch
        final int cricketmodevariants = cricketmodes.length;

        cricketmodeknopf.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence aktuell = cricketmodeknopf.getText();
                if (aktuell.equals(cricketmodes[cricketmodevariants - 1])) cricketmodeknopf.setText(cricketmodes[0]);
                else
                    for (int i = 0; i < cricketmodevariants - 1; i++) {
                        if (aktuell.equals(cricketmodes[i])) {
                            cricketmodeknopf.setText(cricketmodes[i + 1]);
                            break;
                        }
                    }
            }
        });


        // Spieleranzahl wechseln bei touch
        spieleranzahlspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int selected = Integer.parseInt(spieleranzahlspinner.getSelectedItem().toString());
                if (allespieler.size() < selected) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.fehlendespieler), Toast.LENGTH_LONG).show();
                    return;
                }
                spieleranzahl.setText(spieleranzahlspinner.getSelectedItem().toString());

                spiele2text.setVisibility(View.GONE);
                spin2.setVisibility(View.GONE);
                spiele3text.setVisibility(View.GONE);
                spin3.setVisibility(View.GONE);
                spiele4text.setVisibility(View.GONE);
                spin4.setVisibility(View.GONE);
                spiele5text.setVisibility(View.GONE);
                spin5.setVisibility(View.GONE);
                spiele6text.setVisibility(View.GONE);
                spin6.setVisibility(View.GONE);
                spiele7text.setVisibility(View.GONE);
                spin7.setVisibility(View.GONE);
                spiele8text.setVisibility(View.GONE);
                spin8.setVisibility(View.GONE);

                switch (selected){
                    case 8:
                        spiele8text.setVisibility(View.VISIBLE);
                        spin8.setVisibility(View.VISIBLE);
                        if (spin8.getSelectedItemPosition()==0) spin8.setSelection(7);
                    case 7:
                        spiele7text.setVisibility(View.VISIBLE);
                        spin7.setVisibility(View.VISIBLE);
                        if (spin7.getSelectedItemPosition()==0) spin7.setSelection(6);
                    case 6:
                        spiele6text.setVisibility(View.VISIBLE);
                        spin6.setVisibility(View.VISIBLE);
                        if (spin6.getSelectedItemPosition()==0) spin6.setSelection(5);
                    case 5:
                        spiele5text.setVisibility(View.VISIBLE);
                        spin5.setVisibility(View.VISIBLE);
                        if (spin5.getSelectedItemPosition()==0) spin5.setSelection(4);
                    case 4:
                        spiele4text.setVisibility(View.VISIBLE);
                        spin4.setVisibility(View.VISIBLE);
                        if (spin4.getSelectedItemPosition()==0) spin4.setSelection(3);
                    case 3:
                        spiele3text.setVisibility(View.VISIBLE);
                        spin3.setVisibility(View.VISIBLE);
                        if (spin3.getSelectedItemPosition()==0) spin3.setSelection(2);
                    case 2:
                        spiele2text.setVisibility(View.VISIBLE);
                        spin2.setVisibility(View.VISIBLE);
                        if (spin2.getSelectedItemPosition()==0) spin2.setSelection(1);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spieleranzahl.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                spieleranzahlspinner.performClick();
            }
        });

        // startknopf-aktion
        startbutton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                // mehrfache Spielerauswahl?
                ArrayList<String> choosen_player = new ArrayList<>();
                choosen_player.add(spin1.getSelectedItem().toString());
                choosen_player.add(spin2.getSelectedItem().toString());
                choosen_player.add(spin3.getSelectedItem().toString());
                choosen_player.add(spin4.getSelectedItem().toString());
                choosen_player.add(spin5.getSelectedItem().toString());
                choosen_player.add(spin6.getSelectedItem().toString());
                choosen_player.add(spin7.getSelectedItem().toString());
                choosen_player.add(spin8.getSelectedItem().toString());
                int player_for_match = Integer.parseInt(spieleranzahl.getText().toString())-1;
                for (int choosen = 0; choosen <= player_for_match; choosen++) {
                    String cp = choosen_player.get(choosen);
                    int indi = choosen_player.indexOf(cp);
                    int last = choosen_player.lastIndexOf(cp);
                    while (last > player_for_match) {
                        choosen_player.remove(last);
                        last = choosen_player.lastIndexOf(cp);
                    }
                    if (indi != last && indi <= player_for_match && last <= player_for_match)
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                Intent intent;

                if (spieltypeknopf.getText().equals("CRICKET")) {
                    intent = new Intent(MainActivity.this, cricket.class);
                    intent.putExtra("spielvariante",cricketmodeknopf.getText());

                } else if (spieltypeknopf.getText().equals("ELIMINATION")) {
                    intent = new Intent(MainActivity.this, elimination.class);
                    intent.putExtra("doubleout", doubleout.isChecked());
                    intent.putExtra("masterout", masterout.isChecked());
                    intent.putExtra("doublein", doublein.isChecked());
                    intent.putExtra("masterin", masterin.isChecked());
                } else  if (spieltypeknopf.getText().equals("HALVE IT")) {
                    intent = new Intent(MainActivity.this, halve.class);
                } else if (spieltypeknopf.getText().equals("ROUNDtCLOCK")) {
                    intent = new Intent(MainActivity.this, roundtclock.class);
                }

                else {
                    boolean inputmode=false;
                    if (settings.contains("inputmethode")) inputmode=settings.getBoolean("inputmethode",false);
                    if (inputmode) { intent = new Intent(MainActivity.this, matchcalc.class);  }
                    else { intent = new Intent(MainActivity.this, match.class);}
                    intent.putExtra("doubleout", doubleout.isChecked());
                    intent.putExtra("masterout", masterout.isChecked());
                    intent.putExtra("doublein", doublein.isChecked());
                    intent.putExtra("masterin", masterin.isChecked());
                    if (spieltypeknopf.getText().equals("SET/LEG")) {
                        intent.putExtra("sets", Integer.parseInt(sets.getText().toString()));
                        intent.putExtra("legs", Integer.parseInt(legs.getText().toString()));
                    } else {
                        intent.putExtra("sets", 0);
                        intent.putExtra("legs", 0);
                    }

                }
                intent.putExtra("spielmodus", spielmodeknopf.getText());
                intent.putExtra("spieler1", spin1.getSelectedItem().toString());
                intent.putExtra("spieler2", spin2.getSelectedItem().toString());
                intent.putExtra("spieler3", spin3.getSelectedItem().toString());
                intent.putExtra("spieler4", spin4.getSelectedItem().toString());
                intent.putExtra("spieler5", spin5.getSelectedItem().toString());
                intent.putExtra("spieler6", spin6.getSelectedItem().toString());
                intent.putExtra("spieler7", spin7.getSelectedItem().toString());
                intent.putExtra("spieler8", spin8.getSelectedItem().toString());
                intent.putExtra("spieleranzahl", spieleranzahl.getText());
                startActivity(intent);
            }
        });


        // freemodeeingabe übernehmen
        eingabe.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // eingabe kleiner 2?
                    try {
                        if ((eingabe.getText().toString().isEmpty()) || (Integer.parseInt(eingabe.getText().toString()) <= 1)) {
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.minimum), Toast.LENGTH_LONG).show();
                            return true;
                        }
                    } catch (Exception e)   //größere eingabe als integer erlaubt
                    {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.maximum), Toast.LENGTH_LONG).show();
                        return true;
                    }
                    //willkürliche eingabegrenze überschritten?
                    if (Integer.parseInt(eingabe.getText().toString()) > 1000000) {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.maximum), Toast.LENGTH_LONG).show();
                        return true;
                    }

                    // edittext und touchlayer ausblenden
                    eingabe.setVisibility(View.INVISIBLE);
                    touchlayout.setVisibility(View.GONE);
                    // doubleout und co wieder einblenden
                    doubleout.setVisibility(View.VISIBLE);
                    masterout.setVisibility(View.VISIBLE);
                    doublein.setVisibility(View.VISIBLE);
                    masterin.setVisibility(View.VISIBLE);
                    spieleranzahl.setEnabled(true);
                    spielmodeknopf.setEnabled(true);
                    spieltypeknopf.setEnabled(true);
                    spin1.setEnabled(true);
                    spin2.setEnabled(true);
                    spin3.setEnabled(true);
                    spin4.setEnabled(true);
                    spin5.setEnabled(true);
                    spin6.setEnabled(true);
                    spin7.setEnabled(true);
                    spin8.setEnabled(true);
                    startbutton.setEnabled(true);
                    statistikb.setEnabled(true);
                    einstellungsbutton.setEnabled(true);
                    zufall.setVisibility(View.VISIBLE);

                    // keyboard ausblenden
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(eingabe.getWindowToken(), 0);
                    spielmodeknopf.setText(eingabe.getText());
                    return true;
                }
                return false;
            }
        });


        final ScrollView sv = findViewById(R.id.menu);

        //menü einblenden bei klick auf einstellungsbutton und Rest deaktivieren
        einstellungsbutton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                GUIan(false);
            }

        });


        // menü ausblenden, wenn click ausserhalb desselben liegt
        // oder bei free_mode_eingabe tastatur einblenden

        touchlayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sv.getVisibility() == View.VISIBLE) {
                    GUIan(true);
                }

                if (eingabe.getVisibility() == View.VISIBLE) {
                    eingabe.onEditorAction(EditorInfo.IME_ACTION_DONE);
                }

            }
        });

        statistikb.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainActivity.this, statistik.class));
                    }
                }
        );

        final Button einstellungen = findViewById(R.id.buttoneinstellungen);

        einstellungen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, einstellungen.class));
                GUIan(true);
            }
        });


        final Button infob = findViewById(R.id.infobutton);
        infob.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, info.class));
                GUIan(true);
            }
        });

        sliste.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, spielerliste.class));
                GUIan(true);
            }
        });

        final Button hilfe = findViewById(R.id.hilfebutton);
        hilfe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, hilfe.class));
                GUIan(true);
            }
        });

        doubleout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    masterout.setChecked(false);
                }
            }
        });

        masterout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    doubleout.setChecked(true);
                }
            }
        });

        doublein.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (!isChecked) {
                    masterin.setChecked(false);
                }
            }
        });

        masterin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    doublein.setChecked(true);
                }
            }
        });


        zufall.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // zufällige spieltypwahl
                int i;
                Random rand = new Random();
                int gameTypeSelectionPositon = rand.nextInt(spieltyparraylist.size());

                // setze zufällige spielmodewahl in abhängikeit vom spieltyp

                int i2;
                String gameTypeSelection = spieltyparraylist.get(gameTypeSelectionPositon);
                // vorauswahl für alle spieltypen, wird ggf. abgeändert im folgenden
                spieltypeknopf.setText(gameTypeSelection);
                spieleranzahl.setEnabled(true);
                settext.setVisibility(View.GONE);
                sets.setVisibility(View.GONE);
                legtext.setVisibility(View.GONE);
                legs.setVisibility(View.GONE);
                doubleout.setEnabled(true);
                masterout.setEnabled(true);
                doublein.setEnabled(true);
                masterin.setEnabled(true);
                cricketmodeknopf.setVisibility(View.GONE);
                cricketmodetext.setVisibility(View.GONE);
                spielmodeknopf.setText(x01spielmode[rand.nextInt(x01spielmode.length)]);

                // zufall sagt "cricket"
                if (gameTypeSelection.equals("CRICKET")) {
                    doubleout.setEnabled(false);
                    masterout.setEnabled(false);
                    doublein.setEnabled(false);
                    masterin.setEnabled(false);
                    cricketmodeknopf.setVisibility(View.VISIBLE);
                    cricketmodetext.setVisibility(View.VISIBLE);
                    spielmodeknopf.setText(cricketspielmode[rand.nextInt(cricketspielmode.length)]);
                    // choose classic or cut throat
                    for (i=0;i<=rand.nextInt(cricketmodes.length);i++) {
                        cricketmodeknopf.performClick();
                    }
                }
                // zufall sagt "HALVE IT"
                else if (gameTypeSelection.equals("HALVE IT")) {
                    doubleout.setEnabled(false);
                    masterout.setEnabled(false);
                    doublein.setEnabled(false);
                    masterin.setEnabled(false);
                    spielmodeknopf.setText(cricketspielmode[0]);
                }
                // zufall sagt "ROUNDtCLOCK"
                else if (gameTypeSelection.equals("ROUNDtCLOCK")) {
                    doubleout.setEnabled(false);
                    masterout.setEnabled(false);
                    doublein.setEnabled(false);
                    masterin.setEnabled(false);
                    spielmodeknopf.setText(roundtclockmode[rand.nextInt(roundtclockmode.length)]);
                }
                else {  // zufall sagt "x01" oder "elimination" oder "set/leg" oder "free"

                    // zufall sagt "FREE"
                    if (gameTypeSelection.equals("FREE")) {
                        spielmodeknopf.setText(Integer.toString(rand.nextInt(501)+1));
                    }

                    // setze single, double oder master out/in
                    i2 = rand.nextInt(3);
                    switch (i2) {
                        case 0:
                            doubleout.setChecked(false);
                            masterout.setChecked(false);
                            break;
                        case 1:
                            doubleout.setChecked(true);
                            masterout.setChecked(false);
                            break;
                        case 2:
                            masterout.setChecked(true);
                            break;
                        }
                    i2 = rand.nextInt(3);
                    switch (i2) {
                        case 0:
                            doublein.setChecked(false);
                            masterin.setChecked(false);
                            break;
                        case 1:
                            doublein.setChecked(true);
                            masterin.setChecked(false);
                            break;
                        case 2:
                            masterin.setChecked(true);
                            break;
                    }
                    // zufall sagt "set/leg"
                    if (gameTypeSelection.equals("SET/LEG")) {
                        spieleranzahl.setText("2");
                        spieleranzahl.setEnabled(false);
                        settext.setVisibility(View.VISIBLE);
                        sets.setVisibility(View.VISIBLE);
                        legtext.setVisibility(View.VISIBLE);
                        legs.setVisibility(View.VISIBLE);
                        spin2.setVisibility(View.VISIBLE);
                        spiele2text.setVisibility(View.VISIBLE);
                        spiele3text.setVisibility(View.GONE);
                        spiele4text.setVisibility(View.GONE);
                        spiele5text.setVisibility(View.GONE);
                        spiele6text.setVisibility(View.GONE);
                        spiele7text.setVisibility(View.GONE);
                        spiele8text.setVisibility(View.GONE);
                        spin3.setVisibility(View.GONE);
                        spin4.setVisibility(View.GONE);
                        spin5.setVisibility(View.GONE);
                        spin6.setVisibility(View.GONE);
                        spin7.setVisibility(View.GONE);
                        spin8.setVisibility(View.GONE);
                        for (i=0;i<=rand.nextInt(maxsets);i++) sets.performClick();
                        for (i=0;i<=rand.nextInt(maxlegs);i++) legs.performClick();
                    }

                }
            }
        });
    }


    public void GUIan(boolean anaus) {
        final Spinner spin1 = findViewById(R.id.spinner);
        final Spinner spin2 = findViewById(R.id.spinner2);
        final Spinner spin3 = findViewById(R.id.spinner3);
        final Spinner spin4 = findViewById(R.id.spinner4);
        final Spinner spin5 = findViewById(R.id.spinner5);
        final Spinner spin6 = findViewById(R.id.spinner6);
        final Spinner spin7 = findViewById(R.id.spinner7);
        final Spinner spin8 = findViewById(R.id.spinner8);
        final Button spielmodeknopf = findViewById(R.id.spielmodus);
        final Button spieltypeknopf = findViewById(R.id.spieltyp);
        final SwitchCompat doubleout = findViewById(R.id.doubleout);
        final SwitchCompat masterout = findViewById(R.id.masterout);
        final SwitchCompat doublein = findViewById(R.id.doublein);
        final SwitchCompat masterin = findViewById(R.id.masterin);
        final Button spieleranzahl = findViewById(R.id.spieleranzahl);
        final Button startbutton = findViewById(R.id.startbutton);
        final ImageButton statistikb = findViewById(R.id.statistikbutton);
        final Button sets = findViewById(R.id.sets);
        final Button legs = findViewById(R.id.legs);
        final ScrollView sv = findViewById(R.id.menu);
        final Button crkmode =findViewById(R.id.cricketmodeknopf);
        final Button zufall =  findViewById(R.id.zufall);
        final ConstraintLayout touchlayer = findViewById(R.id.touchlayer);

        if (anaus) {
            sv.setVisibility(View.INVISIBLE);
            touchlayer.setVisibility(View.GONE);
        }
        else {
            sv.setVisibility(View.VISIBLE);
            touchlayer.setVisibility(View.VISIBLE);
        }
        doubleout.setEnabled(anaus);
        masterout.setEnabled(anaus);
        doublein.setEnabled(anaus);
        masterin.setEnabled(anaus);
        if (crkmode.getVisibility()==View.VISIBLE || spieltypeknopf.getText().toString().equals("HALVE IT")) {
            doubleout.setEnabled(false);
            masterout.setEnabled(false);
            doublein.setEnabled(false);
            masterin.setEnabled(false);
        }
        spieleranzahl.setEnabled(anaus);
        spielmodeknopf.setEnabled(anaus);
        spieltypeknopf.setEnabled(anaus);
        spin1.setEnabled(anaus);
        spin2.setEnabled(anaus);
        spin3.setEnabled(anaus);
        spin4.setEnabled(anaus);
        spin5.setEnabled(anaus);
        spin6.setEnabled(anaus);
        spin7.setEnabled(anaus);
        spin8.setEnabled(anaus);
        startbutton.setEnabled(anaus);
        statistikb.setEnabled(anaus);
        sets.setEnabled(anaus);
        legs.setEnabled(anaus);
        crkmode.setEnabled(anaus);
        zufall.setEnabled(anaus);
    }


    @Override
    public void onResume() {
        super.onResume();

        arrayAdapter.notifyDataSetChanged();

        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);

        // falls in den einstellungen die Sprache geändert wurde, neustart der activity
        if (settings.contains("Sprache")) {
           if (!(settings.getString("Sprache", "en").equals(startsprache))) {
               Intent intent = getIntent();
               finish();
               startActivity(intent);
           }
        }

        //falls in den einstellungen das theme geändert wurde, neustart der activity

        if (settings.contains("Theme")) {
            if (!(settings.getBoolean("Theme", true) == themeauswahl)) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
        if (settings.contains("setlegmodus")) {
            if (!settings.getBoolean("setlegmodus", true)) {
                maxlegs = 5;
                maxsets = 13;
                setlegmodus = false;
            } else {
                maxlegs = 3;
                maxsets = 7;
                setlegmodus = true;
                Button legsb = findViewById(R.id.legs);
                Button setsb = findViewById(R.id.sets);
                if (Integer.parseInt(setsb.getText().toString()) > maxsets)
                    setsb.setText(String.valueOf(maxsets));
                if (Integer.parseInt(legsb.getText().toString()) > maxlegs)
                    legsb.setText(String.valueOf(maxlegs));
            }
        }



    }

    public void onBackPressed() {
        final EditText eingabe = findViewById(R.id.eingabe);
        if (eingabe.getVisibility() == View.VISIBLE) {
            SwitchCompat doubleout = findViewById(R.id.doubleout);
            SwitchCompat masterout = findViewById(R.id.masterout);
            SwitchCompat doublein = findViewById(R.id.doublein);
            SwitchCompat masterin = findViewById(R.id.masterin);
            Button spieleranzahl = findViewById(R.id.spieleranzahl);
            Button spieltypeknopf = findViewById(R.id.spieltyp);
            Button spielmodeknopf = findViewById(R.id.spielmodus);
            Button startbutton = findViewById(R.id.startbutton);
            Button sets = findViewById(R.id.sets);
            Button legs = findViewById(R.id.legs);
            Button zufall = findViewById(R.id.zufall);
            ImageButton statistikb = findViewById(R.id.statistikbutton);
            ImageButton einstellungsbutton = findViewById(R.id.einstellungsbutton);
            Spinner spin1 = findViewById(R.id.spinner);
            Spinner spin2 = findViewById(R.id.spinner2);
            Spinner spin3 = findViewById(R.id.spinner3);
            Spinner spin4 = findViewById(R.id.spinner4);
            Spinner spin5 = findViewById(R.id.spinner5);
            Spinner spin6 = findViewById(R.id.spinner6);
            Spinner spin7 = findViewById(R.id.spinner7);
            Spinner spin8 = findViewById(R.id.spinner8);
            ConstraintLayout touchlayer = findViewById(R.id.touchlayer);
            eingabe.setVisibility(View.INVISIBLE);
            doubleout.setVisibility(View.VISIBLE);
            masterout.setVisibility(View.VISIBLE);
            doublein.setVisibility(View.VISIBLE);
            masterin.setVisibility(View.VISIBLE);
            spieleranzahl.setEnabled(true);
            spielmodeknopf.setEnabled(true);
            spieltypeknopf.setEnabled(true);
            spin1.setEnabled(true);
            spin2.setEnabled(true);
            spin3.setEnabled(true);
            spin4.setEnabled(true);
            spin5.setEnabled(true);
            spin6.setEnabled(true);
            spin7.setEnabled(true);
            spin8.setEnabled(true);
            startbutton.setEnabled(true);
            statistikb.setEnabled(true);
            einstellungsbutton.setEnabled(true);
            sets.setEnabled(true);
            legs.setEnabled(true);
            zufall.setEnabled(true);
            touchlayer.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
    @CsvDataType()
    public static class spieler implements Serializable {
        private static final long serialVersionUID = 19L;
        @CsvField(pos = 1)
        String spielerName;
        @CsvField(pos = 2)
        int AnzahlSpiele;
        @CsvField(pos = 3)
        int AnzahlEinzelspiele;
        @CsvField(pos = 4)
        int anzahlSiege;
        @CsvField(pos = 13)
        int besterWurf;
        int zweitbesterWurf;
        @CsvField(pos = 8)
        float durchschnitt;
        @CsvField(pos = 7)
        int geworfenePfeile;
        @CsvField(pos = 9)
        int anzahluber60;
        @CsvField(pos = 10)
        int anzahluber100;
        @CsvField(pos = 11)
        int anzahluber140;
        @CsvField(pos = 12)
        int anzahl180;
        int score;
        int legs;
        int sets = 0;
        @CsvField(pos = 5)
        int matcheswon;
        @CsvField(pos = 6)
        int matcheslost;
        @CsvField(pos = 14)
        int checkoutmax;
        int legswon;

        @Override
        public String toString() {
            return this.spielerName;
        }

    }

    @CsvDataType()
   public static class heading implements Serializable {
        private static final long serialVersionUID = 19L;
        @CsvField(pos = 1)
        String spielerName;
        @CsvField(pos = 2)
        String AnzahlSpiele;
        @CsvField(pos = 3)
        String AnzahlEinzelspiele;
        @CsvField(pos = 4)
        String anzahlSiege;
        @CsvField(pos = 5)
        String matcheswon;
        @CsvField(pos = 6)
        String matcheslost;
        @CsvField(pos = 7)
        String geworfenePfeile;
        @CsvField(pos = 8)
        String durchschnitt;
        @CsvField(pos = 9)
        String anzahluber60;
        @CsvField(pos = 10)
        String anzahluber100;
        @CsvField(pos = 11)
        String anzahluber140;
        @CsvField(pos = 12)
        String anzahl180;
        @CsvField(pos = 13)
        String besterWurf;
        @CsvField(pos = 14)
        String checkoutmax;
    }


}
