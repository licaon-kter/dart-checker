package com.DartChecker;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;


public class spielerliste extends AppCompatActivity implements viewadapter.ItemClickListener {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    private viewadapter adapter;
    private int aktuelleposition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_spielerliste);
        SharedPreferences settings = getSharedPreferences("Einstellungen",0);
        ConstraintLayout main = findViewById(R.id.cl);
        if (settings.contains("keepscreenonmenu")) main.setKeepScreenOn(settings.getBoolean("keepscreenonmenu",false));

        final ArrayList<String> SpielerNamen = new ArrayList<>();
        // spielernamen aus allespieler laden
        MainActivity.spieler next;
        int i;
        for (i = 0; i < MainActivity.allespieler.size(); i++) {
            next = MainActivity.allespieler.get(i);
            SpielerNamen.add(next.spielerName);
        }

        RecyclerView recyclerView = findViewById(R.id.rvAnimals);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new viewadapter(this, SpielerNamen);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        final ImageButton spielerhinzu = findViewById(R.id.addbutton);
        // final FloatingActionButton spielerhinzu = findViewById(R.id.spieleradd);
        final EditText eingabe = findViewById(R.id.eingab);
        // liste aktualisieren mit usereingabe
        eingabe.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // edittext ausblenden
                    eingabe.setVisibility(View.INVISIBLE);
                    // keyboard ausblenden
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(eingabe.getWindowToken(), 0);
                    // spielereintrag überschreiben/ändern
                    String neuername = eingabe.getText().toString();
                    if (aktuelleposition != -1) {
                        if (SpielerNamen.contains(neuername) && SpielerNamen.get(aktuelleposition).equals(neuername)) {
                            Toast.makeText(spielerliste.this, String.format(getResources().getString(R.string.bleibt), neuername), Toast.LENGTH_SHORT).show();
                            aktuelleposition = -1;
                            return false;
                        } else {
                            if (SpielerNamen.contains(neuername)) {
                                Toast.makeText(spielerliste.this, String.format(getResources().getString(R.string.existiert), neuername), Toast.LENGTH_SHORT).show();
                                aktuelleposition = -1;
                                return false;
                            }

                        }

                        if (neuername.isEmpty())

                        {
                            String dummy2 = MainActivity.allespieler.get(aktuelleposition).spielerName;
                            AlertDialog alertDialog = new AlertDialog.Builder(spielerliste.this).create();
                            alertDialog.setTitle(getResources().getString(R.string.achtung));
                            alertDialog.setMessage(String.format(getResources().getString(R.string.willstdu), dummy2));
                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            //spieler komplett löschen
                                            MainActivity.allespieler.remove(aktuelleposition);
                                            SpielerNamen.remove(aktuelleposition);
                                            adapter.notifyItemRemoved(aktuelleposition);
                                            dialog.dismiss();
                                            aktuelleposition = -1;

                                        }
                                    });
                            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    aktuelleposition = -1;
                                }
                            });
                            alertDialog.show();
                        } else {
                            //datenspeicher aktualisieren
                            MainActivity.allespieler.get(aktuelleposition).spielerName = neuername;
                            //spieleranzeige aktualisieren
                            SpielerNamen.set(aktuelleposition, neuername);
                            adapter.notifyItemChanged(aktuelleposition);
                            aktuelleposition = -1;
                        }
                    }
                    // neuer! Name
                    else if (neuername.isEmpty()) {
                        Toast.makeText(spielerliste.this, getResources().getString(R.string.keinspieler), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    //neuen spieler hinzufügen
                    else {
                        if (SpielerNamen.contains(neuername)) {
                            Toast.makeText(spielerliste.this, String.format(getResources().getString(R.string.existiert), neuername), Toast.LENGTH_SHORT).show();
                            aktuelleposition = -1;
                            return false;
                        }
                        MainActivity.spieler dummyspieler = new MainActivity.spieler();
                        dummyspieler.spielerName = neuername;
                        MainActivity.allespieler.add(dummyspieler);
                        SpielerNamen.add(neuername);
                        adapter.notifyDataSetChanged();
                    }
                    return true;
                }
                return false;
            }
        });

        //spielernamen-editiormodus/-addmodus
        spielerhinzu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText eingabe = findViewById(R.id.eingab);
                if (eingabe.getVisibility() == View.INVISIBLE) {
                    eingabe.setText("");
                    eingabe.setVisibility(View.VISIBLE);
                    // tastatur einblenden
                    if (eingabe.requestFocus()) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        assert imm != null;
                        //  imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                        imm.showSoftInput(eingabe, InputMethodManager.SHOW_FORCED);
                    }
                }
            }
        });


    }


    @Override
    public void onItemClick(int position) {

        final EditText eingabe = findViewById(R.id.eingab);
        if (eingabe.getVisibility() == View.INVISIBLE) {
            aktuelleposition = position;
            eingabe.setText(adapter.getItem(position));
            eingabe.setVisibility(View.VISIBLE);
            if (eingabe.requestFocus()) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.showSoftInput(eingabe, InputMethodManager.SHOW_FORCED);
            }
        } else {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.showSoftInput(eingabe, InputMethodManager.SHOW_FORCED);
        }
    }


    public void onBackPressed() {
        final EditText eingabe = findViewById(R.id.eingab);
        if (eingabe.getVisibility() == View.VISIBLE) {
            eingabe.setVisibility(View.INVISIBLE);
            aktuelleposition = -1;
        } else {
            MainActivity.speichern(this,false);
            finish();
        }
    }
}


