package com.DartChecker;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Objects;

public class spielende extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    private final DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    private final DecimalFormat formater = new DecimalFormat("###.##", symbols);

    private final View.OnClickListener okaction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_spielende);
        SharedPreferences settings = getSharedPreferences("Einstellungen",0);
        CoordinatorLayout main = findViewById(R.id.mainlayout);
        if (settings.contains("keepscreenonmenu")) main.setKeepScreenOn(settings.getBoolean("keepscreenonmenu",false));

        TextView e1 = findViewById(R.id.erstert);
        TextView e2 = findViewById(R.id.ersterdaten);
        TextView z1 = findViewById(R.id.zweitert);
        TextView z2 = findViewById(R.id.zweiterdaten);
        TextView d1 = findViewById(R.id.drittert);
        TextView d2 = findViewById(R.id.dritterdaten);
        TextView v1 = findViewById(R.id.viertert);
        TextView v2 = findViewById(R.id.vierterdaten);
        TextView f1 = findViewById(R.id.fuenfter);
        TextView f2 = findViewById(R.id.fuenfterdaten);
        TextView s1 = findViewById(R.id.sechster);
        TextView s2 = findViewById(R.id.sechsterdaten);
        TextView si1 = findViewById(R.id.siebenter);
        TextView si2 = findViewById(R.id.siebenterdaten);
        TextView a1 = findViewById(R.id.achter);
        TextView a2 = findViewById(R.id.achterdaten);
        TextView zeitwert = findViewById(R.id.zeitwert);
        Button okb = findViewById(R.id.okbutton);
        okb.setOnClickListener(okaction);
        TableLayout table = findViewById(R.id.tableLayout);



        Intent intent = getIntent();
        long spielzeit = intent.getLongExtra("spielzeit",0);
        long sek,min=0,h=0;
        if (spielzeit>3600) {
            h=spielzeit/3600;
            spielzeit-=h*3600;
        }
        if (spielzeit>60) {
            min=spielzeit/60;
            spielzeit-=min*60;
        }

        sek=spielzeit;
        zeitwert.setText(getResources().getString(R.string.spieldauer)+" "+h+":"+min+":"+sek);


        e1.setVisibility(View.GONE);
        e2.setVisibility(View.GONE);
        z1.setVisibility(View.GONE);
        z2.setVisibility(View.GONE);
        d1.setVisibility(View.GONE);
        d2.setVisibility(View.GONE);
        v1.setVisibility(View.GONE);
        v2.setVisibility(View.GONE);
        f1.setVisibility(View.GONE);
        f2.setVisibility(View.GONE);
        s1.setVisibility(View.GONE);
        s2.setVisibility(View.GONE);
        si1.setVisibility(View.GONE);
        si2.setVisibility(View.GONE);
        a1.setVisibility(View.GONE);
        a2.setVisibility(View.GONE);

        boolean match = intent.getBooleanExtra("match", false);
        if (!match) {

            table.setVisibility(View.GONE);
            int anzahl = intent.getIntExtra("anzahl", 0);

            boolean cricket = intent.getBooleanExtra("cricket", false);
            boolean spielgehtweiter = intent.getBooleanExtra("spielgehtweiter",false);
            boolean roundtclock = intent.getBooleanExtra("roundtclock", false);

            if (roundtclock) {
                String segment;
                int segint;
                switch (anzahl) {
                    case 8:
                        a1.setText(intent.getCharSequenceExtra("achter"));
                        segint = intent.getIntExtra("achterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        a2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        a1.setVisibility(View.VISIBLE);
                        a2.setVisibility(View.VISIBLE);
                    case 7:
                        si1.setText(intent.getCharSequenceExtra("siebenter"));
                        segint = intent.getIntExtra("siebenterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        si2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        si1.setVisibility(View.VISIBLE);
                        si2.setVisibility(View.VISIBLE);
                    case 6:
                        s1.setText(intent.getCharSequenceExtra("sechster"));
                        segint = intent.getIntExtra("sechsterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        s2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        s1.setVisibility(View.VISIBLE);
                        s2.setVisibility(View.VISIBLE);
                    case 5:
                        f1.setText(intent.getCharSequenceExtra("fuenfter"));
                        segint = intent.getIntExtra("fuenfterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        f2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        f1.setVisibility(View.VISIBLE);
                        f2.setVisibility(View.VISIBLE);
                    case 4:
                        v1.setText(intent.getCharSequenceExtra("vierter"));
                        segint = intent.getIntExtra("vierterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        v2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        v1.setVisibility(View.VISIBLE);
                        v2.setVisibility(View.VISIBLE);
                    case 3:
                        d1.setText(intent.getCharSequenceExtra("dritter"));
                        segint = intent.getIntExtra("dritterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        d2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        d1.setVisibility(View.VISIBLE);
                        d2.setVisibility(View.VISIBLE);
                    case 2:
                        z1.setText(intent.getCharSequenceExtra("zweiter"));
                        segint = intent.getIntExtra("zweiterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        z2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment);
                        z1.setVisibility(View.VISIBLE);
                        z2.setVisibility(View.VISIBLE);
                    case 1:
                        e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                        segint = intent.getIntExtra("ersterscore", -123);
                        if (segint == 25) segment = "Bull"; else segment = Integer.toString(segint);
                        e2.setText(getResources().getString(R.string.erreichtesSegment) + " " + segment );
                        e1.setVisibility(View.VISIBLE);
                        e2.setVisibility(View.VISIBLE);
                }
                return;
            }
            if (cricket) {
                switch (anzahl) {
                    case 8:
                        if (spielgehtweiter)
                            a1.setText("8. " + intent.getCharSequenceExtra("achter"));
                        else
                            a1.setText(intent.getCharSequenceExtra("achter"));
                        a2.setText(getResources().getString(R.string.punkte)+" "  + intent.getIntExtra("achterscore", -123));
                        a1.setVisibility(View.VISIBLE);
                        a2.setVisibility(View.VISIBLE);
                    case 7:
                        if (spielgehtweiter)
                            si1.setText("7. " + intent.getCharSequenceExtra("siebenter"));
                        else
                            si1.setText(intent.getCharSequenceExtra("siebenter"));
                        si2.setText(getResources().getString(R.string.punkte)+" "  + intent.getIntExtra("siebenterscore", -123));
                        si1.setVisibility(View.VISIBLE);
                        si2.setVisibility(View.VISIBLE);
                    case 6:
                        if (spielgehtweiter)
                            s1.setText("6. " + intent.getCharSequenceExtra("sechster"));
                        else
                            s1.setText(intent.getCharSequenceExtra("sechster"));
                        s2.setText(getResources().getString(R.string.punkte)+" "  + intent.getIntExtra("sechsterscore", -123));
                        s1.setVisibility(View.VISIBLE);
                        s2.setVisibility(View.VISIBLE);
                    case 5:
                        if (spielgehtweiter)
                            f1.setText("5. "+intent.getCharSequenceExtra("fuenfter"));
                        else
                            f1.setText(intent.getCharSequenceExtra("fuenfter"));
                        f2.setText(getResources().getString(R.string.punkte)+" "  + intent.getIntExtra("fuenfterscore", -123));
                        f1.setVisibility(View.VISIBLE);
                        f2.setVisibility(View.VISIBLE);
                    case 4:
                        if (spielgehtweiter)
                            v1.setText("4. "+intent.getCharSequenceExtra("vierter"));
                        else
                            v1.setText(intent.getCharSequenceExtra("vierter"));
                        v2.setText(getResources().getString(R.string.punkte)+" "  + intent.getIntExtra("vierterscore", -123));
                        v1.setVisibility(View.VISIBLE);
                        v2.setVisibility(View.VISIBLE);
                    case 3:
                        if (spielgehtweiter)
                            d1.setText("3. " + intent.getCharSequenceExtra("dritter"));
                        else
                            d1.setText(intent.getCharSequenceExtra("dritter"));
                        d2.setText(getResources().getString(R.string.punkte)+" " + intent.getIntExtra("dritterscore", -123));
                        d1.setVisibility(View.VISIBLE);
                        d2.setVisibility(View.VISIBLE);
                    case 2:
                        if (spielgehtweiter)
                            z1.setText("2. "+ intent.getCharSequenceExtra("zweiter"));
                        else
                            z1.setText(intent.getCharSequenceExtra("zweiter"));
                        z2.setText(getResources().getString(R.string.punkte)+" " + intent.getIntExtra("zweiterscore", -123));
                        z1.setVisibility(View.VISIBLE);
                        z2.setVisibility(View.VISIBLE);
                    case 1:
                        e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                        e2.setText(getResources().getString(R.string.punkte)+" " + intent.getIntExtra("ersterscore", -123));
                        e1.setVisibility(View.VISIBLE);
                        e2.setVisibility(View.VISIBLE);
                }
                return;
            }

            String darttext = "  " + getResources().getString(R.string.pfeiledd) + " ";
            String resttext = "  " + getResources().getString(R.string.restpunkte) + " ";

            switch (anzahl) {
                case 8:
                    a1.setText("8. " + intent.getCharSequenceExtra("achter"));
                    a2.setText("Ø: " + intent.getFloatExtra("achterschnitt", -123) + darttext + intent.getIntExtra("achterpfeile", -123) + resttext + intent.getIntExtra("achterrest", -123));
                    a1.setVisibility(View.VISIBLE);
                    a2.setVisibility(View.VISIBLE);
                case 7:
                    si1.setText("7. " + intent.getCharSequenceExtra("siebenter"));
                    si2.setText("Ø: " + intent.getFloatExtra("siebenterschnitt", -123) + darttext + intent.getIntExtra("siebenterpfeile", -123) + resttext + intent.getIntExtra("siebenterrest", -123));
                    si1.setVisibility(View.VISIBLE);
                    si2.setVisibility(View.VISIBLE);
                case 6:
                    s1.setText("6. " + intent.getCharSequenceExtra("sechster"));
                    s2.setText("Ø: " + intent.getFloatExtra("sechsterschnitt", -123) + darttext + intent.getIntExtra("sechsterpfeile", -123) + resttext + intent.getIntExtra("sechsterrest", -123));
                    s1.setVisibility(View.VISIBLE);
                    s2.setVisibility(View.VISIBLE);
                case 5:
                    f1.setText("5. " + intent.getCharSequenceExtra("fuenfter"));
                    f2.setText("Ø: " + intent.getFloatExtra("fuenfterschnitt", -123) + darttext + intent.getIntExtra("fuenfterpfeile", -123) + resttext + intent.getIntExtra("fuenfterrest", -123));
                    f1.setVisibility(View.VISIBLE);
                    f2.setVisibility(View.VISIBLE);
                case 4:
                    v1.setText("4. " + intent.getCharSequenceExtra("vierter"));
                    v2.setText("Ø: " + intent.getFloatExtra("vierterschnitt", -123) + darttext + intent.getIntExtra("vierterpfeile", -123) + resttext + intent.getIntExtra("vierterrest", -123));
                    v1.setVisibility(View.VISIBLE);
                    v2.setVisibility(View.VISIBLE);
                case 3:
                    d1.setText("3. " + intent.getCharSequenceExtra("dritter"));
                    d2.setText("Ø: " + intent.getFloatExtra("dritterschnitt", -123) + darttext + intent.getIntExtra("dritterpfeile", -123) + resttext + intent.getIntExtra("dritterrest", -123));
                    d1.setVisibility(View.VISIBLE);
                    d2.setVisibility(View.VISIBLE);
                case 2:
                    z1.setText("2. " + intent.getCharSequenceExtra("zweiter"));
                    z2.setText("Ø: " + intent.getFloatExtra("zweiterschnitt", -123) + darttext + intent.getIntExtra("zweiterpfeile", -123) + resttext + intent.getIntExtra("zweiterrest", -1));
                    z1.setVisibility(View.VISIBLE);
                    z2.setVisibility(View.VISIBLE);
                case 1:
                    e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                    if (intent.getBooleanExtra("suddendeath",false))
                        e2.setText("Ø: " + intent.getFloatExtra("ersterschnitt",-123) + darttext + intent.getIntExtra("ersterpfeile",-123)+ resttext + intent.getIntExtra("ersterrest", -1));
                    else
                        e2.setText("Ø: " + intent.getFloatExtra("ersterschnitt",-123) + darttext + intent.getIntExtra("ersterpfeile",-123));
                    e1.setVisibility(View.VISIBLE);
                    e2.setVisibility(View.VISIBLE);
            }

        }
        // matchmode -> kleine matchstatistik ausgeben
        else {
            TextView spielera = findViewById(R.id.spielera);
            TextView spielerb = findViewById(R.id.spielerb);
            TextView sets1 = findViewById(R.id.sets1);
            TextView sets2 = findViewById(R.id.sets2);
            TextView pfeile1 = findViewById(R.id.pfeile1);
            TextView pfeile2 = findViewById(R.id.pfeile2);
            TextView schnitt1 = findViewById(R.id.durchschnitt1);
            TextView schnitt2 = findViewById(R.id.durchschnitt2);
            TextView u61 = findViewById(R.id.u601);
            TextView u62 = findViewById(R.id.u602);
            TextView u1001 = findViewById(R.id.u1001);
            TextView u1002 = findViewById(R.id.u1002);
            TextView u1401 = findViewById(R.id.u1401);
            TextView u1402 = findViewById(R.id.u1402);
            TextView u1801 = findViewById(R.id.u1801);
            TextView u1802 = findViewById(R.id.u1802);
            TextView bauf1 = findViewById(R.id.besterwurf1);
            TextView bauf2 = findViewById(R.id.besterwurf2);
            TextView check1 = findViewById(R.id.checkout1);
            TextView check2 = findViewById(R.id.checkout2);
            TextView legs1 = findViewById(R.id.legs1);
            TextView legs2 = findViewById(R.id.legs2);
            int colorbesser = spielera.getCurrentTextColor(),
                    colorschlechter = spielerb.getCurrentTextColor();
            table.setVisibility(View.VISIBLE);

            spielera.setText(intent.getCharSequenceExtra("erster"));
            spielerb.setText(intent.getCharSequenceExtra("zweiter"));
            schnitt1.setText(formater.format(intent.getFloatExtra("ersterschnitt", -123)));
            schnitt2.setText(formater.format(intent.getFloatExtra("zweiterschnitt", -123)));
            pfeile1.setText(Integer.toString(intent.getIntExtra("ersterpfeile", -123)));
            pfeile2.setText(Integer.toString(intent.getIntExtra("zweiterpfeile", -123)));
            sets1.setText(Integer.toString(intent.getIntExtra("erstersets", -123)));
            sets2.setText(Integer.toString(intent.getIntExtra("zweitersets", -123)));
            legs1.setText(Integer.toString(intent.getIntExtra("ersterlegs", -123)));
            legs2.setText(Integer.toString(intent.getIntExtra("zweiterlegs", -123)));
            u61.setText(Integer.toString(intent.getIntExtra("erster60", -123)));
            u62.setText(Integer.toString(intent.getIntExtra("zweiter60", -123)));
            u1001.setText(Integer.toString(intent.getIntExtra("erster100", -123)));
            u1002.setText(Integer.toString(intent.getIntExtra("zweiter100", -123)));
            u1401.setText(Integer.toString(intent.getIntExtra("erster140", -123)));
            u1402.setText(Integer.toString(intent.getIntExtra("zweiter140", -123)));
            u1801.setText(Integer.toString(intent.getIntExtra("erster180", -123)));
            u1802.setText(Integer.toString(intent.getIntExtra("zweiter180", -123)));
            bauf1.setText(Integer.toString(intent.getIntExtra("ersterwurf", -123)));
            bauf2.setText(Integer.toString(intent.getIntExtra("zweiterwurf", -123)));
            check1.setText(Integer.toString(intent.getIntExtra("erstercheckout", -123)));
            check2.setText(Integer.toString(intent.getIntExtra("zweitercheckout", -123)));

            if (Float.valueOf(schnitt1.getText().toString()) < Float.valueOf(schnitt2.getText().toString())) {
                schnitt2.setTextColor(colorbesser);
                schnitt1.setTextColor(colorschlechter);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Objects.equals(Float.valueOf(schnitt1.getText().toString()), Float.valueOf(schnitt2.getText().toString())))
                    schnitt2.setTextColor(colorbesser);
            } else if (Float.valueOf(schnitt1.getText().toString()).equals(Float.valueOf(schnitt2.getText().toString())))
                schnitt2.setTextColor(colorbesser);
            if (Integer.parseInt(pfeile1.getText().toString()) > Integer.parseInt(pfeile2.getText().toString())) {
                pfeile2.setTextColor(colorbesser);
                pfeile1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(pfeile1.getText().toString()) == Integer.parseInt(pfeile2.getText().toString()))
                pfeile2.setTextColor(colorbesser);
            if (Integer.parseInt(legs1.getText().toString()) < Integer.parseInt(legs2.getText().toString())) {
                legs2.setTextColor(colorbesser);
                legs1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(legs1.getText().toString()) == Integer.parseInt(legs2.getText().toString()))
                legs2.setTextColor(colorbesser);
            if (Integer.parseInt(u61.getText().toString()) < Integer.parseInt(u62.getText().toString())) {
                u62.setTextColor(colorbesser);
                u61.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u61.getText().toString()) == Integer.parseInt(u62.getText().toString()))
                u62.setTextColor(colorbesser);
            if (Integer.parseInt(u1001.getText().toString()) < Integer.parseInt(u1002.getText().toString())) {
                u1002.setTextColor(colorbesser);
                u1001.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u1001.getText().toString()) == Integer.parseInt(u1002.getText().toString()))
                u1002.setTextColor(colorbesser);
            if (Integer.parseInt(u1401.getText().toString()) < Integer.parseInt(u1402.getText().toString())) {
                u1402.setTextColor(colorbesser);
                u1401.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u1401.getText().toString()) == Integer.parseInt(u1402.getText().toString()))
                u1402.setTextColor(colorbesser);
            if (Integer.parseInt(u1801.getText().toString()) < Integer.parseInt(u1802.getText().toString())) {
                u1802.setTextColor(colorbesser);
                u1801.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u1801.getText().toString()) == Integer.parseInt(u1802.getText().toString()))
                u1802.setTextColor(colorbesser);
            if (Integer.parseInt(check1.getText().toString()) < Integer.parseInt(check2.getText().toString())) {
                check2.setTextColor(colorbesser);
                check1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(check1.getText().toString()) == Integer.parseInt(check2.getText().toString()))
                check2.setTextColor(colorbesser);
            if (Integer.parseInt(bauf1.getText().toString()) < Integer.parseInt(bauf2.getText().toString())) {
                bauf2.setTextColor(colorbesser);
                bauf1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(bauf1.getText().toString()) == Integer.parseInt(bauf2.getText().toString()))
                bauf2.setTextColor(colorbesser);
        }
    }
}
