package com.DartChecker;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class cricket extends AppCompatActivity {
    private final int crazycricketFelderAnzahl = 7;      // (exclusive bull)
    private int crazycricketRangeMinimum = 6;
    private final player[] spieler = new player[9];
    private final ArrayList<pfeil> wuerfe = new ArrayList<pfeil>();
    private final ArrayList<crazySegmente> crazySegmentes = new ArrayList<crazySegmente>();
    private long startTime = 0;
    private int bcolor;
    private int bcolorn;
    private int textcoloraktiv,
            textcolorpassiv;
    private float textsizeaktiv,
            textsizepassiv;
    private boolean t = false;
    private boolean d = false;
    private final View.OnClickListener doubletriple = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button doubleb = findViewById(R.id.doublebutton);
            Button tripleb = findViewById(R.id.triple);
            doubleb.setBackgroundColor(bcolorn);        // workaround for low api version (15)
            tripleb.setBackgroundColor(bcolorn);        // workaround for low api version (15);
            if (v.getId() == R.id.doublebutton) {
                if (t) {
                    t = false;
                    tripleb.setBackgroundColor(bcolorn);
                }
                if (!d) {
                    d = true;
                    doubleb.setBackgroundColor(bcolor);
                } else {
                    d = false;
                    doubleb.setBackgroundColor(bcolorn);
                }
            } else {
                if (d) {
                    d = false;
                    doubleb.setBackgroundColor(bcolorn);
                }
                if (!t) {
                    t = true;
                    tripleb.setBackgroundColor(bcolor);
                } else {
                    t = false;
                    tripleb.setBackgroundColor(bcolorn);
                }
            }
        }
    };
    private boolean spielgehtweiter = false;
    private int spieleranzahl,
            aktiverSpieler = 1,       //index für namensarray
            xdart = 0,                //zähler für geworfene darts (0, 1 oder 2) pro runde
            aufnahme,
            changetime = 1500,
            ii = -1;                   //index für undo-speicher
    private String s = "";
    private CharSequence spielmodus;
    private CharSequence spielvariante;
    private boolean undoabfrage = true; //soll die undoabfrage beim undoclick erscheinen oder undo() ohne rückfrage durchgeführt werden - wichtig für frage beim letzten wurf

    // undo
    private final View.OnClickListener undoclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // sicherheitsabfrage
             if (undoabfrage) {
                AlertDialog alertDialog = new AlertDialog.Builder(cricket.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.willstduUndo));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                undo();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
                 final SharedPreferences settings = getApplicationContext().getSharedPreferences("Einstellungen", 0);
                 if (settings.contains("Undosicherheitsabfrage")) {
                     if (!settings.getBoolean("Undosicherheitsabfrage",false)) {
                         alertDialog.hide();
                         alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                     }
                 }
            } else {
                undoabfrage = true;
                undo();
            }
        }

        private void undo() {
            int dart, faktor, theoretischewurfpunkte, addpunkte, anzahltrefferabzug;
            TextView darts = findViewById(R.id.darts);

            if (ii >= 0) {
                if (xdart == 0) {
                    //spielerwechsel
                    xdart = 2;
                    s = "..";
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);

                    aktiverSpieler = wuerfe.get(ii).spielerindex;   //der letzte Spieler der geworfen hat
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setPaintFlags(textfeld(1, aktiverSpieler, 3).getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    textfeld(1, aktiverSpieler, 2).setPaintFlags(textfeld(1, aktiverSpieler, 2).getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);

                    if (spielmodus.equals("Crazy")) {
                        ladeSegmente(crazySegmentes.get(crazySegmentes.size() - 1)); //letzte gespeicherte segmente laden
                        crazySegmentes.remove(crazySegmentes.size() - 1);
                    }
                    if (spieler[aktiverSpieler].gewinnerplatz != 0)
                        spieler[aktiverSpieler].gewinnerplatz = 0;
                } else xdart--;
                if (xdart == 1) s = ".";
                else if (xdart == 0) s = "";
                darts.setText(s);
                dart = wuerfe.get(ii).zahl;
                faktor = wuerfe.get(ii).faktor;
                addpunkte = wuerfe.get(ii).addpunkte;
                theoretischewurfpunkte = dart * faktor;
                //addpunkte sind 1x,2x oder 3x dart -> entsprechend treffer abziehn und score
                if (spielvariante.equals("Classic")) {
                    spieler[aktiverSpieler].score -= addpunkte;
                } else if (spielvariante.equals("Cut Throat")) {
                    for (int i = 1; i <= spieleranzahl; i++) {
                        if ((i != aktiverSpieler) && !(zahlClosed(i, dart)))
                            spieler[i].score -= addpunkte;
                    }
                }

                if ((faktor > 0) && (dart > 0))
                    anzahltrefferabzug = (theoretischewurfpunkte - addpunkte) / dart;
                else anzahltrefferabzug = 0;
                if (closed(dart) && (anzahltrefferabzug > 0)) {
                    for (int i = 1; i <= spieleranzahl; i++) {
                        textfeld(dart, i, 1).setTextColor(textcolorpassiv);
                    }
                    switch (dart) {
                        case 1:
                            Button b1 = findViewById(R.id.b1);
                            b1.setEnabled(true);
                            break;
                        case 2:
                            Button b2 = findViewById(R.id.b2);
                            b2.setEnabled(true);
                            break;
                        case 3:
                            Button b3 = findViewById(R.id.b3);
                            b3.setEnabled(true);
                            break;
                        case 4:
                            Button b4 = findViewById(R.id.b4);
                            b4.setEnabled(true);
                            break;
                        case 5:
                            Button b5 = findViewById(R.id.b5);
                            b5.setEnabled(true);
                            break;
                        case 6:
                            Button b6 = findViewById(R.id.b6);
                            b6.setEnabled(true);
                            break;
                        case 7:
                            Button b7 = findViewById(R.id.b7);
                            b7.setEnabled(true);
                            break;
                        case 8:
                            Button b8 = findViewById(R.id.b8);
                            b8.setEnabled(true);
                            break;
                        case 9:
                            Button b9 = findViewById(R.id.b9);
                            b9.setEnabled(true);
                            break;
                        case 10:
                            Button b10 = findViewById(R.id.b10);
                            b10.setEnabled(true);
                            break;
                        case 11:
                            Button b11 = findViewById(R.id.b11);
                            b11.setEnabled(true);
                            break;
                        case 12:
                            Button b12 = findViewById(R.id.b12);
                            b12.setEnabled(true);
                            break;
                        case 13:
                            Button b13 = findViewById(R.id.b13);
                            b13.setEnabled(true);
                            break;
                        case 14:
                            Button b14 = findViewById(R.id.b14);
                            b14.setEnabled(true);
                            break;
                        case 15:
                            Button b15 = findViewById(R.id.b15);
                            b15.setEnabled(true);
                            break;
                        case 16:
                            Button b16 = findViewById(R.id.b16);
                            b16.setEnabled(true);
                            break;
                        case 17:
                            Button b17 = findViewById(R.id.b17);
                            b17.setEnabled(true);
                            break;
                        case 18:
                            Button b18 = findViewById(R.id.b18);
                            b18.setEnabled(true);
                            break;
                        case 19:
                            Button b19 = findViewById(R.id.b19);
                            b19.setEnabled(true);
                            break;
                        case 20:
                            Button b20 = findViewById(R.id.b20);
                            b20.setEnabled(true);
                            break;
                        case 25:
                            Button b25 = findViewById(R.id.bull);
                            b25.setEnabled(true);
                            break;
                    }
                }
                spieler[aktiverSpieler].treffer[dart] -= anzahltrefferabzug;
                if (dart != 0)
                    switch (spieler[aktiverSpieler].treffer[dart]) {
                        case 0:
                            textfeld(dart, aktiverSpieler, 1).setText("");
                            break;
                        case 1:
                            textfeld(dart, aktiverSpieler, 1).setText("/");
                            break;
                        case 2:
                            textfeld(dart, aktiverSpieler, 1).setText("X");
                            break;
                        case 3:
                            textfeld(dart, aktiverSpieler, 1).setText("O");
                            break;
                    }
                String str = "";
                switch (faktor) {
                    case 0:
                        str = "undo: " + getResources().getString(R.string.daneben);
                        break;
                    case 1:
                        str = "undo: " + dart;
                        break;
                    case 2:
                        str = "undo: Double " + dart;
                        break;
                    case 3:
                        str = "undo: Triple " + dart;
                        break;
                }
                str = str + " " + getResources().getString(R.string.von_spieler) + " " + spieler[wuerfe.get(ii).spielerindex].spielerName;

                Toast.makeText(cricket.this, str, Toast.LENGTH_SHORT).show();

                //score textfeld aktualisiere
                if (spielvariante.equals("Classic")) {
                    textfeld(1, aktiverSpieler, 2).setText(Integer.toString(spieler[aktiverSpieler].score));
                } else if (spielvariante.equals("Cut Throat")) {
                    for (int i = 1; i <= spieleranzahl; i++) {
                        if (i != aktiverSpieler)
                            textfeld(1, i, 2).setText(Integer.toString(spieler[i].score));
                    }
                }
                ii--;
            } else Toast.makeText(cricket.this, R.string.keinundo, Toast.LENGTH_LONG).show();
        }
    };

    private boolean spielzuende() {
        // annahme: spielgehtweiter ist aktiviert
        int gewinnerplatz = 1;
        int[] spielerplaetze = new int[spieleranzahl+1]; //+1 wegen index0

        // gewinnerplätze auslesen als arbeitskopie
        for (int index=1;index<=spieleranzahl;index++) {
            if (index==aktiverSpieler) continue;
            spielerplaetze[index] = spieler[index].gewinnerplatz;
            // richtigen platz des aktuellen spielers bestimmen
            if (spielerplaetze[index] != 0)
                gewinnerplatz++;
        }
        spielerplaetze[aktiverSpieler]=gewinnerplatz;
        spieler[aktiverSpieler].gewinnerplatz=99; // fakewert um gewinnerfunktion zu täuschen
        boolean eingewinner=true;
        while (eingewinner) {
            eingewinner=false;
            for (int index=1;index<=spieleranzahl;index++) {
                if (index==aktiverSpieler) continue;
                if ((spielerplaetze[index]==0) && gewinner(index)) {
                    spielerplaetze[index]=99;
                    spieler[index].gewinnerplatz=99; //zum testen, platz vergeben (zum täuschen der gewinnerfunktion)
                    eingewinner=true;
                }
            }
        }
        // spielerplätze auf originalwerte zurücksetzen
        for (int index=1;index<=spieleranzahl;index++) if (spieler[index].gewinnerplatz==99) spieler[index].gewinnerplatz=0;
        // hauptcheck: gibts ein spieler der noch keinen platz hat?
        for (int index=1;index<=spieleranzahl;index++) if (spielerplaetze[index]==0) return false;
        return true;
    }

    private final View.OnClickListener weiter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button daneben=findViewById(R.id.daneben);
            switch (xdart) {
                case 0:
                    daneben.performClick();
                case 1:
                    daneben.performClick();
                case 2:
                    daneben.performClick();
            }
        }
    };

    private final View.OnClickListener buttonclick = new View.OnClickListener() {
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {

            int sender = v.getId();
            Button eingabe = findViewById(sender);
            int dart,
                    addpunkte = 0;

            //ergebnis = Integer.parseInt(score.getText().toString());

            // welche taste wurde gedrückt?
            switch (sender) {
                case R.id.bull:
                    dart = 25;
                    break;
                case R.id.daneben:
                    dart = 0;
                    break;
                default:
                    try {
                        dart = Integer.parseInt(eingabe.getText().toString());
                    } catch (Exception e) {
                        dart = 255;         //fehlerwert - sollte niemals eintreten
                    }
                    break;
            }

            int faktor;
            if (d) //double?
            {
                faktor = 2;
                d = false;
                Button doubleb = findViewById(R.id.doublebutton);
                doubleb.setBackgroundColor(bcolorn);
            } else if (t)  //triple?
            {
                faktor = 3;
                t = false;
                Button tripleb = findViewById(R.id.triple);
                tripleb.setBackgroundColor(bcolorn);
                if (dart == 25) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.triplewirklich), Toast.LENGTH_LONG).show();
                    return;
                }
            } else //single!
            {
                faktor = 1;
            }


            pfeil aktuellerWurf;
            if (dart != 0) {
                int i;
                //folgende 3 zeilen zum undo-speichern
                aktuellerWurf = new pfeil();
                if (!closed(dart)) aktuellerWurf.faktor = faktor;
                else aktuellerWurf.faktor = 0;

                for (i = faktor; i > 0; i--) {
                    if (spieler[aktiverSpieler].treffer[dart] < 3) {
                        spieler[aktiverSpieler].treffer[dart]++;
                    } else addpunkte += dart;
                }

                if (!closed(dart)) {
                    aufnahme += addpunkte;
                    // klassisches Cricket?
                    if (spielvariante.equals("Classic")) {
                        spieler[aktiverSpieler].score += addpunkte;
                    }
                    // Cut Throat Cricket?
                    else if (spielvariante.equals("Cut Throat")) {
                        for (i = 1; i <= spieleranzahl; i++) {
                            if ((i != aktiverSpieler) && !(zahlClosed(i, dart)))
                                spieler[i].score += addpunkte;
                        }

                    }

                } else {
                    for (i = 1; i <= spieleranzahl; i++) {
                        textfeld(dart, i, 1).setTextColor(Color.RED);
                    }
                    switch (dart) {
                        case 1:
                            Button b1 = findViewById(R.id.b1);
                            b1.setEnabled(false);
                            break;
                        case 2:
                            Button b2 = findViewById(R.id.b2);
                            b2.setEnabled(false);
                            break;
                        case 3:
                            Button b3 = findViewById(R.id.b3);
                            b3.setEnabled(false);
                            break;
                        case 4:
                            Button b4 = findViewById(R.id.b4);
                            b4.setEnabled(false);
                            break;
                        case 5:
                            Button b5 = findViewById(R.id.b5);
                            b5.setEnabled(false);
                            break;
                        case 6:
                            Button b6 = findViewById(R.id.b6);
                            b6.setEnabled(false);
                            break;
                        case 7:
                            Button b7 = findViewById(R.id.b7);
                            b7.setEnabled(false);
                            break;
                        case 8:
                            Button b8 = findViewById(R.id.b8);
                            b8.setEnabled(false);
                            break;
                        case 9:
                            Button b9 = findViewById(R.id.b9);
                            b9.setEnabled(false);
                            break;
                        case 10:
                            Button b10 = findViewById(R.id.b10);
                            b10.setEnabled(false);
                            break;
                        case 11:
                            Button b11 = findViewById(R.id.b11);
                            b11.setEnabled(false);
                            break;
                        case 12:
                            Button b12 = findViewById(R.id.b12);
                            b12.setEnabled(false);
                            break;
                        case 13:
                            Button b13 = findViewById(R.id.b13);
                            b13.setEnabled(false);
                            break;
                        case 14:
                            Button b14 = findViewById(R.id.b14);
                            b14.setEnabled(false);
                            break;
                        case 15:
                            Button b15 = findViewById(R.id.b15);
                            b15.setEnabled(false);
                            break;
                        case 16:
                            Button b16 = findViewById(R.id.b16);
                            b16.setEnabled(false);
                            break;
                        case 17:
                            Button b17 = findViewById(R.id.b17);
                            b17.setEnabled(false);
                            break;
                        case 18:
                            Button b18 = findViewById(R.id.b18);
                            b18.setEnabled(false);
                            break;
                        case 19:
                            Button b19 = findViewById(R.id.b19);
                            b19.setEnabled(false);
                            break;
                        case 20:
                            Button b20 = findViewById(R.id.b20);
                            b20.setEnabled(false);
                            break;
                        case 25:
                            Button b25 = findViewById(R.id.bull);
                            b25.setEnabled(false);
                            break;
                    }

                    Toast.makeText(cricket.this, dart + " " + getResources().getString(R.string.closed), Toast.LENGTH_SHORT).show();
                }


                //restliche undo daten speichern
                ii++;
                if (!closed(dart)) {
                    //aktuellerWurf.faktor=faktor;
                    aktuellerWurf.addpunkte = addpunkte;
                } else {
                    aktuellerWurf.addpunkte = 0;
                    if (addpunkte > 0)
                        aktuellerWurf.faktor = faktor - (addpunkte / dart); //falls closed durch faktor
                }

                aktuellerWurf.spielerindex = aktiverSpieler;
                aktuellerWurf.zahl = dart;
                wuerfe.add(ii, aktuellerWurf);


                switch (spieler[aktiverSpieler].treffer[dart]) {
                    case 1:
                        textfeld(dart, aktiverSpieler, 1).setText("/");
                        break;
                    case 2:
                        textfeld(dart, aktiverSpieler, 1).setText("X");
                        break;
                    case 3:
                        textfeld(dart, aktiverSpieler, 1).setText("O");
                        break;
                }

                if (spielvariante.equals("Classic")) {
                    textfeld(1, aktiverSpieler, 2).setText(Integer.toString(spieler[aktiverSpieler].score));
                } else if (spielvariante.equals("Cut Throat")) {
                    for (i = 1; i <= spieleranzahl; i++) {
                        if (i != aktiverSpieler)
                            textfeld(1, i, 2).setText(Integer.toString(spieler[i].score));
                    }
                }



                //spiel zuende?
                if (gewinner(aktiverSpieler)) {

                    AlertDialog alertDialog = new AlertDialog.Builder(cricket.this).create();
                    alertDialog.setTitle(getResources().getString(R.string.achtung));
                    alertDialog.setMessage(getResources().getString(R.string.letzteEingabeKorrekt));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ja),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    if (gewinner(aktiverSpieler) && spielgehtweiter) {
                                        textfeld(0, aktiverSpieler, 2).setPaintFlags(textfeld(0, aktiverSpieler, 2).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                        textfeld(0, aktiverSpieler, 2).setTextColor(Color.RED);
                                        textfeld(0, aktiverSpieler, 3).setPaintFlags(textfeld(0, aktiverSpieler, 3).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                        textfeld(0, aktiverSpieler, 3).setTextColor(Color.RED);
                                        xdart = 2;
                                        spieler[aktiverSpieler].gewinnerplatz = 1;
                                        // richtigen platz bestimmen
                                        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++) {
                                            if ((spieler[spielerindex].gewinnerplatz != 0) && (spielerindex != aktiverSpieler))
                                                spieler[aktiverSpieler].gewinnerplatz++;
                                        }
                                        // check spezialfall: wenn andere spieler alle zahlen offen haben und nur zu wenig punkte hatten, sind sie evtl. nun auch fertig
                                        for (int anzahl = 1; anzahl <= spieleranzahl; anzahl++) { //zur sicherheit, falls die spielerreihenfolge "ungünstig" ist bezogen auf gewinner() - siehe if abfrage
                                            for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++) {
                                                //    if (spielerindex == aktiverSpieler) continue;
                                                // wenn ein spieler noch keinen rang/platz hat und nun fertig/gewinner ist, dann...
                                                if ((spieler[spielerindex].gewinnerplatz == 0) && (gewinner(spielerindex))) {
                                                    textfeld(0, spielerindex, 2).setTextColor(Color.RED);
                                                    textfeld(0, aktiverSpieler, 2).setPaintFlags(textfeld(0, aktiverSpieler, 2).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                                    textfeld(0, spielerindex, 3).setTextColor(Color.RED);
                                                    textfeld(0, aktiverSpieler, 3).setPaintFlags(textfeld(0, aktiverSpieler, 3).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                                    spieler[spielerindex].gewinnerplatz = 1;
                                                    // richtigen platz bestimmen
                                                    for (int spielerindex2 = 1; spielerindex2 <= spieleranzahl; spielerindex2++) {
                                                        if ((spieler[spielerindex2].gewinnerplatz != 0) && (spielerindex2 != spielerindex))
                                                            spieler[spielerindex].gewinnerplatz++;
                                                    }
                                                    // nur noch 2 oder weniger spieler übrig?
                                                    if (spieler[spielerindex].gewinnerplatz >= spieleranzahl - 1) {
                                                        spielgehtweiter = false;
                                                        //letzten Spieler auf seinen Platz "verweisen"
                                                        for (int spielerindex3 = 1; spielerindex3 <= spieleranzahl; spielerindex3++) {
                                                            if (spieler[spielerindex3].gewinnerplatz == 0) {
                                                                spieler[spielerindex3].gewinnerplatz = spieleranzahl; //(spieleranzahl==letzterplatz)
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ((gewinner(aktiverSpieler) || alleZahlenClosed()) && !(spielgehtweiter)) {
                                        //spielende einleiten und aufrufen
                                        long spielzeit = (System.currentTimeMillis() - startTime) / 1000;
                                        Intent intent = new Intent(cricket.this, spielende.class);
                                        intent.putExtra("anzahl", spieleranzahl);
                                        intent.putExtra("cricket", true);
                                        intent.putExtra("spielzeit", spielzeit);
                                        SharedPreferences sp = getSharedPreferences("Einstellungen", 0);
                                        if (sp.contains("spielgehtweiter"))
                                            spielgehtweiter = sp.getBoolean("spielgehtweiter", false);
                                        intent.putExtra("spielgehtweiter", spielgehtweiter);
                                        if (spielgehtweiter) {
                                            for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++) {
                                                switch (spieler[spielerindex].gewinnerplatz) {
                                                    case 1:
                                                        intent.putExtra("erster", spieler[spielerindex].spielerName);
                                                        intent.putExtra("ersterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 2:
                                                        intent.putExtra("zweiter", spieler[spielerindex].spielerName);
                                                        intent.putExtra("zweiterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 3:
                                                        intent.putExtra("dritter", spieler[spielerindex].spielerName);
                                                        intent.putExtra("dritterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 4:
                                                        intent.putExtra("vierter", spieler[spielerindex].spielerName);
                                                        intent.putExtra("vierterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 5:
                                                        intent.putExtra("fuenfter", spieler[spielerindex].spielerName);
                                                        intent.putExtra("fuenfterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 6:
                                                        intent.putExtra("sechster", spieler[spielerindex].spielerName);
                                                        intent.putExtra("sechsterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 7:
                                                        intent.putExtra("siebenter", spieler[spielerindex].spielerName);
                                                        intent.putExtra("siebenterscore", spieler[spielerindex].score);
                                                        break;
                                                    case 8:
                                                        intent.putExtra("achter", spieler[spielerindex].spielerName);
                                                        intent.putExtra("achtererscore", spieler[spielerindex].score);
                                                        break;
                                                }
                                            }
                                            startActivity(intent);
                                            finish();
                                            return;
                                        } else {
                                            intent.putExtra("erster", spieler[aktiverSpieler].spielerName);
                                            intent.putExtra("ersterscore", spieler[aktiverSpieler].score);

                                            if (spieleranzahl == 2) {
                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        intent.putExtra("zweiter", spieler[2].spielerName);
                                                        intent.putExtra("zweiterscore", spieler[2].score);
                                                        break;
                                                    case 2:
                                                        intent.putExtra("zweiter", spieler[1].spielerName);
                                                        intent.putExtra("zweiterscore", spieler[1].score);
                                                        break;
                                                }

                                            } else if (spieleranzahl == 3) {
                                                player s2 = new player(), s3 = new player();
                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        s2 = spieler[2];
                                                        s3 = spieler[3];
                                                        break;
                                                    case 2:
                                                        s2 = spieler[1];
                                                        s3 = spieler[3];
                                                        break;
                                                    case 3:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        break;
                                                }
                                                intent.putExtra("zweiter", s2.spielerName);
                                                intent.putExtra("zweiterscore", s2.score);
                                                intent.putExtra("dritter", s3.spielerName);
                                                intent.putExtra("dritterscore", s3.score);

                                            } else if (spieleranzahl == 4) {

                                                player s2 = new player(), s3 = new player(), s4 = new player();

                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        s2 = spieler[2];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        break;
                                                    case 2:
                                                        s2 = spieler[1];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        break;
                                                    case 3:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[4];
                                                        break;
                                                    case 4:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        break;
                                                }
                                                intent.putExtra("zweiter", s2.spielerName);
                                                intent.putExtra("zweiterscore", s2.score);
                                                intent.putExtra("dritter", s3.spielerName);
                                                intent.putExtra("dritterscore", s3.score);
                                                intent.putExtra("vierter", s4.spielerName);
                                                intent.putExtra("vierterscore", s4.score);

                                            } else if (spieleranzahl == 5) {

                                                player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player();

                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        s2 = spieler[2];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        break;
                                                    case 2:
                                                        s2 = spieler[1];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        break;
                                                    case 3:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        break;
                                                    case 4:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[5];
                                                        break;
                                                    case 5:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        break;
                                                }
                                                intent.putExtra("zweiter", s2.spielerName);
                                                intent.putExtra("zweiterscore", s2.score);
                                                intent.putExtra("dritter", s3.spielerName);
                                                intent.putExtra("dritterscore", s3.score);
                                                intent.putExtra("vierter", s4.spielerName);
                                                intent.putExtra("vierterscore", s4.score);
                                                intent.putExtra("fuenfter", s5.spielerName);
                                                intent.putExtra("fuenfterscore", s5.score);

                                            } else if (spieleranzahl == 6) {

                                                player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player(), s6 = new player();

                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        s2 = spieler[2];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        break;
                                                    case 2:
                                                        s2 = spieler[1];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        break;
                                                    case 3:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        break;
                                                    case 4:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        break;
                                                    case 5:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s6 = spieler[6];
                                                        break;
                                                    case 6:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s6 = spieler[5];
                                                        break;
                                                }
                                                intent.putExtra("zweiter", s2.spielerName);
                                                intent.putExtra("zweiterscore", s2.score);
                                                intent.putExtra("dritter", s3.spielerName);
                                                intent.putExtra("dritterscore", s3.score);
                                                intent.putExtra("vierter", s4.spielerName);
                                                intent.putExtra("vierterscore", s4.score);
                                                intent.putExtra("fuenfter", s5.spielerName);
                                                intent.putExtra("fuenfterscore", s5.score);
                                                intent.putExtra("sechster", s6.spielerName);
                                                intent.putExtra("sechsterscore", s6.score);

                                            } else if (spieleranzahl == 7) {

                                                player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player(), s6 = new player(), s7 = new player();

                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        s2 = spieler[2];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        break;
                                                    case 2:
                                                        s2 = spieler[1];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        break;
                                                    case 3:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        break;
                                                    case 4:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        break;
                                                    case 5:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        break;
                                                    case 6:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s7 = spieler[7];
                                                        s6 = spieler[5];
                                                        break;
                                                    case 7:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s7 = spieler[6];
                                                        s6 = spieler[5];
                                                        break;
                                                }
                                                intent.putExtra("zweiter", s2.spielerName);
                                                intent.putExtra("zweiterscore", s2.score);
                                                intent.putExtra("dritter", s3.spielerName);
                                                intent.putExtra("dritterscore", s3.score);
                                                intent.putExtra("vierter", s4.spielerName);
                                                intent.putExtra("vierterscore", s4.score);
                                                intent.putExtra("fuenfter", s5.spielerName);
                                                intent.putExtra("fuenfterscore", s5.score);
                                                intent.putExtra("sechster", s6.spielerName);
                                                intent.putExtra("sechsterscore", s6.score);
                                                intent.putExtra("siebenter", s7.spielerName);
                                                intent.putExtra("siebenterscore", s7.score);

                                            } else if (spieleranzahl == 8) {

                                                player s2 = new player(), s3 = new player(), s4 = new player(), s5 = new player(), s6 = new player(), s7 = new player(), s8 = new player();

                                                switch (aktiverSpieler) {
                                                    case 1:
                                                        s2 = spieler[2];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 2:
                                                        s2 = spieler[1];
                                                        s3 = spieler[3];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 3:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[4];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 4:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[5];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 5:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s6 = spieler[6];
                                                        s7 = spieler[7];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 6:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s7 = spieler[7];
                                                        s6 = spieler[5];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 7:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s7 = spieler[6];
                                                        s6 = spieler[5];
                                                        s8 = spieler[8];
                                                        break;
                                                    case 8:
                                                        s2 = spieler[1];
                                                        s3 = spieler[2];
                                                        s4 = spieler[3];
                                                        s5 = spieler[4];
                                                        s7 = spieler[6];
                                                        s6 = spieler[5];
                                                        s8 = spieler[7];
                                                        break;
                                                }
                                                intent.putExtra("zweiter", s2.spielerName);
                                                intent.putExtra("zweiterscore", s2.score);
                                                intent.putExtra("dritter", s3.spielerName);
                                                intent.putExtra("dritterscore", s3.score);
                                                intent.putExtra("vierter", s4.spielerName);
                                                intent.putExtra("vierterscore", s4.score);
                                                intent.putExtra("fuenfter", s5.spielerName);
                                                intent.putExtra("fuenfterscore", s5.score);
                                                intent.putExtra("sechster", s6.spielerName);
                                                intent.putExtra("sechsterscore", s6.score);
                                                intent.putExtra("siebenter", s7.spielerName);
                                                intent.putExtra("siebenterscore", s7.score);
                                                intent.putExtra("achter", s8.spielerName);
                                                intent.putExtra("achterscore", s8.score);
                                            }

                                            startActivity(intent);
                                            finish();
                                            return;
                                        }
                                    }
                                    TextView darts = findViewById(R.id.darts);
                                    if (xdart < 2) {
                                        xdart++;
                                        s = s + (".");

                                    } else {  //Spielerwechsel
                                        xdart = 0;
                                        s = "";

                                        if ((aufnahme > 0) || (spieler[aktiverSpieler].gewinnerplatz != 0)) {
                                            buttonfreeze(true);
                                            final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                                            if (spieler[aktiverSpieler].gewinnerplatz != 0)
                                                aufnahmetv.setText(spieler[aktiverSpieler].gewinnerplatz + ".");
                                            else aufnahmetv.setText(Integer.toString(aufnahme));
                                            aufnahmetv.setVisibility(View.VISIBLE);
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    buttonfreeze(false);
                                                    aufnahmetv.setVisibility(View.INVISIBLE);
                                                }
                                            }, changetime);
                                            aufnahme = 0;
                                        }
                                        //if (spieler[aktiverSpieler].gewinnerplatz == spieleranzahl-1) return;
                                        if (spieler[aktiverSpieler].gewinnerplatz != 0) {
                                            textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                            textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                        } else {
                                            textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                                            textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                                            textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                            textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                        }
                                        do {
                                            if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                                            else aktiverSpieler = 1;
                                        } while (spieler[aktiverSpieler].gewinnerplatz != 0);    // für cricket über die volle distanz
                                        textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                                        textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                                        if (spielmodus.equals("Crazy")) {
                                            crazySegmente segment = new crazySegmente();
                                            speichereSegmente(segment);
                                            crazySegmentes.add(segment);
                                            randomfieldselect();
                                        }
                                    }
                                    darts.setText(s);
                                }
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            // letzter Wurf war eine Fehleingabe -> Spiel nicht beenden und letzte Eingabe zurücknehmen ohne nochmal nachzufragen
                            TextView darts = findViewById(R.id.darts);
                            if (xdart < 2) {
                                xdart++;
                                s = s + (".");

                            } else {  //Spielerwechsel
                                xdart = 0;
                                s = "";
                            }
                            darts.setText(s);

                            undoabfrage = false;
                            Button undo = findViewById(R.id.undo);
                            undo.performClick();
                        }
                    });

                    if (spielzuende() || !spielgehtweiter) {
                        alertDialog.show();
                    }
                    else {
                        alertDialog.show();
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                    }
                    return;
                }
            }   // kein Treffer
                else if (!gewinner(aktiverSpieler)) {
                    ii++;
                    aktuellerWurf = new pfeil();
                    aktuellerWurf.faktor = 0;
                    aktuellerWurf.addpunkte = 0;
                    aktuellerWurf.zahl = dart;
                    aktuellerWurf.spielerindex = aktiverSpieler;
                    wuerfe.add(ii, aktuellerWurf);
                }
                // Spiel geht weiter

                TextView darts = findViewById(R.id.darts);
                if (xdart < 2) {
                    xdart++;
                    s = s + (".");

                } else {  //Spielerwechsel
                    xdart = 0;
                    s = "";

                    if ((aufnahme > 0) || (spieler[aktiverSpieler].gewinnerplatz != 0)) {
                        buttonfreeze(true);
                        final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                        if (spieler[aktiverSpieler].gewinnerplatz != 0)
                            aufnahmetv.setText(spieler[aktiverSpieler].gewinnerplatz + ".");
                        else aufnahmetv.setText(Integer.toString(aufnahme));
                        aufnahmetv.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                buttonfreeze(false);
                                aufnahmetv.setVisibility(View.INVISIBLE);
                            }
                        }, changetime);
                        aufnahme = 0;
                    }
                    //if (spieler[aktiverSpieler].gewinnerplatz == spieleranzahl-1) return;
                    if (spieler[aktiverSpieler].gewinnerplatz != 0) {
                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    } else {
                        textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                        textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                        textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                        textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    }
                    do {
                        if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                        else aktiverSpieler = 1;
                    } while (spieler[aktiverSpieler].gewinnerplatz != 0);    // für cricket über die volle distanz
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    if (spielmodus.equals("Crazy")) {
                        crazySegmente segment = new crazySegmente();
                        speichereSegmente(segment);
                        crazySegmentes.add(segment);
                        randomfieldselect();
                    }
                }
                darts.setText(s);


        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_cricket);
        TypedValue outValue = new TypedValue();
        cricket.this.getTheme().resolveAttribute(R.attr.colorButtonNormal, outValue, true);
        bcolor = outValue.data;
        cricket.this.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
        bcolorn = outValue.data;
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        ConstraintLayout main = findViewById(R.id.matchbereich);
        if (settings.contains("keepscreenongame")) main.setKeepScreenOn(settings.getBoolean("keepscreenongame",true));

        TextView p1name = findViewById(R.id.p1name);
        TextView p2name = findViewById(R.id.p2name);
        textcoloraktiv = p1name.getCurrentTextColor();
        textcolorpassiv = p2name.getCurrentTextColor();
        textsizepassiv = p2name.getTextSize();
        textsizeaktiv = textsizepassiv + 4;
        textfeld(1, 1, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
        textfeld(1, 1, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);

        Intent intent = getIntent();
        spieleranzahl = Integer.parseInt(intent.getStringExtra("spieleranzahl"));
        spielmodus = intent.getCharSequenceExtra("spielmodus");
        CharSequence spieler1n = intent.getCharSequenceExtra("spieler1");
        CharSequence spieler2n = intent.getCharSequenceExtra("spieler2");
        CharSequence spieler3n = intent.getCharSequenceExtra("spieler3");
        CharSequence spieler4n = intent.getCharSequenceExtra("spieler4");
        CharSequence spieler5n = intent.getCharSequenceExtra("spieler5");
        CharSequence spieler6n = intent.getCharSequenceExtra("spieler6");
        CharSequence spieler7n = intent.getCharSequenceExtra("spieler7");
        CharSequence spieler8n = intent.getCharSequenceExtra("spieler8");
        spielvariante = intent.getCharSequenceExtra("spielvariante");

        player s1 = new player();
        player s2 = new player();
        player s3 = new player();
        player s4 = new player();
        player s5 = new player();
        player s6 = new player();
        player s7 = new player();
        player s8 = new player();

        spieler[1] = s1;
        spieler[2] = s2;
        spieler[3] = s3;
        spieler[4] = s4;
        spieler[5] = s5;
        spieler[6] = s6;
        spieler[7] = s7;
        spieler[8] = s8;
        for (int a = 1; a <= spieleranzahl; a++) spieler[a].score = 0;

        spieler[1].spielerName = spieler1n.toString();
        spieler[2].spielerName = spieler2n.toString();
        spieler[3].spielerName = spieler3n.toString();
        spieler[4].spielerName = spieler4n.toString();
        spieler[5].spielerName = spieler5n.toString();
        spieler[6].spielerName = spieler6n.toString();
        spieler[7].spielerName = spieler7n.toString();
        spieler[8].spielerName = spieler8n.toString();

        TextView p3name = findViewById(R.id.p3name);
        TextView p4name = findViewById(R.id.p4name);
        TextView p5name = findViewById(R.id.p5name);
        TextView p6name = findViewById(R.id.p6name);
        TextView p7name = findViewById(R.id.p7name);
        TextView p8name = findViewById(R.id.p8name);
        TextView p2score = findViewById(R.id.p2score);
        TextView p3score = findViewById(R.id.p3score);
        TextView p4score = findViewById(R.id.p4score);
        TextView p5score = findViewById(R.id.p5score);
        TextView p6score = findViewById(R.id.p6score);
        TextView p7score = findViewById(R.id.p7score);
        TextView p8score = findViewById(R.id.p8score);

        // Namenszuweisung und Layout der Namensanzeige in Abhängigkeit von der Anzahl der Spieler ändern
        p1name.setText(spieler[1].spielerName);
        switch (spieleranzahl) {
            case 2:
                ConstraintSet cs = new ConstraintSet();
                cs.clone(main);
                cs.connect(p2name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
                cs.applyTo(main);
                p3name.setVisibility(View.GONE);
                p4name.setVisibility(View.GONE);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p3score.setVisibility(View.GONE);
                p4score.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                break;
            case 1:
                p2name.setVisibility(View.GONE);
                p3name.setVisibility(View.GONE);
                p4name.setVisibility(View.GONE);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p2score.setVisibility(View.GONE);
                p3score.setVisibility(View.GONE);
                p4score.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                break;
            case 3:
                ConstraintSet cs3 = new ConstraintSet();
                cs3.clone(main);
                cs3.connect(p2name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs3.connect(p3name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.END);
                cs3.connect(p3name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
                cs3.applyTo(main);
                p4name.setVisibility(View.GONE);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p4score.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                break;
            case 4:
                ConstraintSet cs4 = new ConstraintSet();
                cs4.clone(main);
                cs4.connect(p2name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs4.connect(p3name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.START);
                cs4.connect(p3name.getId(), ConstraintSet.END, R.id.p4name, ConstraintSet.START);
                cs4.connect(p4name.getId(), ConstraintSet.START, R.id.p3name, ConstraintSet.END);
                cs4.connect(p4name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
                cs4.applyTo(main);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                break;
            case 5:
                ConstraintSet cs5 = new ConstraintSet();
                cs5.clone(main);
                cs5.connect(p3name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs5.connect(p4name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.START);
                cs5.connect(p4name.getId(), ConstraintSet.END, R.id.p5name, ConstraintSet.START);
                cs5.connect(p5name.getId(), ConstraintSet.START, R.id.p4name, ConstraintSet.END);
                cs5.applyTo(main);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                break;
            case 6:
                ConstraintSet cs6 = new ConstraintSet();
                cs6.clone(main);
                cs6.connect(p3name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs6.connect(p4name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.START);
                cs6.connect(p4name.getId(), ConstraintSet.END, R.id.p5name, ConstraintSet.START);
                cs6.connect(p5name.getId(), ConstraintSet.START, R.id.p4name, ConstraintSet.END);
                cs6.applyTo(main);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                p6name.setText(spieler[6].spielerName);
                break;
            case 7:
                p8name.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                p6name.setText(spieler[6].spielerName);
                p7name.setText(spieler[7].spielerName);
                break;
            case 8:
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                p6name.setText(spieler[6].spielerName);
                p7name.setText(spieler[7].spielerName);
                p8name.setText(spieler[8].spielerName);
                break;
        }

        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button restdaneben = findViewById(R.id.weiter);
        Button bundo = findViewById(R.id.undo);
        b1.setOnClickListener(buttonclick);
        b2.setOnClickListener(buttonclick);
        b3.setOnClickListener(buttonclick);
        b4.setOnClickListener(buttonclick);
        b5.setOnClickListener(buttonclick);
        b6.setOnClickListener(buttonclick);
        b7.setOnClickListener(buttonclick);
        b8.setOnClickListener(buttonclick);
        b9.setOnClickListener(buttonclick);
        b10.setOnClickListener(buttonclick);
        b11.setOnClickListener(buttonclick);
        b12.setOnClickListener(buttonclick);
        b13.setOnClickListener(buttonclick);
        b14.setOnClickListener(buttonclick);
        b15.setOnClickListener(buttonclick);
        b16.setOnClickListener(buttonclick);
        b17.setOnClickListener(buttonclick);
        b18.setOnClickListener(buttonclick);
        b19.setOnClickListener(buttonclick);
        b20.setOnClickListener(buttonclick);
        daneben.setOnClickListener(buttonclick);
        restdaneben.setOnClickListener(weiter);
        bull.setOnClickListener(buttonclick);
        bdouble.setOnClickListener(doubletriple);
        btripel.setOnClickListener(doubletriple);
        bundo.setOnClickListener(undoclick);

        if (settings.contains("changetime")) changetime = settings.getInt("changetime", 1500);
        if (settings.contains("spielgehtweiter"))
            spielgehtweiter = settings.getBoolean("spielgehtweiter", false);
        if (settings.contains("crazystart"))
            crazycricketRangeMinimum = settings.getInt("crazystart",6);


        //Layout in Abhängikeit vom spielmodus ändern

        if (!spielmodus.equals("Crazy")) {
            b1.setVisibility(View.GONE);
            b2.setVisibility(View.GONE);
            b3.setVisibility(View.GONE);
            b4.setVisibility(View.GONE);
            b5.setVisibility(View.GONE);
            b6.setVisibility(View.GONE);
            b7.setVisibility(View.GONE);
            b8.setVisibility(View.GONE);
            b9.setVisibility(View.GONE);
        }

        if (spielmodus.equals("15-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b13.setVisibility(View.GONE);
            b14.setVisibility(View.GONE);
        } else if (spielmodus.equals("14-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b13.setVisibility(View.GONE);
        } else if (spielmodus.equals("13-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
        } else if (spielmodus.equals("12-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
        } else if (spielmodus.equals("11-Bull")) {
            b10.setVisibility(View.GONE);
        } else if (spielmodus.equals("Crazy")) {
            initial_randomfieldselect(crazycricketFelderAnzahl);
        }
        findViewById(android.R.id.content).invalidate();

        startTime = System.currentTimeMillis();
    }

    private void ladeSegmente(crazySegmente seg) {
        for (int button = crazycricketRangeMinimum; button <= 20; button++) {
            if (seg.segmente[button]) zeigeFeld(button);
            else versteckeFeld(button);
        }
    }

    private void speichereSegmente(crazySegmente seg) {
        for (int button = crazycricketRangeMinimum; button <= 20; button++) {
            seg.segmente[button] = buttonselect(button).getVisibility() == View.VISIBLE;
        }
    }

    private void randomfieldselect() {
        boolean leereZahlenfelder = false;
        int anzahlBenoetigterNeuerZahlenfelder = 0;
        // zählung und ausblendung der ungetroffenen zahlenfelder
        for (int zahlenfeld = 1; zahlenfeld <= 20; zahlenfeld++) {
            if ((buttonselect(zahlenfeld).getVisibility() == View.VISIBLE) && (!open(zahlenfeld))) {
                leereZahlenfelder = true;
                versteckeFeld(zahlenfeld);
                anzahlBenoetigterNeuerZahlenfelder++;
            }
        }
        // wenn es keine zutauschenden zahlenfelder gibt, hat die funktion nichts mehr zu tun
        if (!leereZahlenfelder) return;
        // neue, zufällig gewählte Felder sichtbar machen
        Random rand = new Random();
        int neuesFeld;
        boolean unfertig;
        for (int anzahl = anzahlBenoetigterNeuerZahlenfelder; anzahl > 0; anzahl--) {
            unfertig = true;
            while (unfertig) {
                neuesFeld = rand.nextInt(21 - crazycricketRangeMinimum) + crazycricketRangeMinimum;
                if (buttonselect(neuesFeld).getVisibility() == View.GONE) {
                    zeigeFeld(neuesFeld);
                    unfertig = false;
                }
            }
        }
    }

    // wählt zufällige Zahlenfelder aus und versteckt nicht benötigte zahlenfelder
    private void initial_randomfieldselect(int felderAnzahl) {
        if (felderAnzahl > 20) return;
        for (int i = 1; i < crazycricketRangeMinimum; i++) {
            versteckeFeld(i);
        }

        Random rand = new Random();
        boolean unfertig;
        int feldnummer;
        for (int i = crazycricketRangeMinimum; i <= (20 - felderAnzahl + 1); i++) {
            unfertig = true;
            while (unfertig) {
                feldnummer = rand.nextInt(21 - crazycricketRangeMinimum) + crazycricketRangeMinimum;
                if (buttonselect(feldnummer).getVisibility() == View.VISIBLE) {
                    versteckeFeld(feldnummer);
                    unfertig = false;
                }
            }
        }

    }

    private void versteckeFeld(int feld) {
        buttonselect(feld).setVisibility(View.GONE);
        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++)
            textfeld(feld, spielerindex, 1).setVisibility(View.GONE);
    }

    private void zeigeFeld(int feld) {
        buttonselect(feld).setVisibility(View.VISIBLE);
        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++)
            textfeld(feld, spielerindex, 1).setVisibility(View.VISIBLE);
    }

    // hat irgendein spieler das zahlenfeld bereits 3x getroffen?
    private boolean open(int zahlenfeld) {
        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++) {
            if (spieler[spielerindex].treffer[zahlenfeld] == 3) {
                return true;
            }
        }
        return false;
    }

    private Button buttonselect(int buttonnumber) {
        String stt = "b" + buttonnumber;
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }

    // liefert das gewünschte textview zurück
    private TextView textfeld(int dart, int aktiverSpieler, int typ)
    // typ=1 -> trefferfeld
    // typ=2 -> scorefeld
    // typ=3 -> namensfeld
    {
        String stt = "";
        switch (typ) {
            case 1:
                if (dart == 25) {
                    stt = "tbull" + "p" + aktiverSpieler;
                } else {
                    stt = "t" + dart + "p" + aktiverSpieler;
                }
                break;
            case 2:
                stt = "p" + aktiverSpieler + "score";
                break;
            case 3:
                stt = "p" + aktiverSpieler + "name";
                break;
        }
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }

    //haben alle spieler das feld 3x getroffen?
    private boolean closed(int dart) {
        for (int j = 1; j <= spieleranzahl; j++) {
            if (spieler[j].treffer[dart] != 3) return false;
        }
        return true;
    }

    private boolean alleZahlenClosed() {
        for (int segment = 1; segment <= 20; segment++) {
            if ((buttonselect(segment).getVisibility() == View.VISIBLE) && !closed(segment)) {
                return false;
            }
        }
        return closed(25);
    }

    private boolean gewinner(int aktSpieler) {
        // wenn nur noch ein spieler im spiel ist, ist er der letzte "gewinner"
        if ((spieleranzahl > 1) && spielgehtweiter) {
            int anzahlrestlicherspieler = 0;
            for (int i = 1; i <= spieleranzahl; i++) {
                if ((spieler[i].gewinnerplatz == 0) && (i != aktSpieler)) {
                    anzahlrestlicherspieler++;
                }
            }
            if (anzahlrestlicherspieler == 0) return true;
        }

        if (spielvariante.equals("Classic")) {
            // gewinner ist, der a) mehr punkte hat als alle anderen mitspieler...
            for (int i = 1; i <= spieleranzahl; i++) {
                if ((spieler[aktSpieler].score < spieler[i].score) && (aktSpieler != i) && (spieler[i].gewinnerplatz == 0))
                    return false;
            }
        } else if (spielvariante.equals("Cut Throat")) {
            // bzw. gewinner ist, der a) am wenigsten Punkte hat als alle anderen mitspieler...
            for (int i = 1; i <= spieleranzahl; i++) {
                if ((spieler[aktSpieler].score > spieler[i].score) && (aktSpieler != i) && (spieler[i].gewinnerplatz == 0))
                    return false;
            }

        }

        // ...und b) der sämtliche zahlen geschlossen  (d.h. 3x getroffen) hat...
        if (spielmodus.equals("Crazy")) {
            for (int feld = 1; feld <= 20; feld++) {
                if ((buttonselect(feld).getVisibility() == View.VISIBLE) && (spieler[aktSpieler].treffer[feld] != 3))
                    return false;
            }

        } else {
            for (int d = Integer.parseInt(spielmodus.subSequence(0, 2).toString()); d <= 20; d++) {
                if (spieler[aktSpieler].treffer[d] != 3) return false;
            }
        }
        // ...inklusive bull
        return spieler[aktSpieler].treffer[25] == 3;
    }

    private void buttonfreeze(boolean freeze) {
        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button restdaneben = findViewById(R.id.weiter);
        Button bundo = findViewById(R.id.undo);

        freeze = !freeze;       //eine frage der logik ;-)

        bdouble.setEnabled(freeze);
        btripel.setEnabled(freeze);
        daneben.setEnabled(freeze);
        restdaneben.setEnabled(freeze);
        bundo.setEnabled(freeze);
        if (!freeze) {
            b1.setEnabled(false);
            b2.setEnabled(false);
            b3.setEnabled(false);
            b4.setEnabled(false);
            b5.setEnabled(false);
            b6.setEnabled(false);
            b7.setEnabled(false);
            b8.setEnabled(false);
            b9.setEnabled(false);
            b10.setEnabled(false);
            b11.setEnabled(false);
            b12.setEnabled(false);
            b13.setEnabled(false);
            b14.setEnabled(false);
            b15.setEnabled(false);
            b16.setEnabled(false);
            b17.setEnabled(false);
            b18.setEnabled(false);
            b19.setEnabled(false);
            b20.setEnabled(false);
            bull.setEnabled(false);

        } else {
            if (!closed(1)) b1.setEnabled(true);
            if (!closed(2)) b2.setEnabled(true);
            if (!closed(3)) b3.setEnabled(true);
            if (!closed(4)) b4.setEnabled(true);
            if (!closed(5)) b5.setEnabled(true);
            if (!closed(6)) b6.setEnabled(true);
            if (!closed(7)) b7.setEnabled(true);
            if (!closed(8)) b8.setEnabled(true);
            if (!closed(9)) b9.setEnabled(true);
            if (!closed(10)) b10.setEnabled(true);
            if (!closed(11)) b11.setEnabled(true);
            if (!closed(12)) b12.setEnabled(true);
            if (!closed(13)) b13.setEnabled(true);
            if (!closed(14)) b14.setEnabled(true);
            if (!closed(15)) b15.setEnabled(true);
            if (!closed(16)) b16.setEnabled(true);
            if (!closed(17)) b17.setEnabled(true);
            if (!closed(18)) b18.setEnabled(true);
            if (!closed(19)) b19.setEnabled(true);
            if (!closed(20)) b20.setEnabled(true);
            if (!closed(25)) bull.setEnabled(true);
        }

    }

    public void onBackPressed() {

        AlertDialog alertDialog = new AlertDialog.Builder(cricket.this).create();
        alertDialog.setTitle(getResources().getString(R.string.achtung));
        alertDialog.setMessage(getResources().getString(R.string.willstduverlassen));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.zuruck), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private static class player {
        final int[] treffer = new int[26]; // feld 25 ist anzahl_bull_treffer, ansonsten 10-20, bei crazy cricket auch mehr;
        String spielerName;
        int score;
        int darts;
        int gewinnerplatz;
    }

    private static class pfeil {
        int zahl;
        int faktor;
        int addpunkte;
        int spielerindex;
    }

    private static class crazySegmente {
        final boolean[] segmente = new boolean[26];
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    private boolean zahlClosed(int spielerindex, int dart) {

        return spieler[spielerindex].treffer[dart] == 3;
    }
}

