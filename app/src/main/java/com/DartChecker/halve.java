package com.DartChecker;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class halve extends AppCompatActivity {
    private final int crazycricketFelderAnzahl = 7;      // (exclusive bull)
    private int crazycricketRangeMinimum = 6;
    private final player[] spieler = new player[9];
    private final ArrayList<pfeil> wuerfe = new ArrayList<pfeil>();
    private final ArrayList<crazySegmente> crazySegmentes = new ArrayList<crazySegmente>();
    private final boolean[] segmentaktiv = new boolean[26];
    private long startTime = 0;
    private int bcolor;
    private int bcolorn;
    private int textcoloraktiv,
            textcolorpassiv,
            buttontextcolorenabled,
            buttontextcoloraktiv,
            buttontextcolordisabled;
    private float textsizeaktiv,
            textsizepassiv,
            textsizescoreaktiv,
            textsizescorepassiv;
    private boolean t = false;
    private boolean d = false;
    private final View.OnClickListener doubletriple = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button doubleb = findViewById(R.id.doublebutton);
            Button tripleb = findViewById(R.id.triple);
            Button doubleb_dt = findViewById(R.id.doublebutton_dt);
            Button tripleb_dt = findViewById(R.id.triple_dt);
            doubleb.setBackgroundColor(bcolorn);        // workaround for low api version (15)
            tripleb.setBackgroundColor(bcolorn);        // workaround for low api version (15)
            doubleb_dt.setBackgroundColor(bcolorn);
            tripleb_dt.setBackgroundColor(bcolorn);

            int x = v.getId();
            switch (x)
            {
                case R.id.doublebutton:
                    if (t) {
                        t = false;
                        tripleb.setBackgroundColor(bcolorn);
                    }
                    if (!d) {
                        d = true;
                        doubleb.setBackgroundColor(bcolor);
                    } else {
                        d = false;
                        doubleb.setBackgroundColor(bcolorn);
                    }
                    break;
                case R.id.triple:
                    if (d) {
                        d = false;
                        doubleb.setBackgroundColor(bcolorn);
                    }
                    if (!t) {
                        t = true;
                        tripleb.setBackgroundColor(bcolor);
                    } else {
                        t = false;
                        tripleb.setBackgroundColor(bcolorn);
                    }
                    break;
                case R.id.doublebutton_dt:
                    if (t) {
                        t = false;
                        tripleb_dt.setBackgroundColor(bcolorn);
                    }
                    if (!d) {
                        d = true;
                        doubleb_dt.setBackgroundColor(bcolor);
                    } else {
                        d = false;
                        doubleb_dt.setBackgroundColor(bcolorn);
                    }
                    break;
                case R.id.triple_dt:
                    if (d) {
                        d = false;
                        doubleb_dt.setBackgroundColor(bcolorn);
                    }
                    if (!t) {
                        t = true;
                        tripleb_dt.setBackgroundColor(bcolor);
                    } else {
                        t = false;
                        tripleb_dt.setBackgroundColor(bcolorn);
                    }
                    break;
            }
        }
    };
    public int spieleranzahl,
            aktiverSpieler = 1,       //index für namensarray
            xdart = 0,                //zähler für geworfene darts (0, 1 oder 2) pro runde
            aufnahme,
            changetime = 1500,
            ii = -1;                   //index für undo-speicher
    private String s = "";
    private CharSequence spielmodus;
    private boolean undoabfrage = true; //soll die undoabfrage beim undoclick erscheinen oder undo() ohne rückfrage durchgeführt werden - wichtig für frage beim letzten wurf

    // undo
    private final View.OnClickListener undoclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // sicherheitsabfrage
             if (undoabfrage) {
                AlertDialog alertDialog = new AlertDialog.Builder(halve.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.willstduUndo));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                undo();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
                 final SharedPreferences settings = getApplicationContext().getSharedPreferences("Einstellungen", 0);
                 if (settings.contains("Undosicherheitsabfrage")) {
                     if (!settings.getBoolean("Undosicherheitsabfrage",false)) {
                         alertDialog.hide();
                         alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                     }
                 }
            } else {
                undoabfrage = true;
                undo();
            }
        }

        private void undo() {
            int dart, faktor, theoretischewurfpunkte, addpunkte, anzahltrefferabzug;
            TextView darts = findViewById(R.id.darts);
            int aktivSeg = aktivesSegment();
            if (ii >= 0) {
                boolean split=false;
                if (xdart == 0) {
                    //spielerwechsel
                    int aufn = wuerfe.get(ii).faktor+wuerfe.get(ii-1).faktor+wuerfe.get(ii-2).faktor;
                    if (aufn==0) split=true;
                    xdart = 2;
                    s = "..";
                    //alten Spieler deaktivieren
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescorepassiv);

                    // segmentwechsel
                    if ((aktiverSpieler==1) ) {
                        if (aktivSeg!=0) {
                            segmentaktiv[aktivSeg] = false;
                            buttonselect(aktivSeg).setTextColor(buttontextcolordisabled);
                            buttonselect(aktivSeg).setEnabled(false);
                            switch (aktivSeg) {
                                case 15:
                                    segmentaktiv[15]=true;
                                    buttonselect(15).setEnabled(true);
                                    buttonselect(15).setTextColor(buttontextcoloraktiv);
                                    break;
                                case 17:
                                    segmentaktiv[21] = true;  // auf Dx setzen
                                    setallactive(true,21);
                                    buttonselect(21).setTextColor(buttontextcoloraktiv);
                                    break;
                                case 19:
                                    segmentaktiv[22] = true;  // auf Tx setzen
                                    setallactive(true,22);
                                    buttonselect(22).setTextColor(buttontextcoloraktiv);
                                    break;
                                case 21:
                                    segmentaktiv[16] = true;
                                    setallactive(false,1);
                                    buttonselect(16).setEnabled(true);
                                    buttonselect(16).setTextColor(buttontextcoloraktiv);
                                    break;
                                case 22:
                                    segmentaktiv[18] = true;
                                    setallactive(false,1);
                                    buttonselect(18).setEnabled(true);
                                    buttonselect(18).setTextColor(buttontextcoloraktiv);
                                    break;
                                case 25:
                                    segmentaktiv[20]=true;
                                    buttonselect(25).setEnabled(false);
                                    buttonselect(25).setTextColor(buttontextcolordisabled);
                                    buttonselect(20).setEnabled(true);
                                    buttonselect(20).setTextColor(buttontextcoloraktiv);
                                    break;
                                default:
                                    // nicht Dx- oder Tx-Feld oder Bull
                                    segmentaktiv[aktivSeg - 1] = true;
                                    buttonselect(aktivSeg - 1).setEnabled(true);
                                    buttonselect(aktivSeg - 1).setTextColor(buttontextcoloraktiv);
                                    break;
                            }
                        }
                    }
                    // neuen Spieler aktivieren
                    aktiverSpieler = wuerfe.get(ii).spielerindex;   //der letzte Spieler der geworfen hat
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setPaintFlags(textfeld(1, aktiverSpieler, 3).getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    textfeld(1, aktiverSpieler, 2).setPaintFlags(textfeld(1, aktiverSpieler, 2).getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescoreaktiv);

                    if (spielmodus.equals("Crazy")) {
                        ladeSegmente(crazySegmentes.get(crazySegmentes.size() - 1)); //letzte gespeicherte segmente laden
                        crazySegmentes.remove(crazySegmentes.size() - 1);
                    }
                    if (spieler[aktiverSpieler].gewinnerplatz != 0)
                        spieler[aktiverSpieler].gewinnerplatz = 0;
                    // wenn halbiert wurde, verdoppeln
                    if (split) spieler[aktiverSpieler].score=spieler[aktiverSpieler].score*2;
                    //todo: folgende zeile fehlt im cricket modus!!
                    aufnahme = wuerfe.get(ii).addpunkte+wuerfe.get(ii-1).addpunkte+wuerfe.get(ii-2).addpunkte;
                } else xdart--;
                if (xdart == 1) s = ".";
                else if (xdart == 0) s = "";
                darts.setText(s);
                dart = wuerfe.get(ii).zahl;
                faktor = wuerfe.get(ii).faktor;
                addpunkte = wuerfe.get(ii).addpunkte;
                theoretischewurfpunkte = dart * faktor;

                spieler[aktiverSpieler].score -= addpunkte;
                anzahltrefferabzug = theoretischewurfpunkte;


                String str = getResources().getString(R.string.undo_1_2_3) + ": ";
                switch (faktor) {
                    case 0:
                        if (split)
                            str = str + getResources().getString(R.string.halbierung);
                        else
                            str = str + getResources().getString(R.string.daneben);
                        break;
                    case 1:
                        str = str + dart;
                        break;
                    case 2:
                        str = str + getResources().getString(R.string.doublebutton) + " " + dart;
                        break;
                    case 3:
                        str = str + getResources().getString(R.string.triple) + " " + dart;
                        break;
                }
                str = str + " " + getResources().getString(R.string.von_spieler) + " " + spieler[wuerfe.get(ii).spielerindex].spielerName;

                Toast.makeText(halve.this, str, Toast.LENGTH_SHORT).show();

                //score textfelder aktualisieren
                textfeld(1, aktiverSpieler, 2).setText(Integer.toString(spieler[aktiverSpieler].score));
                int oldscore=0;
                if (!textfeld(aktivesSegment(),aktiverSpieler,1).getText().toString().isEmpty())
                    oldscore = Integer.parseInt(textfeld(aktivesSegment(),aktiverSpieler,1).getText().toString());
                int newscore = oldscore - addpunkte;
                textfeld(aktivesSegment(), aktiverSpieler, 1).setText(String.valueOf(newscore));
                if ((newscore==0) && (xdart==0)) // keine "0" anzeigen, wenn nichts geworfen wurde
                    textfeld(aktivesSegment(), aktiverSpieler, 1).setText("");
                wuerfe.remove(ii);
                ii--;
                aufnahme-=addpunkte;
            } else Toast.makeText(halve.this, R.string.keinundo, Toast.LENGTH_LONG).show();
        }
    };

    private boolean spielzuende() {
      return alleZahlenClosed();
    }

    private final View.OnClickListener weiter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button daneben=findViewById(R.id.daneben);
            switch (xdart) {
                case 0:
                    daneben.performClick();
                case 1:
                    daneben.performClick();
                case 2:
                    daneben.performClick();
            }
        }
    };

    private void setallactive(Boolean active, int feld) {
        if (active) {
            for (int i = 1; i <= 26; i++) {
                buttonselect(i).setVisibility(View.GONE);
                buttonselect_dt(i).setVisibility(View.VISIBLE);
            }
            buttonselect(27).setVisibility(View.GONE);
            buttonselect(28).setVisibility(View.GONE);

            if (feld==21) {
                buttonselect_dt(21).setTextColor(buttontextcoloraktiv);
                buttonselect_dt(22).setVisibility(View.INVISIBLE);
            }
            if (feld==22) {
                buttonselect_dt(22).setTextColor(buttontextcoloraktiv);
                buttonselect_dt(21).setVisibility(View.INVISIBLE);
            }
        }
        else {
                for (int i = 1; i <= 26; i++)
                    buttonselect_dt(i).setVisibility(View.GONE);
                for (int i = 15; i <= 28;i++)
                    buttonselect(i).setVisibility(View.VISIBLE);
                }
    }

    private final View.OnClickListener buttonclick = new View.OnClickListener() {
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {

            int sender = v.getId();
            Button eingabe = findViewById(sender);
            int dart,
                addpunkte;

            //ergebnis = Integer.parseInt(score.getText().toString());

            // welche taste wurde gedrückt?
            switch (sender) {
                case R.id.bull:
                case R.id.bull_dt:
                    dart = 25;
                    break;
                case R.id.daneben:
                case R.id.daneben_dt:
                    dart = 0;
                    break;
                default:
                    try {
                        dart = Integer.parseInt(eingabe.getText().toString());
                    } catch (Exception e) {
                        dart = 255;         //fehlerwert - sollte niemals eintreten
                    }
                    break;
            }

            int faktor;
            if (d) //double?
            {
                faktor = 2;
                d = false;
                Button doubleb = findViewById(R.id.doublebutton);
                doubleb.setBackgroundColor(bcolorn);
                Button double_dt = findViewById(R.id.doublebutton_dt);
                double_dt.setBackgroundColor(bcolorn);
            } else if (t)  //triple?
            {
                faktor = 3;
                t = false;
                Button tripleb = findViewById(R.id.triple);
                tripleb.setBackgroundColor(bcolorn);
                Button tripleb_dt = findViewById(R.id.triple_dt);
                tripleb_dt.setBackgroundColor(bcolorn);
                if (dart == 25) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.triplewirklich), Toast.LENGTH_LONG).show();
                    return;
                }
            } else //single!
            {
                faktor = 1;
            }


            TextView darts = findViewById(R.id.darts);


            if (xdart < 2) {
                xdart++;
                s = s + (".");
            } else {
                xdart = 0;
                s = "";
            }

            pfeil aktuellerWurf;
        //    if (dart != 0) {
                if (segmentaktiv[21] && faktor!=2) faktor=0;
                if (segmentaktiv[22] && faktor!=3) faktor=0;
                spieler[aktiverSpieler].treffer[dart]+=faktor;
                addpunkte = faktor*dart;
                aufnahme += addpunkte;
                spieler[aktiverSpieler].score += addpunkte;
                //folgende 7 zeilen zum undo-speichern
                aktuellerWurf = new pfeil();
                aktuellerWurf.faktor = faktor;
                ii++;
                aktuellerWurf.addpunkte = addpunkte;
                aktuellerWurf.spielerindex = aktiverSpieler;
                aktuellerWurf.zahl = dart;
                wuerfe.add(ii, aktuellerWurf);

                // richtige Punkteanzahl in aktives/richtiges Textfeld schreiben
                int oldscore=0;
                if (!textfeld(aktivesSegment(),aktiverSpieler,1).getText().toString().isEmpty())
                    oldscore = Integer.parseInt(textfeld(aktivesSegment(),aktiverSpieler,1).getText().toString());
                int newscore = oldscore + aktuellerWurf.addpunkte;

                if (xdart==0 && aufnahme==0) {
                    if (spieler[aktiverSpieler].score % 2 != 0) spieler[aktiverSpieler].score++;
                    spieler[aktiverSpieler].score=spieler[aktiverSpieler].score / 2;
                }

                textfeld(aktivesSegment(), aktiverSpieler, 1).setText(String.valueOf(newscore));
                textfeld(1, aktiverSpieler, 2).setText(Integer.toString(spieler[aktiverSpieler].score));



                //spiel zuende?
                if (alleZahlenClosed()) {
                    ende();
                    return;
                }

                // Spiel geht weiter, egal was geworfen wurde
                if (xdart == 0) {  //Spielerwechsel

                    String ausgabe;
                    if (aufnahme!=0) ausgabe = Integer.toString(aufnahme);
                    else {
                        ausgabe=getResources().getString(R.string.halbierung);
                    }

                        buttonfreeze(true);
                        final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                        if (spieler[aktiverSpieler].gewinnerplatz != 0)
                            aufnahmetv.setText(spieler[aktiverSpieler].gewinnerplatz + ".");
                        else aufnahmetv.setText(ausgabe);
                        aufnahmetv.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                buttonfreeze(false);
                                aufnahmetv.setVisibility(View.INVISIBLE);
                            }
                        }, changetime);
                        aufnahme = 0;



                    textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescorepassiv);
                    if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                    else aktiverSpieler = 1;
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescoreaktiv);
                    if (spielmodus.equals("Crazy")) {
                        crazySegmente segment = new crazySegmente();
                        speichereSegmente(segment);
                        crazySegmentes.add(segment);
                        randomfieldselect();
                    }
                    // nächste Runde freischalten und alte sperren (spielerwechsel)
                    if (aktiverSpieler==1) {
                        if (segmentaktiv[25]) {
                            segmentaktiv[25]=false;
                            buttonselect(25).setEnabled(false);
                        }
                        for (int i=1;i<=22;i++) {
                            if (segmentaktiv[i]) {
                                segmentaktiv[i] = false;
                                buttonselect(i).setTextColor(buttontextcolordisabled);
                                switch (i) {
                                    case 16:
                                        segmentaktiv[21] = true;  // auf Dx setzen
                                        setallactive(true,21);
                                        buttonselect(21).setTextColor(buttontextcoloraktiv);
                                        break;
                                    case 18:
                                        segmentaktiv[22] = true;  // auf Tx setzen
                                        setallactive(true,22);
                                        buttonselect(22).setTextColor(buttontextcoloraktiv);
                                        break;
                                    case 20:
                                        segmentaktiv[25] = true;  // auf Bull setzen (letztes Segment)
                                        buttonselect(25).setEnabled(true);
                                        buttonselect(25).setTextColor(buttontextcoloraktiv);
                                        break;
                                    case 21:
                                        segmentaktiv[17] = true;
                                        setallactive(false,21);
                                        buttonselect(17).setEnabled(true);
                                        buttonselect(17).setTextColor(buttontextcoloraktiv);
                                        break;
                                    case 22:
                                        segmentaktiv[19] = true;
                                        setallactive(false,22);
                                        buttonselect(19).setEnabled(true);
                                        buttonselect(19).setTextColor(buttontextcoloraktiv);
                                        break;
                                    default:
                                        // nicht Dx- oder Tx-Feld
                                        segmentaktiv[i + 1] = true;
                                        buttonselect(i + 1).setEnabled(true);
                                        buttonselect(i + 1).setTextColor(buttontextcoloraktiv);
                                        buttonselect(i).setEnabled(false);
                                        break;
                                }

                                break;
                            }
                        }
                    }
                }
                darts.setText(s);


        }


        private void ende()
        {
            AlertDialog alertDialog = new AlertDialog.Builder(halve.this).create();
            alertDialog.setTitle(getResources().getString(R.string.achtung));
            alertDialog.setMessage(getResources().getString(R.string.letzteEingabeKorrekt));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ja),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            if ( alleZahlenClosed()) {
                                //spielende einleiten und aufrufen
                                long spielzeit = (System.currentTimeMillis() - startTime) / 1000;
                                Intent intent = new Intent(halve.this, spielende.class);
                                intent.putExtra("anzahl", spieleranzahl);
                                intent.putExtra("cricket", true);
                                intent.putExtra("spielzeit", spielzeit);

                                for (int i=1;i<=spieleranzahl;i++) {
                                    spieler[i].gewinnerplatz=i;
                                }
                                boolean changed=true;
                                int platzsave;
                                while (changed) {
                                    changed = false;
                                    for (int i = 1; i <= spieleranzahl; i++) {
                                        for (int j = 1; j <= spieleranzahl; j++) {
                                            if (spieler[i].score > spieler[j].score && spieler[i].gewinnerplatz > spieler[j].gewinnerplatz) {
                                                platzsave = spieler[j].gewinnerplatz;
                                                spieler[j].gewinnerplatz = spieler[i].gewinnerplatz;
                                                spieler[i].gewinnerplatz = platzsave;
                                                changed = true;
                                            }
                                        }
                                    }
                                }

                                for (int i=1;i<=spieleranzahl;i++) {
                                    switch (spieler[i].gewinnerplatz) {
                                        case 1:
                                            intent.putExtra("erster", spieler[i].spielerName);
                                            intent.putExtra("ersterscore", spieler[i].score);
                                            break;
                                        case 2:
                                            intent.putExtra("zweiter", spieler[i].spielerName);
                                            intent.putExtra("zweiterscore", spieler[i].score);
                                            break;
                                        case 3:
                                            intent.putExtra("dritter", spieler[i].spielerName);
                                            intent.putExtra("dritterscore", spieler[i].score);
                                            break;
                                        case 4:
                                            intent.putExtra("vierter", spieler[i].spielerName);
                                            intent.putExtra("vierterscore", spieler[i].score);
                                            break;
                                        case 5:
                                            intent.putExtra("fuenfter", spieler[i].spielerName);
                                            intent.putExtra("fuenfterscore", spieler[i].score);
                                            break;
                                        case 6:
                                            intent.putExtra("sechster", spieler[i].spielerName);
                                            intent.putExtra("sechsterscore", spieler[i].score);
                                            break;
                                        case 7:
                                            intent.putExtra("siebenter", spieler[i].spielerName);
                                            intent.putExtra("siebenterscore", spieler[i].score);
                                            break;
                                        case 8:
                                            intent.putExtra("achter", spieler[i].spielerName);
                                            intent.putExtra("achterscore", spieler[i].score);
                                            break;
                                    }
                                }
                                startActivity(intent);
                                finish();
                                return;

                            }
                            if (xdart == 0) {  //Spielerwechsel

                                String ausgabe;
                                if (aufnahme!=0) ausgabe = Integer.toString(aufnahme);
                                else {
                                    ausgabe=getResources().getString(R.string.halbierung);
                                    if (spieler[aktiverSpieler].score % 2 != 0) spieler[aktiverSpieler].score++;
                                    spieler[aktiverSpieler].score=spieler[aktiverSpieler].score / 2;
                                }

                                buttonfreeze(true);
                                final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                                if (spieler[aktiverSpieler].gewinnerplatz != 0)
                                    aufnahmetv.setText(spieler[aktiverSpieler].gewinnerplatz + ".");
                                else aufnahmetv.setText(ausgabe);
                                aufnahmetv.setVisibility(View.VISIBLE);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        buttonfreeze(false);
                                        aufnahmetv.setVisibility(View.INVISIBLE);
                                    }
                                }, changetime);
                                aufnahme = 0;

                                textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                                textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                                textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                                textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescorepassiv);

                                if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                                else aktiverSpieler = 1;
                                textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                                textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                                textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                                textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescoreaktiv);
                                if (spielmodus.equals("Crazy")) {
                                    crazySegmente segment = new crazySegmente();
                                    speichereSegmente(segment);
                                    crazySegmentes.add(segment);
                                    randomfieldselect();
                                }
                                // nächste Runde freischalten und alte sperren
                                // (wenn spiel zuende  und  abfrage = ja)
                                if (aktiverSpieler==1) {
                                    if (segmentaktiv[25]) {
                                        segmentaktiv[25]=false;
                                        buttonselect(25).setEnabled(false);
                                    }
                                    for (int i=1;i<=22;i++) {
                                        if (segmentaktiv[i]) {
                                            segmentaktiv[i] = false;
                                            buttonselect(i).setTextColor(buttontextcolordisabled);
                                            switch (i) {
                                                case 16:
                                                    segmentaktiv[21] = true;  // auf Dx setzen
                                                    setallactive(true,21);
                                                    buttonselect(21).setTextColor(buttontextcoloraktiv);
                                                    break;
                                                case 18:
                                                    segmentaktiv[22] = true;  // auf Tx setzen
                                                    setallactive(true,22);
                                                    break;
                                                case 20:
                                                    segmentaktiv[25] = true;  // auf Bull setzen (letztes Segment)
                                                    buttonselect(25).setEnabled(true);
                                                    break;
                                                case 21:
                                                    segmentaktiv[17] = true;
                                                    setallactive(false,21);
                                                    buttonselect(17).setEnabled(true);
                                                    break;
                                                case 22:
                                                    segmentaktiv[19] = true;
                                                    setallactive(false,22);
                                                    buttonselect(19).setEnabled(true);
                                                    break;
                                                default:
                                                    // nicht Dx- oder Tx-Feld
                                                    segmentaktiv[i + 1] = true;
                                                    buttonselect(i + 1).setEnabled(true);
                                                    buttonselect(i).setEnabled(false);
                                                    break;
                                            }
                                            segmentaktiv[i] = false;

                                            break;
                                        }
                                    }
                                }
                            }
                            TextView darts = findViewById(R.id.darts);
                            darts.setText(s);
                        }
                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    // letzter Wurf war eine Fehleingabe -> Spiel nicht beenden und letzte Eingabe zurücknehmen ohne nochmal nachzufragen
                    TextView darts = findViewById(R.id.darts);
                    if (xdart < 2) {
                        xdart++;
                        s = s + (".");

                    } else {  //Spielerwechsel
                        xdart = 0;
                        s = "";
                    }
                    darts.setText(s);

                    undoabfrage = false;
                    Button undo = findViewById(R.id.undo);
                    undo.performClick();
                }
            });

            if (spielzuende()) {
                alertDialog.show();
            }
            else {
                //alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
            }
        }

    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_halve);
        TypedValue outValue = new TypedValue();
        halve.this.getTheme().resolveAttribute(R.attr.colorButtonNormal, outValue, true);
        bcolor = outValue.data;
        halve.this.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
        bcolorn = outValue.data;
        SharedPreferences settings = getSharedPreferences("Einstellungen",0);
        ConstraintLayout main = findViewById(R.id.matchbereich);
        if (settings.contains("keepscreenongame")) main.setKeepScreenOn(settings.getBoolean("keepscreenongame",true));


        TextView p1name = findViewById(R.id.p1name);
        TextView p2name = findViewById(R.id.p2name);
        textcoloraktiv = p1name.getCurrentTextColor();
        textcolorpassiv = p2name.getCurrentTextColor();
        textsizepassiv = p2name.getTextSize();
        textsizeaktiv = textsizepassiv + 4;
        TextView p1score = findViewById(R.id.p1score);
        textsizescorepassiv = p1score.getTextSize();
        textsizescoreaktiv = p1score.getTextSize()+4;
        buttonselect(16).setEnabled(false);
        buttontextcolordisabled = buttonselect(16).getCurrentTextColor();
        buttonselect(16).setEnabled(true);
        buttontextcoloraktiv = textcoloraktiv;
        buttontextcolorenabled = buttonselect(16).getCurrentTextColor();
        textfeld(1, 1, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
        textfeld(1, 1, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizescoreaktiv);

        Intent intent = getIntent();
        spieleranzahl = Integer.parseInt(intent.getStringExtra("spieleranzahl"));
        spielmodus = intent.getCharSequenceExtra("spielmodus");
        CharSequence spieler1n = intent.getCharSequenceExtra("spieler1");
        CharSequence spieler2n = intent.getCharSequenceExtra("spieler2");
        CharSequence spieler3n = intent.getCharSequenceExtra("spieler3");
        CharSequence spieler4n = intent.getCharSequenceExtra("spieler4");
        CharSequence spieler5n = intent.getCharSequenceExtra("spieler5");
        CharSequence spieler6n = intent.getCharSequenceExtra("spieler6");
        CharSequence spieler7n = intent.getCharSequenceExtra("spieler7");
        CharSequence spieler8n = intent.getCharSequenceExtra("spieler8");
        player s1 = new player();
        player s2 = new player();
        player s3 = new player();
        player s4 = new player();
        player s5 = new player();
        player s6 = new player();
        player s7 = new player();
        player s8 = new player();

        spieler[1] = s1;
        spieler[2] = s2;
        spieler[3] = s3;
        spieler[4] = s4;
        spieler[5] = s5;
        spieler[6] = s6;
        spieler[7] = s7;
        spieler[8] = s8;
        for (int a = 1; a <= spieleranzahl; a++) spieler[a].score = 0;

        spieler[1].spielerName = spieler1n.toString();
        spieler[2].spielerName = spieler2n.toString();
        spieler[3].spielerName = spieler3n.toString();
        spieler[4].spielerName = spieler4n.toString();
        spieler[5].spielerName = spieler5n.toString();
        spieler[6].spielerName = spieler6n.toString();
        spieler[7].spielerName = spieler7n.toString();
        spieler[8].spielerName = spieler8n.toString();

        TextView p3name = findViewById(R.id.p3name);
        TextView p4name = findViewById(R.id.p4name);
        TextView p5name = findViewById(R.id.p5name);
        TextView p6name = findViewById(R.id.p6name);
        TextView p7name = findViewById(R.id.p7name);
        TextView p8name = findViewById(R.id.p8name);
        TextView p2score = findViewById(R.id.p2score);
        TextView p3score = findViewById(R.id.p3score);
        TextView p4score = findViewById(R.id.p4score);
        TextView p5score = findViewById(R.id.p5score);
        TextView p6score = findViewById(R.id.p6score);
        TextView p7score = findViewById(R.id.p7score);
        TextView p8score = findViewById(R.id.p8score);

        // Namenszuweisung und Layout der Namensanzeige in Abhängigkeit von der Anzahl der Spieler ändern
        p1name.setText(spieler[1].spielerName);
        switch (spieleranzahl) {
            case 2:
                ConstraintSet cs = new ConstraintSet();
                cs.clone(main);
                cs.connect(p2name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
                cs.applyTo(main);
                p3name.setVisibility(View.GONE);
                p4name.setVisibility(View.GONE);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p3score.setVisibility(View.GONE);
                p4score.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                break;
            case 1:
                p2name.setVisibility(View.GONE);
                p3name.setVisibility(View.GONE);
                p4name.setVisibility(View.GONE);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p2score.setVisibility(View.GONE);
                p3score.setVisibility(View.GONE);
                p4score.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                break;
            case 3:
                ConstraintSet cs3 = new ConstraintSet();
                cs3.clone(main);
                cs3.connect(p2name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs3.connect(p3name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.END);
                cs3.connect(p3name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
                cs3.applyTo(main);
                p4name.setVisibility(View.GONE);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p4score.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                break;
            case 4:
                ConstraintSet cs4 = new ConstraintSet();
                cs4.clone(main);
                cs4.connect(p2name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs4.connect(p3name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.START);
                cs4.connect(p3name.getId(), ConstraintSet.END, R.id.p4name, ConstraintSet.START);
                cs4.connect(p4name.getId(), ConstraintSet.START, R.id.p3name, ConstraintSet.END);
                cs4.connect(p4name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
                cs4.applyTo(main);
                p5name.setVisibility(View.GONE);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p5score.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                break;
            case 5:
                ConstraintSet cs5 = new ConstraintSet();
                cs5.clone(main);
                cs5.connect(p3name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs5.connect(p4name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.START);
                cs5.connect(p4name.getId(), ConstraintSet.END, R.id.p5name, ConstraintSet.START);
                cs5.connect(p5name.getId(), ConstraintSet.START, R.id.p4name, ConstraintSet.END);
                cs5.applyTo(main);
                p6name.setVisibility(View.GONE);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p6score.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                break;
            case 6:
                ConstraintSet cs6 = new ConstraintSet();
                cs6.clone(main);
                cs6.connect(p3name.getId(), ConstraintSet.END, R.id.mittelinks, ConstraintSet.END);
                cs6.connect(p4name.getId(), ConstraintSet.START, R.id.mitterrechts, ConstraintSet.START);
                cs6.connect(p4name.getId(), ConstraintSet.END, R.id.p5name, ConstraintSet.START);
                cs6.connect(p5name.getId(), ConstraintSet.START, R.id.p4name, ConstraintSet.END);
                cs6.applyTo(main);
                p7name.setVisibility(View.GONE);
                p8name.setVisibility(View.GONE);
                p7score.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                p6name.setText(spieler[6].spielerName);
                break;
            case 7:
                p8name.setVisibility(View.GONE);
                p8score.setVisibility(View.GONE);
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                p6name.setText(spieler[6].spielerName);
                p7name.setText(spieler[7].spielerName);
                break;
            case 8:
                p2name.setText(spieler[2].spielerName);
                p3name.setText(spieler[3].spielerName);
                p4name.setText(spieler[4].spielerName);
                p5name.setText(spieler[5].spielerName);
                p6name.setText(spieler[6].spielerName);
                p7name.setText(spieler[7].spielerName);
                p8name.setText(spieler[8].spielerName);
                break;
        }

        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button restdaneben = findViewById(R.id.weiter);
        Button bundo = findViewById(R.id.undo);
        Button bdx = findViewById(R.id.bdx);
        Button btx = findViewById(R.id.btx);
        Button b1_dt = findViewById(R.id.b1_dt);
        Button b2_dt = findViewById(R.id.b2_dt);
        Button b3_dt = findViewById(R.id.b3_dt);
        Button b4_dt = findViewById(R.id.b4_dt);
        Button b5_dt = findViewById(R.id.b5_dt);
        Button b6_dt = findViewById(R.id.b6_dt);
        Button b7_dt = findViewById(R.id.b7_dt);
        Button b8_dt = findViewById(R.id.b8_dt);
        Button b9_dt = findViewById(R.id.b9_dt);
        Button b10_dt = findViewById(R.id.b10_dt);
        Button b11_dt = findViewById(R.id.b11_dt);
        Button b12_dt = findViewById(R.id.b12_dt);
        Button b13_dt = findViewById(R.id.b13_dt);
        Button b14_dt = findViewById(R.id.b14_dt);
        Button b15_dt = findViewById(R.id.b15_dt);
        Button b16_dt = findViewById(R.id.b16_dt);
        Button b17_dt = findViewById(R.id.b17_dt);
        Button b18_dt = findViewById(R.id.b18_dt);
        Button b19_dt = findViewById(R.id.b19_dt);
        Button b20_dt = findViewById(R.id.b20_dt);
        Button bull_dt = findViewById(R.id.bull_dt);
        Button daneben_dt = findViewById(R.id.daneben_dt);
        Button restdaneben_dt = findViewById(R.id.restdaneben_dt);
        Button bundo_dt = findViewById(R.id.undo_dt);
        Button bdouble_dt = findViewById(R.id.doublebutton_dt);
        Button btriple_dt = findViewById(R.id.triple_dt);


        b1.setOnClickListener(buttonclick);
        b2.setOnClickListener(buttonclick);
        b3.setOnClickListener(buttonclick);
        b4.setOnClickListener(buttonclick);
        b5.setOnClickListener(buttonclick);
        b6.setOnClickListener(buttonclick);
        b7.setOnClickListener(buttonclick);
        b8.setOnClickListener(buttonclick);
        b9.setOnClickListener(buttonclick);
        b10.setOnClickListener(buttonclick);
        b11.setOnClickListener(buttonclick);
        b12.setOnClickListener(buttonclick);
        b13.setOnClickListener(buttonclick);
        b14.setOnClickListener(buttonclick);
        b15.setOnClickListener(buttonclick);
        b16.setOnClickListener(buttonclick);
        b17.setOnClickListener(buttonclick);
        b18.setOnClickListener(buttonclick);
        b19.setOnClickListener(buttonclick);
        b20.setOnClickListener(buttonclick);
        daneben.setOnClickListener(buttonclick);
        restdaneben.setOnClickListener(weiter);
        bull.setOnClickListener(buttonclick);
        bdouble.setOnClickListener(doubletriple);
        btripel.setOnClickListener(doubletriple);
        bundo.setOnClickListener(undoclick);

        b1_dt.setOnClickListener(buttonclick);
        b2_dt.setOnClickListener(buttonclick);
        b3_dt.setOnClickListener(buttonclick);
        b4_dt.setOnClickListener(buttonclick);
        b5_dt.setOnClickListener(buttonclick);
        b6_dt.setOnClickListener(buttonclick);
        b7_dt.setOnClickListener(buttonclick);
        b8_dt.setOnClickListener(buttonclick);
        b9_dt.setOnClickListener(buttonclick);
        b10_dt.setOnClickListener(buttonclick);
        b11_dt.setOnClickListener(buttonclick);
        b12_dt.setOnClickListener(buttonclick);
        b13_dt.setOnClickListener(buttonclick);
        b14_dt.setOnClickListener(buttonclick);
        b15_dt.setOnClickListener(buttonclick);
        b16_dt.setOnClickListener(buttonclick);
        b17_dt.setOnClickListener(buttonclick);
        b18_dt.setOnClickListener(buttonclick);
        b19_dt.setOnClickListener(buttonclick);
        b20_dt.setOnClickListener(buttonclick);
        daneben_dt.setOnClickListener(buttonclick);
        restdaneben_dt.setOnClickListener(weiter);
        bull_dt.setOnClickListener(buttonclick);
        bdouble_dt.setOnClickListener(doubletriple);
        btriple_dt.setOnClickListener(doubletriple);
        bundo_dt.setOnClickListener(undoclick);

        if (settings.contains("changetime")) changetime = settings.getInt("changetime", 1500);
        if (settings.contains("crazystart"))
            crazycricketRangeMinimum = settings.getInt("crazystart",6);

        for (int z=0;z<=25;z++) {
            segmentaktiv[z]=false;
        }

        //Layout in Abhängikeit vom spielmodus ändern

        b16.setEnabled(false);
        b17.setEnabled(false);
        b18.setEnabled(false);
        b19.setEnabled(false);
        b20.setEnabled(false);
        bull.setEnabled(false);
        bdx.setEnabled(false);
        btx.setEnabled(false);

        if (!spielmodus.equals("Crazy")) {
            b1.setVisibility(View.GONE);
            b2.setVisibility(View.GONE);
            b3.setVisibility(View.GONE);
            b4.setVisibility(View.GONE);
            b5.setVisibility(View.GONE);
            b6.setVisibility(View.GONE);
            b7.setVisibility(View.GONE);
            b8.setVisibility(View.GONE);
            b9.setVisibility(View.GONE);
        }

        if (spielmodus.equals("15-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b13.setVisibility(View.GONE);
            b14.setVisibility(View.GONE);
            segmentaktiv[15]=true;
            buttonselect(15).setTextColor(buttontextcoloraktiv);
        } else if (spielmodus.equals("14-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b13.setVisibility(View.GONE);
            b15.setEnabled(false);
            segmentaktiv[14]=true;
            buttonselect(14).setTextColor(buttontextcoloraktiv);
        } else if (spielmodus.equals("13-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b15.setEnabled(false);
            b14.setEnabled(false);
            segmentaktiv[13]=true;
            buttonselect(13).setTextColor(buttontextcoloraktiv);
        } else if (spielmodus.equals("12-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b15.setEnabled(false);
            b14.setEnabled(false);
            b13.setEnabled(false);
            buttonselect(12).setTextColor(buttontextcoloraktiv);
            segmentaktiv[12]=true;
        } else if (spielmodus.equals("11-Bull")) {
            b10.setVisibility(View.GONE);
            b15.setEnabled(false);
            b14.setEnabled(false);
            b13.setEnabled(false);
            b12.setEnabled(false);
            segmentaktiv[11]=true;
            buttonselect(11).setTextColor(buttontextcoloraktiv);
        } else if (spielmodus.equals("10-Bull")) {
            b15.setEnabled(false);
            b14.setEnabled(false);
            b13.setEnabled(false);
            b12.setEnabled(false);
            b11.setEnabled(false);
            segmentaktiv[10]=true;
            buttonselect(10).setTextColor(buttontextcoloraktiv);
        }
        else if (spielmodus.equals("Crazy")) {
            initial_randomfieldselect(crazycricketFelderAnzahl);
        }
        findViewById(android.R.id.content).invalidate();

        startTime = System.currentTimeMillis();
    }

    private void ladeSegmente(crazySegmente seg) {
        for (int button = crazycricketRangeMinimum; button <= 20; button++) {
            if (seg.segmente[button]) zeigeFeld(button);
            else versteckeFeld(button);
        }
    }

    private void speichereSegmente(crazySegmente seg) {
        for (int button = crazycricketRangeMinimum; button <= 20; button++) {
            seg.segmente[button] = buttonselect(button).getVisibility() == View.VISIBLE;
        }
    }

    private void randomfieldselect() {
        boolean leereZahlenfelder = false;
        int anzahlBenoetigterNeuerZahlenfelder = 0;
        // zählung und ausblendung der ungetroffenen zahlenfelder
        for (int zahlenfeld = 1; zahlenfeld <= 20; zahlenfeld++) {
            if ((buttonselect(zahlenfeld).getVisibility() == View.VISIBLE) && (!open(zahlenfeld))) {
                leereZahlenfelder = true;
                versteckeFeld(zahlenfeld);
                anzahlBenoetigterNeuerZahlenfelder++;
            }
        }
        // wenn es keine zutauschenden zahlenfelder gibt, hat die funktion nichts mehr zu tun
        if (!leereZahlenfelder) return;
        // neue, zufällig gewählte Felder sichtbar machen
        Random rand = new Random();
        int neuesFeld;
        boolean unfertig;
        for (int anzahl = anzahlBenoetigterNeuerZahlenfelder; anzahl > 0; anzahl--) {
            unfertig = true;
            while (unfertig) {
                neuesFeld = rand.nextInt(21 - crazycricketRangeMinimum) + crazycricketRangeMinimum;
                if (buttonselect(neuesFeld).getVisibility() == View.GONE) {
                    zeigeFeld(neuesFeld);
                    unfertig = false;
                }
            }
        }
    }

    // wählt zufällige Zahlenfelder aus und versteckt nicht benötigte zahlenfelder
    private void initial_randomfieldselect(int felderAnzahl) {
        if (felderAnzahl > 20) return;
        for (int i = 1; i < crazycricketRangeMinimum; i++) {
            versteckeFeld(i);
        }

        Random rand = new Random();
        boolean unfertig;
        int feldnummer;
        for (int i = crazycricketRangeMinimum; i <= (20 - felderAnzahl + 1); i++) {
            unfertig = true;
            while (unfertig) {
                feldnummer = rand.nextInt(21 - crazycricketRangeMinimum) + crazycricketRangeMinimum;
                if (buttonselect(feldnummer).getVisibility() == View.VISIBLE) {
                    versteckeFeld(feldnummer);
                    unfertig = false;
                }
            }
        }

    }

    private void versteckeFeld(int feld) {
        buttonselect(feld).setVisibility(View.GONE);
        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++)
            textfeld(feld, spielerindex, 1).setVisibility(View.GONE);
    }

    private void zeigeFeld(int feld) {
        buttonselect(feld).setVisibility(View.VISIBLE);
        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++)
            textfeld(feld, spielerindex, 1).setVisibility(View.VISIBLE);
    }



    // hat irgendein spieler das zahlenfeld bereits 3x getroffen?
    private boolean open(int zahlenfeld) {
        for (int spielerindex = 1; spielerindex <= spieleranzahl; spielerindex++) {
            if (spieler[spielerindex].treffer[zahlenfeld] == 3) {
                return true;
            }
        }
        return false;
    }

    private int aktivesSegment() {
        for (int i=1;i<=25;i++) {
            if (segmentaktiv[i])
                return i;
        }
        return 0;
    }

    private Button buttonselect(int buttonnumber) {
        String stt;
        switch (buttonnumber) {
            case 21:
                stt="bdx";
                break;
            case 22:
                stt="btx";
                break;
            case 23:
                stt="daneben";
                break;
            case 24:
                stt="weiter";
                break;
            case 25:
                stt="bull";
                break;
            case 26:
                stt="undo";
                break;
            case 27:
                stt="doublebutton";
                break;
            case 28:
                stt="triple";
                break;
            default:
                stt = "b" + buttonnumber;
                break;
        }
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }
    private Button buttonselect_dt(int buttonnumber) {
        String stt;
        switch (buttonnumber) {
            case 21:
                stt="doublebutton_dt";
                break;
            case 22:
                stt="triple_dt";
                break;
            case 23:
                stt="daneben_dt";
                break;
            case 24:
                stt="restdaneben_dt";
                break;
            case 25:
                stt="bull_dt";
                break;
            case 26:
                stt="undo_dt";
                break;
            default:
                stt = "b" + buttonnumber + "_dt";
                break;
        }
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }

    // liefert das gewünschte textview zurück
    private TextView textfeld(int dart, int aktiverSpieler, int typ)
    // typ=1 -> trefferfeld
    // typ=2 -> scorefeld
    // typ=3 -> namensfeld
    {
        String stt = "";
        switch (typ) {
            case 1:
                if (dart == 25) stt = "tbull" + "p" + aktiverSpieler;
                else if (dart == 21) stt = "tdx"+"p"+aktiverSpieler;
                     else if (dart == 22) stt = "ttx"+"p"+aktiverSpieler;
                          else stt = "t" + dart + "p" + aktiverSpieler;
                break;
            case 2:
                stt = "p" + aktiverSpieler + "score";
                break;
            case 3:
                stt = "p" + aktiverSpieler + "name";
                break;
        }
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }

    private boolean alleZahlenClosed() {
        // spiel ist vorbei, wenn a) das Bull Segment aktiv ist, b) der letzte Spieler dran ist/war und c) er 3x geworfen hat
        return segmentaktiv[25] && aktiverSpieler == spieleranzahl && xdart == 0;
       // if ( (!segmentaktiv[25] || aktiverSpieler != spieleranzahl) || textfeld(25,spieleranzahl,1).getText().toString().isEmpty() || xdart !=2 ) return false;
    }


    private void buttonfreeze(boolean freeze) {

        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button restdaneben = findViewById(R.id.weiter);
        Button bundo = findViewById(R.id.undo);
        Button bdx = findViewById(R.id.bdx);
        Button btx = findViewById(R.id.btx);

        freeze = !freeze;       //eine frage der logik ;-)

        bdouble.setEnabled(freeze);
        btripel.setEnabled(freeze);
        daneben.setEnabled(freeze);
        restdaneben.setEnabled(freeze);
        bundo.setEnabled(freeze);
        int aktseg = aktivesSegment();
        if (!freeze) {
                b1.setEnabled(false);
                b2.setEnabled(false);
                b3.setEnabled(false);
                b4.setEnabled(false);
                b5.setEnabled(false);
                b6.setEnabled(false);
                b7.setEnabled(false);
                b8.setEnabled(false);
                b9.setEnabled(false);
                b10.setEnabled(false);
                b11.setEnabled(false);
                b12.setEnabled(false);
                b13.setEnabled(false);
                b14.setEnabled(false);
                b15.setEnabled(false);
                b16.setEnabled(false);
                b17.setEnabled(false);
                b18.setEnabled(false);
                b19.setEnabled(false);
                b20.setEnabled(false);
                bull.setEnabled(false);
                bdx.setEnabled(false);
                btx.setEnabled(false);
                for (int i=1;i<=26;i++) buttonselect_dt(i).setEnabled(false);
        } else {
            switch (aktseg) {
                case 21:
                    setallactive(true, 21);
                    buttonselect(21).setTextColor(buttontextcoloraktiv);
                    break;
                case 22:
                    setallactive(true, 22);
                    buttonselect(22).setTextColor(buttontextcoloraktiv);
                    break;
                default:
                    // nicht Dx- oder Tx-Feld
                    buttonselect(aktseg).setEnabled(true);
                    break;
            }
            for (int i=1;i<=26;i++) buttonselect_dt(i).setEnabled(true);
        }
    }


    public void onBackPressed() {

        AlertDialog alertDialog = new AlertDialog.Builder(halve.this).create();
        alertDialog.setTitle(getResources().getString(R.string.achtung));
        alertDialog.setMessage(getResources().getString(R.string.willstduverlassen));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.zuruck), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private static class player {
        final int[] treffer = new int[26]; // feld 25 ist anzahl_bull_treffer, ansonsten 10-20, bei crazy cricket auch mehr;
        String spielerName;
        int score;
        int darts;
        int gewinnerplatz;
    }

    private static class pfeil {
        int zahl;
        int faktor;
        int addpunkte;
        int spielerindex;
    }

    private static class crazySegmente {
        final boolean[] segmente = new boolean[26];
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }


}

