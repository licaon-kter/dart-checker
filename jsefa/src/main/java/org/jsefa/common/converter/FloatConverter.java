package org.jsefa.common.converter;



import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.ParseException;
import java.util.Locale;

/**
 * Converter for <code>Float</code> objects.<br>
 * The format consists of two <code>String</code>s. The first denotes the {@link Locale} and the second is a
 * pattern as used by {@link DecimalFormat}.<br>
 * <p>
 * It is thread-safe (the access to the non-thread-safe {@link DecimalFormat} is synchronized).
 *
 * @author Norman Lahme-Huetig, this is a copy of BigDecimalConverter class, modified by OneStep
 */
public class FloatConverter implements SimpleTypeConverter {
    /**
     * The default format which is used when no format is explicitly given.
     */
    private static final String[] DEFAULT_FORMAT = {"en", "#0.00"};

    private static final FieldPosition FIELD_POSITION = new FieldPosition(0);

    private final DecimalFormat decimalFormat;


    /**
     * Creates a <code>BigDecimalConverter</code>.<br>
     * If no format is given, the default format (see {@link #getDefaultFormat()}) is used.
     *
     * @param configuration the configuration
     * @return a big decimal converter
     * @throws ConversionException if the given format is not valid.
     */
    public static FloatConverter create(SimpleTypeConverterConfiguration configuration) {
        return new FloatConverter(configuration);
    }

    /**
     * Constructs a new <code>BigDecimalConverter</code>.<br>
     * If no format is given, the default format (see {@link #getDefaultFormat()}) is used.
     *
     * @param configuration the configuration
     * @throws ConversionException if the given format is not valid.
     */
    protected FloatConverter(SimpleTypeConverterConfiguration configuration) {
        String[] format = getFormat(configuration);
        try {
            Locale locale = new Locale(format[0]);
            String pattern = format[1];

            this.decimalFormat = new DecimalFormat(pattern, new DecimalFormatSymbols(locale));
            this.decimalFormat.setParseBigDecimal(true);
        } catch (Exception e) {
            throw new ConversionException("Could not create a " + this.getClass().getName() + " with format "
                    + format[0] + ", " + format[1], e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public final synchronized BigDecimal fromString(String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        try {
            Object result = this.decimalFormat.parseObject(value);
            if (result instanceof BigDecimal) {
                return (BigDecimal) result;
            } else {
                return new BigDecimal(((Double) result).doubleValue()).setScale(this.decimalFormat
                        .getMaximumFractionDigits(), BigDecimal.ROUND_HALF_UP);
            }
        } catch (ParseException e) {
            throw new ConversionException("Wrong Float format " + value);
        }
    }

    /**
     * {@inheritDoc}
     */
    public final synchronized String toString(Object value) {
        if (value == null) {
            return null;
        }

        return this.decimalFormat.format(value, new StringBuffer(), FIELD_POSITION).toString();
    }

    /**
     * Returns the default format which is used when no format is given.
     *
     * @return the default format.
     */
    protected String[] getDefaultFormat() {
        return FloatConverter.DEFAULT_FORMAT;
    }

    private String[] getFormat(SimpleTypeConverterConfiguration configuration) {
        if (configuration.getFormat() == null) {
            return getDefaultFormat();
        }
        if (configuration.getFormat().length != 2) {
            throw new ConversionException("The format for a FloatConverter must be an array with 2 entries");
        }
        return configuration.getFormat();
    }

}
