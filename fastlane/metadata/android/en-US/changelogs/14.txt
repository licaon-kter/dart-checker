added:
- support for up to 8 player (except set/leg mode)
- Help: general dart rules

fixed:
- Main screen: selected player names changed, when changing player count
- Help: tiny language mixture appeared
