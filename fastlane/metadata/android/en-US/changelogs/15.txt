added:
- Preferences: language selection
- X01/Cricket: undo safety question
- X01: improved layout for big screens and landscape mode - variable textsize of score

fixed:
- SET/LEG: overview at end of game: wrong text overlays were visible
- main menu: layout of player names especially for small screens and lots of players
- help: light theme wasn't applied