hinzugefügt:
- X01 / SET/LEG / FREE / ELMINATION: Anzeige von mehreren Checkoutvorschlägen sowohl für Singleout, Doubleout als auch Masterout (einfach den Vorschlag berühren)
- Einstellungen: Auswahl des bevorzugten Checkoutsegmentes für die Checkoutvorschläge

bearbeitet:
- CRICKET / HALVE IT: größer Schriftgröße der Punkte

repariert:
- HALVE IT: der letzt mögliche Split im Spiel wurde nicht gespeichert