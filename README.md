Dart Checker
------------

An app for Android, that assists players of darts in counting their scores.


Dart Checker takes care of your score, while you are playing darts
alone or with friends. Its the better form of a scoreboard. No manual calculating
is needed anymore. Just put in your hits, like you would tell.

    Games

    * single X01 (301, 501)
    * SET/LEG (play like the pros)
    * FREE training mode (variable score)
    * (CRAZY) CRICKET (CUT THROAT)
    * ELIMINATION
    * HALVE IT
    * AROUND THE CLOCK


    Features

    * 1-8 player (except SET/LEG mode)
    * master, double and single out
    * master, double and single in
    * checkout suggestions (all reasonably possible)
    * multiple undo in a row of wrong inputs
    * statistics (about as many matches and player as you like) including possibility to export and import
    * two input variants (dart by dart or total score of three darts)
    * power saving dark theme or light one
    * language: german / english / spanish / french
    * offline - absolutely no internet connection is needed or used, beside app download ;-)



[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.DartChecker/)
